/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-01 23:08:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-01 23:10:28
 */
import { GET_REGION, GET_REGION_SUCCESS, GET_REGION_FAILURE, OPEN_DIALOG_REGION, CLOSE_DIALOG_REGION } from "./type";
/**
 * @function getRegion
 * @returns {Object} Object tipe state GET_REGION
 * @description Reducer state untuk request get data region
 */
export const getRegion = () => ({
    type: GET_REGION
});
/**
 * @function getRegionSuccess
 * @returns {Object} Object tipe state GET_REGION_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data region
 */
export const getRegionSuccess = response => ({
    type: GET_REGION_SUCCESS,
    payload: response
});
/**
 * @function getRegionFailure
 * @returns {Object} Object tipe state GET_REGION_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data region
 */
export const getRegionFailure = error => ({
    type: GET_REGION_FAILURE,
    payload: error
});
/**
 * @function openDialogRegion
 * @returns {Object} Object tipe state OPEN_DIALOG_REGION
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar region
 */
export const openDialogRegion = dialog => ({
    type: OPEN_DIALOG_REGION,
    payload: dialog
});
/**
 * @function closeDialogRegion
 * @returns {Object} Object tipe state CLOSE_DIALOG_REGION
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar region
 */
export const closeDialogRegion = dialog => ({
    type: CLOSE_DIALOG_REGION,
    payload: dialog
});