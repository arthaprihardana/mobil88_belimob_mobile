/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 10:52:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-02 15:01:03
 */
import {GET_LOGIN, GET_LOGIN_SUCCESS, GET_LOGIN_FAILURE, GET_LOGOUT, GET_LOGOUT_SUCCESS, GET_LOGOUT_FAILURE} from './type';
/**
 * @function getLogin
 * @param {Object} user_data - Data user yang dikirimkan (username, password)
 * @returns {Object} Object tipe state GET_LOGIN
 * @description Reducer state untuk request login
 */
export const getLogin = user_data => ({
    type: GET_LOGIN,
    payload: user_data
});
/**
 * @function getLoginSuccess
 * @returns {Object} Object tipe state GET_LOGIN_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get login
 */
export const getLoginSuccess = response => ({
    type: GET_LOGIN_SUCCESS,
    payload: response
});
/**
 * @function getLoginFailure
 * @returns {Object} Object tipe state GET_LOGIN_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get login
 */
export const getLoginFailure = error => ({
    type: GET_LOGIN_FAILURE,
    payload: error
});
/**
 * @function getLogout
 * @param {Object} FormData - Data user yang dikirimkan
 * @returns {Object} Object tipe state GET_LOGOUT
 * @description Reducer state untuk request logout
 */
export const getLogout = FormData => ({
    type: GET_LOGOUT,
    payload: FormData
});
/**
 * @function getLogoutSuccess
 * @returns {Object} Object tipe state GET_LOGOUT_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get logout
 */
export const getLogoutSuccess = response => ({
    type: GET_LOGOUT_SUCCESS,
    payload: response
});
/**
 * @function getLogoutFailure
 * @returns {Object} Object tipe state GET_LOGOUT_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get logout
 */
export const getLogoutFailure = error => ({
    type: GET_LOGOUT_FAILURE,
    payload: error
});