/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:29:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-05 15:49:51
 */
import { GET_LEADS_REASON, GET_LEADS_REASON_SUCCESS, GET_LEADS_REASON_FAILURE, OPEN_DIALOG_LEADS_REASON, CLOSE_DIALOG_LEADS_REASON } from "./type";
/**
 * @function getLeadsReason
 * @returns {Object} Object tipe state GET_LEADS_REASON
 * @description Reducer state untuk request get data lead reason
 */
export const getLeadsReason = () => ({
    type: GET_LEADS_REASON
});
/**
 * @function getLeadsReasonSuccess
 * @returns {Object} Object tipe state GET_LEADS_REASON_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data lead reason
 */
export const getLeadsReasonSuccess = response => ({
    type: GET_LEADS_REASON_SUCCESS,
    payload: response
});
/**
 * @function getLeadsReasonFailure
 * @returns {Object} Object tipe state GET_LEADS_REASON_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data lead reason
 */
export const getLeadsReasonFailure = error => ({
    type: GET_LEADS_REASON_FAILURE,
    payload: error
});
/**
 * @function openDialogLeadsReason
 * @returns {Object} Object tipe state OPEN_DIALOG_LEADS_REASON
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar lead reason
 */
export const openDialogLeadsReason = dialog => ({
    type: OPEN_DIALOG_LEADS_REASON,
    payload: dialog
});
/**
 * @function closeDialogLeadsReason
 * @returns {Object} Object tipe state CLOSE_DIALOG_LEADS_REASON
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar lead reason
 */
export const closeDialogLeadsReason = dialog => ({
    type: CLOSE_DIALOG_LEADS_REASON,
    payload: dialog
});