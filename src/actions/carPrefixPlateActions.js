/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:25:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-19 14:43:43
 */
import { GET_CAR_PREFIX_PLATE, GET_CAR_PREFIX_PLATE_SUCCESS, GET_CAR_PREFIX_PLATE_FAILURE, OPEN_DIALOG_CAR_PREFIX_PLATE, CLOSE_DIALOG_CAR_PREFIX_PLATE } from "./type";
/**
 * @function getCarPrefixPlate
 * @returns {Object} Object tipe state GET_CAR_PREFIX_PLATE
 * @description Reducer state untuk request get data car prefix plate
 */
export const getCarPrefixPlate = () => ({
    type: GET_CAR_PREFIX_PLATE
});
/**
 * @function getCarPrefixPlateSuccess
 * @returns {Object} Object tipe state GET_CAR_PREFIX_PLATE_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data car prefix plate
 */
export const getCarPrefixPlateSuccess = response => ({
    type: GET_CAR_PREFIX_PLATE_SUCCESS,
    payload: response
});
/**
 * @function getCarPrefixPlateFailure
 * @returns {Object} Object tipe state GET_CAR_PREFIX_PLATE_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data car prefix plate
 */
export const getCarPrefixPlateFailure = error => ({
    type: GET_CAR_PREFIX_PLATE_FAILURE,
    payload: error
});
/**
 * @function openCarPrefixPlate
 * @returns {Object} Object tipe state OPEN_DIALOG_CAR_PREFIX_PLATE
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar car prefix plate
 */
export const openCarPrefixPlate = dialog => ({
    type: OPEN_DIALOG_CAR_PREFIX_PLATE,
    payload: dialog
});
/**
 * @function closeCarPrefixPlate
 * @returns {Object} Object tipe state CLOSE_DIALOG_CAR_PREFIX_PLATE
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar car prefix plate
 */
export const closeCarPrefixPlate = dialog => ({
    type: CLOSE_DIALOG_CAR_PREFIX_PLATE,
    payload: dialog
});