/**
 * @author: Artha Prihardana 
 * @Date: 2018-11-06 10:30:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-06 13:24:35
 */
import { GET_CHECK_TOKEN, GET_CHECK_TOKEN_SUCCESS, GET_CHECK_TOKEN_FAILURE, UPDATE_REFRESH_TOKEN, UPDATE_REFRESH_TOKEN_SUCCESS, UPDATE_REFRESH_TOKEN_FAILURE } from "./type";
/**
 * @function getCheckToken
 * @returns {Object} Object tipe state GET_CHECK_TOKEN
 * @description Reducer state untuk cek token
 */
export const getCheckToken = () => ({
    type: GET_CHECK_TOKEN
});
/**
 * @function getCheckTokenSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state GET_CHECK_TOKEN_SUCCESS
 * @description Reducer state untuk response dari cek token
 */
export const getCheckTokenSuccess = response => ({
    type: GET_CHECK_TOKEN_SUCCESS,
    payload: response
});
/**
 * @function getCheckTokenFailure
 * @param {(Object|String)} error - error response dari REST API
 * @returns {Object} Object tipe state GET_CHECK_TOKEN_FAILURE
 * @description Reducer state untuk error response dari cek token
 */
export const getCheckTokenFailure = error => ({
    type: GET_CHECK_TOKEN_FAILURE,
    payload: error
});
/**
 * @function updateRefreshToken
 * @returns {Object} Object tipe state UPDATE_REFRESH_TOKEN
 * @description Reducer state untuk refresh token
 */
export const updateRefreshToken = () => ({
    type: UPDATE_REFRESH_TOKEN
});
/**
 * @function updateRefreshTokenSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state UPDATE_REFRESH_TOKEN_SUCCESS
 * @description Reducer state untuk response dari refresh token
 */
export const updateRefreshTokenSuccess = response => ({
    type: UPDATE_REFRESH_TOKEN_SUCCESS,
    payload: response
});
/**
 * @function updateRefreshTokenFailure
 * @param {(Object|String)} error - error response dari REST API
 * @returns {Object} Object tipe state UPDATE_REFRESH_TOKEN_FAILURE
 * @description Reducer state untuk error response dari refresh token
 */
export const updateRefreshTokenFailure = error => ({
    type: UPDATE_REFRESH_TOKEN_FAILURE,
    payload: error
});