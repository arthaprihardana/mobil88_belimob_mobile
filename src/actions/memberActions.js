/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 13:34:26 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-30 13:34:26 
 */
import {GET_MEMBER, GET_MEMBER_SUCCESS, GET_MEMBER_FAILURE, UPDATE_MEMBER, UPDATE_MEMBER_SUCCESS, UPDATE_MEMBER_FAILURE, UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAILURE, UPDATE_FOTO_PROFIL, UPDATE_FOTO_PROFIL_SUCCESS, UPDATE_FOTO_PROFILE_FAILURE, UPDATE_PEMBAYARAN, UPDATE_PEMBAYARAN_FAILURE, UPDATE_PEMBAYARAN_SUCCESS} from './type';
/**
 * @function getMember
 * @param {String} member_code - Member Code
 */
export const getMember = member_code => ({
    type: GET_MEMBER,
    payload: member_code
});
/**
 * @function getMemberSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state GET_MEMBER_SUCCESS
 * @description Reducer state untuk response dari get data member
 */
export const getMemberSuccess = response => ({
    type: GET_MEMBER_SUCCESS,
    payload: response
});
/**
 * @function getMemberFailure
 * @returns {Object} Object tipe state GET_MEMBER_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data member
 */
export const getMemberFailure = error => ({
    type: GET_MEMBER_FAILURE,
    payload: error
});
/**
 * @function updateMember
 * @param {Object} member - Parameter form data yang akan dikirimkan saat request update member
 * @description Reducer state untuk request update member
 */
export const updateMember = member => ({
    type: UPDATE_MEMBER,
    payload: member
});
/**
 * @function updateMemberSuccess
 * @returns {Object} Object tipe state UPDATE_MEMBER_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari update member
 */
export const updateMemberSuccess = response => ({
    type: UPDATE_MEMBER_SUCCESS,
    payload: response
});
/**
 * @function updateMemberFailure
 * @returns {Object} Object tipe state UPDATE_MEMBER_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari update member
 */
export const updateMemberFailure = error => ({
    type: UPDATE_MEMBER_FAILURE,
    payload: error
});
/**
 * @function updatePassword
 * @param {Object} reset_password - Parameter form data yang akan dikirimkan saat request update password
 * @description Reducer state untuk request update password
 */
export const updatePassword = reset_password => ({
    type: UPDATE_PASSWORD,
    payload: reset_password
});
/**
 * @function updatePasswordSuccess
 * @returns {Object} Object tipe state UPDATE_PASSWORD_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari update password
 */
export const updatePasswordSuccess = response => ({
    type: UPDATE_PASSWORD_SUCCESS,
    payload: response
});
/**
 * @function updatePasswordFailure
 * @returns {Object} Object tipe state UPDATE_PASSWORD_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari update password
 */
export const updatePasswordFailure = error => ({
    type: UPDATE_PASSWORD_FAILURE,
    payload: error
});
/**
 * @function updateFotoProfil
 * @param {Object} foto - Parameter yang dikirimkan saat request update Foto Profil
 * @description Reducer state untuk request update foto profil
 */
export const updateFotoProfil = foto => ({
    type: UPDATE_FOTO_PROFIL,
    payload: foto
});
/**
 * @function updateFotoProfilSuccess
 * @returns {Object} Object tipe state UPDATE_FOTO_PROFIL_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari update foto profil
 */
export const updateFotoProfilSuccess = response => ({
    type: UPDATE_FOTO_PROFIL_SUCCESS,
    payload: response
});
/**
 * @function updateFotoProfilFailure
 * @returns {Object} Object tipe state UPDATE_FOTO_PROFILE_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari update foto profil
 */
export const updateFotoProfilFailure = error => ({
    type: UPDATE_FOTO_PROFILE_FAILURE,
    payload: error
});
/**
 * @function updatePembayaran
 * @param {Object} pembayaran - Parameter form data yang akan dikirimkan saat request update pembayaran
 * @description Reducer state untuk request update pembayaran
 */
export const updatePembayaran = pembayaran => ({
    type: UPDATE_PEMBAYARAN,
    payload: pembayaran
});
/**
 * @function updatePembayaranSuccess
 * @returns {Object} Object tipe state UPDATE_PEMBAYARAN_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari update pembayaran
 */
export const updatePembayaranSuccess = response => ({
    type: UPDATE_PEMBAYARAN_SUCCESS,
    payload: response
});
/**
 * @function updatePembayaranFailure
 * @returns {Object} Object tipe state UPDATE_PEMBAYARAN_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari update pembayaran
 */
export const updatePembayaranFailure = error => ({
    type: UPDATE_PEMBAYARAN_FAILURE,
    payload: error
});