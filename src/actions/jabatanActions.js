/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-27 14:04:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 14:06:45
 */
import { GET_JABATAN, GET_JABATAN_SUCCESS, GET_JABATAN_FAILURE, OPEN_DIALOG_JABATAN, CLOSE_DIALOG_JABATAN } from "./type";
/**
 * @function getJabatan
 * @returns {Object} Object tipe state GET_JABATAN
 * @description Reducer state untuk request get data jabatan / posisi
 */
export const getJabatan = () => ({
    type: GET_JABATAN
})
/**
 * @function getJabatanSuccess
 * @returns {Object} Object tipe state GET_JABATAN_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data jabatan / posisi
 */
export const getJabatanSuccess = response => ({
    type: GET_JABATAN_SUCCESS,
    payload: response
});
/**
 * @function getJabatanFailure
 * @returns {Object} Object tipe state GET_JABATAN_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data jabatan / posisi
 */
export const getJabatanFailure = error => ({
    type: GET_JABATAN_FAILURE,
    payload: error
});
/**
 * @function openDialogJabatan
 * @returns {Object} Object tipe state OPEN_DIALOG_JABATAN
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar jabatan / posisi
 */
export const openDialogJabatan = dialog => ({
    type: OPEN_DIALOG_JABATAN,
    payload: dialog
});
/**
 * @function closeDialogJabatan
 * @returns {Object} Object tipe state CLOSE_DIALOG_JABATAN
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar jabatan / posisi
 */
export const closeDialogJabatan = dialog => ({
    type: CLOSE_DIALOG_JABATAN,
    payload: dialog
});