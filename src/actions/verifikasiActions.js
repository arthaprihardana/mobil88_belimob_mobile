/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 09:51:27 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-30 09:51:27 
 */
import {GET_VERIFIKASI, GET_VERIFIKASI_SUCCESS, GET_VERIFIKASI_FAILURE} from './type';
/**
 * @function getVerifikasi
 * @param {Object} verifikasi - Data yang dikirimkan untuk diverifikasi
 */
export const getVerifikasi = verifikasi => ({
    type: GET_VERIFIKASI,
    payload: verifikasi
});
/**
 * @function getVerifikasiSuccess
 * @param {Object} response - sukses response verifikasi
 */
export const getVerifikasiSuccess = response => ({
    type: GET_VERIFIKASI_SUCCESS,
    payload: response
});
/**
 * @function getVerifikasiFailure
 * @param {(Object|String)} error - error response verifikasi
 */
export const getVerifikasiFailure = error => ({
    type: GET_VERIFIKASI_FAILURE,
    payload: error
});