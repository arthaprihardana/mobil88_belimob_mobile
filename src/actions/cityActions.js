/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:36:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-19 10:42:35
 */
import {GET_CITY, GET_CITY_SUCCESS, GET_CITY_FAILURE, OPEN_DIALOG_CITY, CLOSE_DIALOG_CITY} from './type';
/**
 * @function getCity
 * @returns {Object} Object tipe state GET_CITY
 * @param {String} ProvinsiId - ID dari Provinsi yang terpilih
 * @description Reducer state untuk request get data city
 */
export const getCity = (ProvinsiId) => ({
    type: GET_CITY,
    payload: ProvinsiId
});
/**
 * @function getCitySuccess
 * @returns {Object} Object tipe state GET_CITY_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data city
 */
export const getCitySuccess = response => ({
    type: GET_CITY_SUCCESS,
    payload: response
});
/**
 * @function getCityFailure
 * @returns {Object} Object tipe state GET_CITY_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data city
 */
export const getCityFailure = error => ({
    type: GET_CITY_FAILURE,
    payload: error
});
/**
 * @function openCity
 * @returns {Object} Object tipe state OPEN_DIALOG_CITY
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar city
 */
export const openCity = dialog => ({
    type: OPEN_DIALOG_CITY,
    payload: dialog
});
/**
 * @function closeCity
 * @returns {Object} Object tipe state CLOSE_DIALOG_CITY
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar city
 */
export const closeCity = dialog => ({
    type: CLOSE_DIALOG_CITY,
    payload: dialog
});