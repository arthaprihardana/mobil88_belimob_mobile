/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-28 14:28:49 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-09-28 14:28:49 
 */
import { OPEN_DIALOG_RESPONSE, CLOSE_DIALOG_RESPONSE } from "./type";
/**
 * @function openDialogResponse
 * @description Reducer state untuk membuka dialog response
 */
export const openDialogResponse = () => ({
    type: OPEN_DIALOG_RESPONSE
});
/**
 * @function closeDialogResponse
 * @description Reducer state untuk menutup dialog response
 */
export const closeDialogResponse = () => ({
    type: CLOSE_DIALOG_RESPONSE
});