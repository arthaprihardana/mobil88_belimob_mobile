/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:11:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-19 14:23:31
 */
import {GET_CAR_BRAND, GET_CAR_BRAND_SUCCESS, GET_CAR_BRAND_FAILURE, OPEN_DIALOG_CAR_BRAND, CLOSE_DIALOG_CAR_BRAND} from './type';
/**
 * @function getCarBrand
 * @returns {Object} Object tipe state GET_CAR_BRAND
 * @description Reducer state untuk request get data car brand
 */
export const getCarBrand = () => ({
    type: GET_CAR_BRAND
});
/**
 * @function getCarBrandSuccess
 * @returns {Object} Object tipe state GET_CAR_BRAND_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data car brand
 */
export const getCarBrandSuccess = response => ({
    type: GET_CAR_BRAND_SUCCESS,
    payload: response
});
/**
 * @function getCarBrandFailure
 * @returns {Object} Object tipe state GET_CAR_BRAND_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data car brand
 */
export const getCarBrandFailure = error => ({
    type: GET_CAR_BRAND_FAILURE,
    payload: error
});
/**
 * @function openCarBrand
 * @returns {Object} Object tipe state OPEN_DIALOG_CAR_BRAND
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar car brand
 */
export const openCarBrand = dialog => ({
    type: OPEN_DIALOG_CAR_BRAND,
    payload: dialog
});
/**
 * @function closeCarBrand
 * @returns {Object} Object tipe state CLOSE_DIALOG_CAR_BRAND
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar car brand
 */
export const closeCarBrand = dialog => ({
    type: CLOSE_DIALOG_CAR_BRAND,
    payload: dialog
});