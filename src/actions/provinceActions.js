/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:35:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-19 09:58:54
 */
import { GET_PROVINCE, GET_PROVINCE_SUCCESS, GET_PROVINCE_FAILURE, OPEN_DIALOG_PROVINCE, CLOSE_DIALOG_PROVINCE } from "./type";
/**
 * @function getProvince
 * @returns {Object} Object tipe state GET_PROVINCE
 * @description Reducer state untuk request get data provinsi
 */
export const getProvince = () => ({
    type: GET_PROVINCE
});
/**
 * @function getProvinceSuccess
 * @returns {Object} Object tipe state GET_PROVINCE_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data provinsi
 */
export const getProvinceSuccess = response => ({
    type: GET_PROVINCE_SUCCESS,
    payload: response
});
/**
 * @function getProvinceFailure
 * @returns {Object} Object tipe state GET_PROVINCE_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data provinsi
 */
export const getProvinceFailure = error => ({
    type: GET_PROVINCE_FAILURE,
    payload: error
});
/**
 * @function openProvince
 * @returns {Object} Object tipe state OPEN_DIALOG_PROVINCE
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar provinsi
 */
export const openProvince = dialog => ({
    type: OPEN_DIALOG_PROVINCE,
    payload: dialog
});
/**
 * @function closeProvince
 * @returns {Object} Object tipe state CLOSE_DIALOG_PROVINCE
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar provinsi
 */
export const closeProvince = dialog => ({
    type: CLOSE_DIALOG_PROVINCE,
    payload: dialog
});