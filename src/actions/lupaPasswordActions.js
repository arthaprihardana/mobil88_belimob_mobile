/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-10 11:36:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-10 13:58:02
 */
import { GET_LUPA_PASSWORD, GET_LUPA_PASSWORD_SUCCESS, GET_LUPA_PASSWORD_FAILURE, GET_RESET_PASSWORD, GET_RESET_PASSWORD_SUCCESS, GET_RESET_PASSWORD_FAILURE } from "./type";
/**
 * @function getLupaPassword
 * @param {Object} FormData - Form data yang dikirimkan
 * @returns {Object} Object tipe state GET_LUPA_PASSWORD
 * @description Reducer state untuk request lupa password
 */
export const getLupaPassword = FormData => ({
    type: GET_LUPA_PASSWORD,
    payload: FormData
});
/**
 * @function getLupaPasswordSuccess
 * @returns {Object} Object tipe state GET_LUPA_PASSWORD_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get lupa password
 */
export const getLupaPasswordSuccess = response => ({
    type: GET_LUPA_PASSWORD_SUCCESS,
    payload: response
});
/**
 * @function getLupaPasswordFailure
 * @returns {Object} Object tipe state GET_LUPA_PASSWORD_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get lupa password
 */
export const getLupaPasswordFailure = error => ({
    type: GET_LUPA_PASSWORD_FAILURE,
    payload: error
});
/**
 * @function getResetPassword
 * @param {Object} FormData - Form data yang dikirimkan untuk request reset password
 * @returns {Object} Object tipe state GET_RESET_PASSWORD
 * @description Reducer state untuk request reset password
 */
export const getResetPassword = FormData => ({
    type: GET_RESET_PASSWORD,
    payload: FormData
});
/**
 * @function getResetPasswordSuccess
 * @returns {Object} Object tipe state GET_RESET_PASSWORD_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari reset password
 */
export const getResetPasswordSuccess = response => ({
    type: GET_RESET_PASSWORD_SUCCESS,
    payload: response
});
/**
 * @function getResetPasswordFailure
 * @returns {Object} Object tipe state GET_RESET_PASSWORD_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari reset password
 */
export const getResetPasswordFailure = error => ({
    type: GET_RESET_PASSWORD_FAILURE,
    payload: error
});