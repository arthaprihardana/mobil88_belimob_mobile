/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-07 13:43:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-24 11:49:52
 */
import { GET_DASHBOARD, GET_DASHBOARD_SUCCESS, GET_DASHBOARD_FAILURE, OPEN_DIALOG_MONTH_YEAR, CLOSE_DIALOG_MONTH_YEAR } from "./type";
/**
 * @function getDashboard
 * @returns {Object} Object tipe state GET_DASHBOARD
 * @param {Object} param - Parameter filter yang dikirimkan (MemberCode, Month, Year)
 * @description Reducer state untuk request get data dashboard
 */
export const getDashboard = param => ({
    type: GET_DASHBOARD,
    payload: param
});
/**
 * @function getDashboardSuccess
 * @returns {Object} Object tipe state GET_DASHBOARD_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data dashboard
 */
export const getDashboardSuccess = response => ({
    type: GET_DASHBOARD_SUCCESS,
    payload: response
});
/**
 * @function getDashboardFailure
 * @returns {Object} Object tipe state GET_DASHBOARD_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data dashboard
 */
export const getDashboardFailure = error => ({
    type: GET_DASHBOARD_FAILURE,
    payload: error
});
/**
 * @function openDialogMonthYear
 * @returns {Object} Object tipe state OPEN_DIALOG_MONTH_YEAR
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog filter tahun / bulan
 */
export const openDialogMonthYear = tipe => ({
    type: OPEN_DIALOG_MONTH_YEAR,
    payload: tipe
});
/**
 * @function closeDialogMonthYear
 * @returns {Object} Object tipe state CLOSE_DIALOG_MONTH_YEAR
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog filter tahun / bulan
 */
export const closeDialogMonthYear = value => ({
    type: CLOSE_DIALOG_MONTH_YEAR,
    payload: value
});