/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 11:29:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-15 11:55:49
 */
import { NetInfo, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { APIM_KEY } from "../constant";
/**
 * Fungsi fetch REST API yang ditambahkan fungsi Timeout didalam nya
 * @function fetchWithTimeout
 * @param {String} url - End Point Url
 * @param {Object} options - Optional data yang akan dikrim
 * @returns {Promise}
 * 
 * @example 
 * return fetchWithTimeout(END_POINT_API_URL, {
 *      "method": "GET/POST/PUT",
 *      "headers": { 
 *          "Content-Type": "application/json",
 *          "Authorization": "Bearer xxx
 *      }
 * })
 * .then(response => response)
 * .catch(error => error);
 */
export default fetchWithTimeout = (url, options) => {
    const FETCH_TIMEOUT = 30000;
    let didTimeOut = false;

    return new Promise((resolve, reject) => {

        const timeout = setTimeout(function() {
            didTimeOut = true;
            reject(new Error('Request timed out'));
        }, FETCH_TIMEOUT);

        let headers = Object.assign(options.headers, { 'Ocp-Apim-Subscription-Key': APIM_KEY });
        let opts = Object.assign(options, { headers: headers });
        fetch(url, opts).then(response => {
            clearTimeout(timeout);
            if(!didTimeOut) {
                return response.json();
            }
        }).then(responseJson => {
            if(responseJson.status) {
                resolve(responseJson);
            } else {
                let reg = /token/i;
                if(responseJson.errorMsg !== null && reg.test(responseJson.errorMsg)) {
                    AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                    Actions.reset('login', { invalid: true });
                } else {
                    resolve(responseJson);
                }
            }
        }).catch(err => {
            console.log('fetch failed! ', err);
            if(!didTimeOut) {
                NetInfo.getConnectionInfo().then((connectionInfo) => {
                    if(connectionInfo.type == "none") {
                        resolve({ connectionInfo: connectionInfo.type })
                    } else {
                        reject(err);
                    }
                });
            } else {
                if(didTimeOut) return;
                reject(err);
            }
        });
    });
}