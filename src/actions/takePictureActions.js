/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-21 14:46:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 14:50:47
 */
import { TAKE_PICTURE, TAKE_PICTURE_PROFILE_SUCCESS, TAKE_PICTURE_PROFILE_FAILURE , TAKE_PICTURE_KTP, TAKE_PICTURE_KTP_SUCCESS, TAKE_PICTURE_KTP_FAILURE, TAKE_PICTURE_CARD_ID, TAKE_PICTURE_CARD_ID_SUCCESS, TAKE_PICTURE_CARD_ID_FAILURE, OPEN_CAMERA, CLOSE_CAMERA, OPEN_CAMERA_ROLL, CLOSE_CAMERA_ROLL} from "./type";
/**
 * @function openCamera
 * @param {String} type - Tipe field yang akan menggunakan fitur kamera
 * @description Reducer state untuk membuka Kamera
 */
export const openCamera = type => ({
    type: OPEN_CAMERA,
    payload: type
});
/**
 * @function closeCamera
 * @param {String} type - Tipe field yang akan menggunakan fitur kamera
 * @description Reducer state untuk menutup Kamera
 */
export const closeCamera = type => ({
    type: CLOSE_CAMERA,
    payload: type
});
/**
 * @function openCameraRoll
 * @param {String} type - Tipe field yang akan menggunakan fitur kamera roll / galeri
 * @description Reducer state untuk membuka Kamera roll / galeri
 */
export const openCameraRoll = type => ({
    type: OPEN_CAMERA_ROLL,
    payload: type
});
/**
 * @function closeCameraRoll
 * @param {String} type - Tipe field yang akan menggunakan fitur kamera roll / galeri
 * @description Reducer state untuk menutup Kamera roll / galeri
 */
export const closeCameraRoll = type => ({
    type: CLOSE_CAMERA_ROLL,
    payload: type
});
/**
 * @function takePictureProfile
 * @description Handle untuk mengambil gambar profil
 */
export const takePictureProfile = () => ({
    type: TAKE_PICTURE
});
/**
 * @function takePictureProfileSuccess
 * @param {Object} response - Sukses response dalam pengambilan gambar profil
 */
export const takePictureProfileSuccess = response => ({
    type: TAKE_PICTURE_PROFILE_SUCCESS,
    payload: response
});
/**
 * @function takePictureProfileFailure
 * @param {(Object|String)} error - Error response dalam pengambilan gambar profil
 */
export const takePictureProfileFailure = error => ({
    type: TAKE_PICTURE_PROFILE_FAILURE,
    payload: error
});
/**
 * @function takePictureKtp
 * @description Handle untuk mengambil gambar ktp
 */
export const takePictureKtp = () => ({
    type: TAKE_PICTURE_KTP
});
/**
 * @function takePictureKtpSuccess
 * @param {Object} response - Sukses response dalam pengambilan gambar ktp
 */
export const takePictureKtpSuccess = response => ({
    type: TAKE_PICTURE_KTP_SUCCESS,
    payload: response
});
/**
 * @function takePictureKtpFailure
 * @param {(Object|String)} error - Error response dalam pengambilan gambar ktp
 */
export const takePictureKtpFailure = error => ({
    type: TAKE_PICTURE_KTP_FAILURE,
    payload: error
});
/**
 * @function takePictureCardId
 * @description Handle untuk mengambil gambar id card
 */
export const takePictureCardId = () => ({
    type: TAKE_PICTURE_CARD_ID
});
/**
 * @function takePictureCardIdSuccess
 * @param {Object} response - Sukses response dalam pengambilan gambar id card
 */
export const takePictureCardIdSuccess = response => ({
    type: TAKE_PICTURE_CARD_ID_SUCCESS,
    payload: response
});
/**
 * @function takePrictureCardIdFailure
 * @param {(Object|String)} error - Error response dalam pengambilan gambar id card
 */
export const takePrictureCardIdFailure = error => ({
    type: TAKE_PICTURE_CARD_ID_FAILURE,
    payload: error
});