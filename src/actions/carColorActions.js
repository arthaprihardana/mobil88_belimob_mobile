/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:23:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-19 14:24:07
 */
import { GET_CAR_COLOR, GET_CAR_COLOR_SUCCESS, GET_CAR_COLOR_FAILURE, OPEN_DIALOG_CAR_COLOR , CLOSE_DIALOG_CAR_COLOR} from "./type";
/**
 * @function getCarColor
 * @returns {Object} Object tipe state GET_CAR_COLOR
 * @description Reducer state untuk request get data car color
 */
export const getCarColor = () => ({
    type: GET_CAR_COLOR
});
/**
 * @function getCarColorSuccess
 * @returns {Object} Object tipe state GET_CAR_COLOR_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data car color
 */
export const getCarColorSuccess = response => ({
    type: GET_CAR_COLOR_SUCCESS,
    payload: response
});
/**
 * @function getCarColorFailure
 * @returns {Object} Object tipe state GET_CAR_COLOR_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data car color
 */
export const getCarColorFailure = error => ({
    type: GET_CAR_COLOR_FAILURE,
    payload: error
});
/**
 * @function openCarColor
 * @returns {Object} Object tipe state OPEN_DIALOG_CAR_COLOR
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar car color
 */
export const openCarColor = dialog => ({
    type: OPEN_DIALOG_CAR_COLOR,
    payload: dialog
});
/**
 * @function closeCarColor
 * @returns {Object} Object tipe state CLOSE_DIALOG_CAR_COLOR
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar car color
 */
export const closeCarColor = dialog => ({
    type: CLOSE_DIALOG_CAR_COLOR,
    payload: dialog
});