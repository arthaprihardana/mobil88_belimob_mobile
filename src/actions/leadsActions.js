/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 10:25:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 14:27:00
 */
import { OPEN_DIALOG_FILTER, CLOSE_DIALOG_FILTER, OPEN_DIALOG_TAHUN_PABRIKAN, CLOSE_DIALOG_TAHUN_PABRIKAN, OPEN_DIALOG_FACELIFT_MODE, CLOSE_DIALOG_FACELIFT_MODE, OPEN_DIALOG_TIPE_STNK, CLOSE_DIALOG_TIPE_STNK , GET_LEADS, GET_LEADS_SUCCESS, GET_LEADS_FAILURE, GET_DETAIL_LEADS, GET_DETAIL_LEADS_SUCCESS, GET_DETAIL_LEADS_FAILURE, ADD_LEADS, ADD_LEADS_SUCCESS, ADD_LEADS_FAILURE, INSERT_DMS, INSERT_DMS_SUCCESS, INSERT_DMS_FAILURE, LEAD_NOT_APPOINTMENT, LEAD_NOT_APPOINTMENT_SUCCESS, LEAD_NOT_APPOINTMENT_FAILURE, LEAD_YES_APPOINTMENT ,LEAD_YES_APPOINTMENT_SUCCESS, LEAD_YES_APPOINTMENT_FAILURE, CANCEL_LEADS, CANCEL_LEADS_SUCCESS, CANCEL_LEADS_FAILURE, GET_LEAD_STATUS, GET_LEAD_STATUS_SUCCESS, GET_LEAD_STATUS_FAILURE, OPEN_DIALOG_LEAD_STATUS, CLOSE_DIALOG_LEAD_STATUS } from "./type";
/**
 * @function openDialogFilter
 * @param {Boolean} dialog - true/false
 */
export const openDialogFilter = dialog => ({
    type: OPEN_DIALOG_FILTER,
    payload: dialog
});
/**
 * @function closeDialogFilter
 * @param {Boolean} dialog - true/false
 */
export const closeDialogFilter = dialog => ({
    type: CLOSE_DIALOG_FILTER,
    payload: dialog
});
/**
 * @function openDialogTahunPabrikan
 * @param {Boolean} dialog - true/false
 */
export const openDialogTahunPabrikan = dialog => ({
    type: OPEN_DIALOG_TAHUN_PABRIKAN,
    payload: dialog
});
/**
 * @function closeDialogTahunPabrikan
 * @param {Boolean} dialog - true/false
 */
export const closeDialogTahunPabrikan = dialog => ({
    type: CLOSE_DIALOG_TAHUN_PABRIKAN,
    payload: dialog
});
/**
 * @function openDialogFacelift
 * @param {Boolean} dialog - true/false
 */
export const openDialogFacelift = dialog => ({
    type: OPEN_DIALOG_FACELIFT_MODE,
    payload: dialog
});
/**
 * @function closeDialogFacelift
 * @param {Boolean} dialog - true/false
 */
export const closeDialogFacelift = dialog => ({
    type: CLOSE_DIALOG_FACELIFT_MODE,
    payload: dialog
});
/**
 * @function openDialogTipeStnk
 * @param {Boolean} dialog - true/false
 */
export const openDialogTipeStnk = dialog => ({
    type: OPEN_DIALOG_TIPE_STNK,
    payload: dialog
});
/**
 * @function closeDialogTipeStnk
 * @param {Boolean} dialog - true/false
 */
export const closeDialogTipeStnk = dialog => ({
    type: CLOSE_DIALOG_TIPE_STNK,
    payload: dialog
});
/**
 * @function getLeads
 * @param {Object} params - Parameter yang dikirimkan saat request Leads (MemberCode,Page)
 * @returns {Object} Object tipe state GET_LEADS
 * @description Reducer state untuk request get data lead
 */
export const getLeads = params => ({
    type: GET_LEADS,
    payload: params
});
/**
 * @function getLeadsSuccess
 * @returns {Object} Object tipe state GET_LEADS_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data lead
 */
export const getLeadsSuccess = response => ({
    type: GET_LEADS_SUCCESS,
    payload: response
});
/**
 * @function getLeadsFailure
 * @returns {Object} Object tipe state GET_LEADS_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data lead
 */
export const getLeadsFailure = error => ({
    type: GET_LEADS_FAILURE,
    payload: error
});
/**
 * @function getLeadsDetail
 * @param {String} refCode - Reference Code Lead
 * @returns {Object} Object tipe state GET_DETAIL_LEADS
 * @description Reducer state untuk response dari get data detail lead
 */
export const getLeadsDetail = refCode => ({
    type: GET_DETAIL_LEADS,
    payload: refCode
});
/**
 * @function getLeadsDetailSuccess
 * @returns {Object} Object tipe state GET_DETAIL_LEADS_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data detail lead
 */
export const getLeadsDetailSuccess = response => ({
    type: GET_DETAIL_LEADS_SUCCESS,
    payload: response
});
/**
 * @function getLeadsDetailFailure
 * @returns {Object} Object tipe state GET_DETAIL_LEADS_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data detail lead
 */
export const getLeadsDetailFailure = error => ({
    type: GET_DETAIL_LEADS_FAILURE,
    payload: error
});
/**
 * @function addLeads
 * @param {Object} FormData - Forma data yang akan dikirim saat add Lead
 * @returns {Object} Object tipe state ADD_LEADS
 * @description Reducer state untuk request add lead
 */
export const addLeads = FormData => ({
    type: ADD_LEADS,
    payload: FormData
});
/**
 * @function addLeadsSuccess
 * @returns {Object} Object tipe state ADD_LEADS_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari add lead
 */
export const addLeadsSuccess = response => ({
    type: ADD_LEADS_SUCCESS,
    payload: response
});
/**
 * @function addLeadsFailure
 * @returns {Object} Object tipe state ADD_LEADS_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari add lead
 */
export const addLeadsFailure = error => ({
    type: ADD_LEADS_FAILURE,
    payload: error
});
/**
 * @function leadsNotAppointment
 * @param {Object} FormData - Object data yang dikirimkan saat request untuk Not Appoinment
 */
export const leadsNotAppointment = FormData => ({
    type: LEAD_NOT_APPOINTMENT,
    payload: FormData
});
/**
 * @function leadsNotAppointmentSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state LEAD_NOT_APPOINTMENT_SUCCESS
 * @description Reducer state untuk response dari response Lead Not Appointment
 */
export const leadsNotAppointmentSuccess = response => ({
    type: LEAD_NOT_APPOINTMENT_SUCCESS,
    payload: response
});
/**
 * @function leadsNotAppointmentFailure
 * @param {Object} response - error response dari REST API
 * @returns {Object} Object tipe state LEAD_NOT_APPOINTMENT_FAILURE
 * @description Reducer state untuk error response dari response Lead Not Appointment
 */
export const leadsNotAppointmentFailure = error => ({
    type: LEAD_NOT_APPOINTMENT_FAILURE,
    payload: error
});
/**
 * @function leadsYesAppointment
 * @param {Object} FormData - Object data yang dikirimkan saat request untuk Yes Appoinment
 */
export const leadsYesAppointment = FormData => ({
    type: LEAD_YES_APPOINTMENT,
    payload: FormData
});
/**
 * @function leadsYesAppointmentSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state LEAD_YES_APPOINTMENT_SUCCESS
 * @description Reducer state untuk response dari response Lead Yes Appointment
 */
export const leadsYesAppointmentSuccess = response => ({
    type: LEAD_YES_APPOINTMENT_SUCCESS,
    payload: response
});
/**
 * @function leadsYesAppointmentFailure
 * @param {Object} response - error response dari REST API
 * @returns {Object} Object tipe state LEAD_YES_APPOINTMENT_FAILURE
 * @description Reducer state untuk error response dari response Lead Yes Appointment
 */
export const leadsYesAppointmentFailure = error => ({
    type: LEAD_YES_APPOINTMENT_FAILURE,
    payload: error
});
/**
 * @function cancelLeads
 * @param {Object} FormData - Object data yang dikirimkan saat request cancel Lead
 */
export const cancelLeads = FormData => ({
    type: CANCEL_LEADS,
    payload: FormData
});
/**
 * @function cancelLeadsSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state CANCEL_LEADS_SUCCESS
 * @description Reducer state untuk response dari response Cancel Lead
 */
export const cancelLeadsSuccess = response => ({
    type: CANCEL_LEADS_SUCCESS,
    payload: response
});
/**
 * @function cancelLeadsFailure
 * @param {(Object|String)} error - error response dari REST API
 * @returns {Object} Object tipe state CANCEL_LEADS_FAILURE
 * @description Reducer state untuk response dari error response Cancel Lead
 */
export const cancelLeadsFailure = error => ({
    type: CANCEL_LEADS_FAILURE,
    payload: error
});
/**
 * @function getLeadStatus
 */
export const getLeadStatus = () => ({
    type: GET_LEAD_STATUS
});
/**
 * @function getLeadStatusSuccess
 * @param {Object} response - sukses response dari REST API
 * @returns {Object} Object tipe state GET_LEAD_STATUS_SUCCESS
 * @description Reducer state untuk response dari lead status
 */
export const getLeadStatusSuccess = response => ({
    type: GET_LEAD_STATUS_SUCCESS,
    payload: response
});
/**
 * @function getLeadStatusFailure
 * @param {(Object|String)} error - error response dari REST API
 * @returns {Object} Object tipe state GET_LEAD_STATUS_FAILURE
 * @description Reducer state untuk response dari lead status
 */
export const getLeadStatusFailure = error => ({
    type: GET_LEAD_STATUS_FAILURE,
    payload: error
});

export const openDialogLeadStatus = dialog => ({
    type: OPEN_DIALOG_LEAD_STATUS,
    payload: dialog
});

export const closeDialogLeadStatus = dialog => ({
    type: CLOSE_DIALOG_LEAD_STATUS,
    payload: dialog
})