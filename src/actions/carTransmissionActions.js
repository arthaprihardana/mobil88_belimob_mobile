/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:21:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-17 13:33:38
 */
import {GET_CAR_TRANSMISSION, GET_CAR_TRANSMISSION_SUCCESS, GET_CAR_TRANSMISSION_FAILURE, OPEN_DIALOG_CAR_TRANSMISSION, CLOSE_DIALOG_CAR_TRANSMISSION} from './type';
/**
 * @function getCarTransmission
 * @returns {Object} Object tipe state GET_CAR_TRANSMISSION
 * @description Reducer state untuk request get data transmission
 */
export const getCarTransmission = () => ({
    type: GET_CAR_TRANSMISSION
});
/**
 * @function getCarTransmissionSuccess
 * @returns {Object} Object tipe state GET_CAR_TRANSMISSION_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data transmission
 */
export const getCarTransmissionSuccess = response => ({
    type: GET_CAR_TRANSMISSION_SUCCESS,
    payload: response
});
/**
 * @function getCarTransmissionFailure
 * @returns {Object} Object tipe state GET_CAR_TRANSMISSION_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data transmission
 */
export const getCarTransmissionFailure = error => ({
    type: GET_CAR_TRANSMISSION_FAILURE,
    payload: error
});
/**
 * @function openCarTransmission
 * @returns {Object} Object tipe state OPEN_DIALOG_CAR_TRANSMISSION
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar car transmission
 */
export const openCarTransmission = dialog => ({
    type: OPEN_DIALOG_CAR_TRANSMISSION,
    payload: dialog
});
/**
 * @function closeCarTransmission
 * @returns {Object} Object tipe state CLOSE_DIALOG_CAR_TRANSMISSION
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar car transmission
 */
export const closeCarTransmission = dialog => ({
    type: CLOSE_DIALOG_CAR_TRANSMISSION,
    payload: dialog
});