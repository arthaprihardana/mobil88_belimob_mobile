/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:27:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-09 14:22:41
 */
import { GET_MRP, GET_MRP_SUCCESS, GET_MRP_FAILURE, GET_MRP_DESCRIPTION, GET_MRP_DESCRIPTION_SUCCESS, GET_MRP_DESCRIPTION_FAILURE , OPEN_DIALOG_MRP_DESCRIPTION, CLOSE_DIALOG_MRP_DESCRIPTION} from "./type";
/**
 * @function getMrp
 * @param {Object} params - Parameter yang dikirimkan saat request MRP
 * @description Reducer state untuk request get data MRP
 */
export const getMrp = (params) => ({
    type: GET_MRP,
    payload: params
});
/**
 * @function getMrpSuccess
 * @returns {Object} Object tipe state GET_MRP_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data MRP
 */
export const getMrpSuccess = response => ({
    type: GET_MRP_SUCCESS,
    payload: response
});
/**
 * @function getMrpFailure
 * @returns {Object} Object tipe state GET_MRP_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data MRP
 */
export const getMrpFailure = error => ({
    type: GET_MRP_FAILURE,
    payload: error
});
/**
 * @function getMrpDescription
 * @description Reducer state untuk error response dari get data MRP Description
 */
export const getMrpDescription = () => ({
    type: GET_MRP_DESCRIPTION
});
/**
 * @function getMrpDescriptionSuccess
 * @returns {Object} Object tipe state GET_MRP_DESCRIPTION_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data MRP Description
 */
export const getMrpDescriptionSuccess = response => ({
    type: GET_MRP_DESCRIPTION_SUCCESS,
    payload: response
});
/**
 * @function getMrpDescriptionFailure
 * @returns {Object} Object tipe state GET_MRP_DESCRIPTION_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data MRP Description
 */
export const getMrpDescriptionFailure = error => ({
    type: GET_MRP_DESCRIPTION_FAILURE,
    payload: error
});
/**
 * @function openDialogMrpDescription
 * @param {Boolean} dialog - true/false
 */
export const openDialogMrpDescription = dialog => ({
    type: OPEN_DIALOG_MRP_DESCRIPTION
});
/**
 * @function closeDialogMrpDescription
 * @param {Boolean} dialog - true/false
 */
export const closeDialogMrpDescription = dialog => ({
    type: CLOSE_DIALOG_MRP_DESCRIPTION
});