/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 11:01:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-19 14:16:20
 */
import {TAKE_PICTURE, TAKE_PICTURE_SUCCESS, TAKE_PICTURE_FAILURE} from './type';
/**
 * @function takePicture
 * @returns {Object} Object tipe state TAKE_PICTURE
 * @param {Object} picture - Object data dari hasil ambil gambar (path, base64)
 * @description Reducer state untuk aksi ambil gambar
 */
export const takePicture = picture => ({
    type: TAKE_PICTURE,
    payload: picture
});
/**
 * @function takePictureSuccess
 * @returns {Object} Object tipe state TAKE_PICTURE_SUCCESS
 * @param {Object} response - Object data dari hasil ambil gambar (path, base64)
 * @description Reducer state untuk response ambil gambar
 */
export const takePictureSuccess = response => ({
    type: TAKE_PICTURE_SUCCESS,
    payload: response
});
/**
 * @function takePictureFailure
 * @returns {Object} Object tipe state TAKE_PICTURE_FAILURE
 * @param {(Object|String)} error - error response dari hasil ambil gambar
 * @description Reducer state untuk eror response ambil gambar
 */
export const takePictureFailure = error => ({
    type: TAKE_PICTURE_FAILURE,
    payload: error
});