/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 10:26:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-19 14:16:49
 */
import { GET_BANK , GET_BANK_SUCCESS, GET_BANK_FAILURE, OPEN_DIALOG_BANK, CLOSE_DIALOG_BANK} from "./type";
/**
 * @function getBank
 * @returns {Object} Object tipe state GET_BANK
 * @description Reducer state untuk request get data bank
 */
export const getBank = () => ({
    type: GET_BANK
});
/**
 * @function getBankSuccess
 * @returns {Object} Object tipe state GET_BANK_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data bank
 */
export const getBankSuccess = response => ({
    type: GET_BANK_SUCCESS,
    payload: response
});
/**
 * @function getBankFailure
 * @returns {Object} Object tipe state GET_BANK_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data bank
 */
export const getBankFailure = error => ({
    type: GET_BANK_FAILURE,
    payload: error
});
/**
 * @function openDialogBank
 * @returns {Object} Object tipe state OPEN_DIALOG_BANK
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar bank
 */
export const openDialogBank = dialog => ({
    type: OPEN_DIALOG_BANK,
    payload: dialog
});
/**
 * @function closeDialogBank
 * @returns {Object} Object tipe state CLOSE_DIALOG_BANK
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar bank
 */
export const closeDialogBank = dialog => ({
    type: CLOSE_DIALOG_BANK,
    payload: dialog
});