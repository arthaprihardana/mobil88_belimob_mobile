/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-25 14:31:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 10:46:49
 */
import { GET_INSENTIVE, GET_INSENTIVE_SUCCESS, GET_INSENTIVE_FAILURE, GET_TEAM_DOWNLINE , GET_TEAM_DOWNLINE_SUCCESS, GET_TEAM_DOWNLINE_FAILURE, GET_TEAM_LIST_LEAD_CLOSE, GET_TEAM_LIST_LEAD_CLOSE_SUCCESS, GET_TEAM_LIST_LEAD_CLOSE_FAILURE, GET_TEAM_LIST_LEAD_PROCESS, GET_TEAM_LIST_LEAD_PROCESS_SUCCESS, GET_TEAM_LIST_LEAD_PROCESS_FAILURE} from "./type";
/**
 * @function getInsentive
 * @param {String} MemberCode - Parameter Member Code
 */
export const getInsentive = MemberCode => ({
    type: GET_INSENTIVE,
    payload: MemberCode
})
/**
 * @function getInsentiveSuccess
 * @param {Object} response - Sukses response dari REST API
 */
export const getInsentiveSuccess = response => ({
    type: GET_INSENTIVE_SUCCESS,
    payload: response
});
/**
 * @function getInsentiveFailure
 * @param {(Object|String)} error - error response dari REST API
 */
export const getInsentiveFailure = error => ({
    type: GET_INSENTIVE_FAILURE,
    payload: error
});
/**
 * @function getTeamDownline
 * @param {String} MemberCode - Parameter member code
 */
export const getTeamDownline = MemberCode => ({
    type: GET_TEAM_DOWNLINE,
    payload: MemberCode
});
/**
 * @function getTeamDownlineSuccess
 * @param {Object} response - Sukses response dari REST API
 */
export const getTeamDownlineSuccess = response => ({
    type: GET_TEAM_DOWNLINE_SUCCESS,
    payload: response
});
/**
 * @function getTeamDownlineFailure
 * @param {(Object|String)} error - error response dari REST API
 */
export const getTeamDownlineFailure = error => ({
    type: GET_TEAM_DOWNLINE_FAILURE,
    payload: error
});
/**
 * @function getTeamListLeadClose
 * @param {String} MemberCode - Parameter member code
 */
export const getTeamListLeadClose = MemberCode => ({
    type: GET_TEAM_LIST_LEAD_CLOSE,
    payload: MemberCode
});
/**
 * @function getTeamListLeadCloseSuccess
 * @param {Object} response - Sukses response dari REST API
 */
export const getTeamListLeadCloseSuccess = response => ({
    type: GET_TEAM_LIST_LEAD_CLOSE_SUCCESS,
    payload: response
})
/**
 * @function getTeamListLeadCloseFailure
 * @param {(Object|String)} error - error response dari REST API
 */
export const getTeamListLeadCloseFailure = error => ({
    type: GET_TEAM_LIST_LEAD_CLOSE_FAILURE,
    payload: error
});
/**
 * @function getTeamListLeadsProcess
 * @param {String} MemberCode - Parameter member code
 */
export const getTeamListLeadsProcess = MemberCode => ({
    type: GET_TEAM_LIST_LEAD_PROCESS,
    payload: MemberCode
})
/**
 * @function getTeamListLeadsProcessSuccess
 * @param {Object} response - Sukses response dari REST API
 */
export const getTeamListLeadsProcessSuccess = response => ({
    type: GET_TEAM_LIST_LEAD_PROCESS_SUCCESS,
    payload: response
});
/**
 * @function getTeamListLeadsProcessFailure
 * @param {(Object|String)} error - error response dari REST API
 */
export const getTeamListLeadsProcessFailure = error => ({
    type: GET_TEAM_LIST_LEAD_PROCESS_FAILURE,
    payload: error
});