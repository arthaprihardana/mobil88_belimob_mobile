/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 14:29:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 10:18:30
 */
import {GET_REGISTER, GET_REGISTER_SUCCESS, GET_REGISTER_FAILURE, GET_DETAIL_REGISTER, GET_DETAIL_REGISTER_SUCCESS, GET_DETAIL_REGISTER_FAILURE} from './type';
/**
 * @function getRegister
 * @param {Object} user_register - Form data register user
 * @description Reducer state untuk request register user
 */
export const getRegister = user_register => ({
    type: GET_REGISTER,
    payload: user_register
});
/**
 * @function getRegisterSuccess
 * @returns {Object} Object tipe state GET_REGISTER_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response register user
 */
export const getRegisterSuccess = response => ({
    type: GET_REGISTER_SUCCESS,
    payload: response
});
/**
 * @function getRegisterFailure
 * @returns {Object} Object tipe state GET_REGISTER_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response register user
 */
export const getRegisterFailure = error => ({
    type: GET_REGISTER_FAILURE,
    payload: error
});
/**
 * @function getDetailRegister
 * @param {Object} user_register - Form data detail register user
 * @description Reducer state untuk request detail register user
 */
export const getDetailRegister = user_register => ({
    type: GET_DETAIL_REGISTER,
    payload: user_register
});
/**
 * @function getDetailRegisterSuccess
 * @returns {Object} Object tipe state GET_DETAIL_REGISTER_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response detail register user
 */
export const getDetailRegisterSuccess = response => ({
    type: GET_DETAIL_REGISTER_SUCCESS,
    payload: response
});
/**
 * @function getDetailRegisterFailure
 * @returns {Object} Object tipe state GET_DETAIL_REGISTER_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response detail register user
 */
export const getDetailRegisterFailure = error => ({
    type: GET_DETAIL_REGISTER_FAILURE,
    payload: error
});