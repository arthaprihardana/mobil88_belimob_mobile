/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:45:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-19 10:59:08
 */
import { GET_DISTRICT, GET_DISTRICT_SUCCESS, GET_DISTRICT_FAILURE, OPEN_DIALOG_DISTRICT, CLOSE_DIALOG_DISTRICT } from "./type";
/**
 * @function getDistrict
 * @returns {Object} Object tipe state GET_DISTRICT
 * @param {String} CityId - ID dari City yang terpilih
 * @description Reducer state untuk request get data district
 */
export const getDistrict = CityId => ({
    type: GET_DISTRICT,
    payload: CityId
});
/**
 * @function getDistrictSuccess
 * @returns {Object} Object tipe state GET_DISTRICT_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data district
 */
export const getDistrictSuccess = response => ({
    type: GET_DISTRICT_SUCCESS,
    payload: response
});
/**
 * @function getDistrictFailure
 * @returns {Object} Object tipe state GET_DISTRICT_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data district
 */
export const getDistrictFailure = error => ({
    type: GET_DISTRICT_FAILURE,
    payload: error
});
/**
 * @function openDistrict
 * @returns {Object} Object tipe state OPEN_DIALOG_DISTRICT
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar district
 */
export const openDistrict = dialog => ({
    type: OPEN_DIALOG_DISTRICT,
    payload: dialog
});
/**
 * @function closeDistrict
 * @returns {Object} Object tipe state CLOSE_DIALOG_DISTRICT
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar district
 */
export const closeDistrict = dialog =>  ({
    type: CLOSE_DIALOG_DISTRICT,
    payload: dialog
});