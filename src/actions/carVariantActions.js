/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:19:08 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-09-03 11:19:08 
 */
import { GET_CAR_VARIANT, GET_CAR_VARIANT_SUCCESS, GET_CAR_VARIANT_FAILURE, OPEN_DIALOG_CAR_VARIANT, CLOSE_DIALOG_CAR_VARIANT } from "./type";
/**
 * @function getCarVariant
 * @returns {Object} Object tipe state GET_CAR_VARIANT
 * @param {String} carModelId - ID dari Car Model yang dipilih
 * @description Reducer state untuk request get data car variant
 */
export const getCarVariant = (carModelId) => ({
    type: GET_CAR_VARIANT,
    payload: carModelId
});
/**
 * @function getCarvariantSuccess
 * @returns {Object} Object tipe state GET_CAR_VARIANT_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data car variant
 */
export const getCarvariantSuccess = response => ({
    type: GET_CAR_VARIANT_SUCCESS,
    payload: response
});
/**
 * @function getCarVariantFailure
 * @returns {Object} Object tipe state GET_CAR_VARIANT_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data car variant
 */
export const getCarVariantFailure = error => ({
    type: GET_CAR_VARIANT_FAILURE,
    payload: error
});
/**
 * @function openCarVariant
 * @returns {Object} Object tipe state OPEN_DIALOG_CAR_VARIANT
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar car variant
 */
export const openCarVariant = dialog => ({
    type: OPEN_DIALOG_CAR_VARIANT,
    payload: dialog
});
/**
 * @function closeCarVariant
 * @returns {Object} Object tipe state CLOSE_DIALOG_CAR_VARIANT
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar car variant
 */
export const closeCarVariant = dialog => ({
    type: CLOSE_DIALOG_CAR_VARIANT,
    payload: dialog
});