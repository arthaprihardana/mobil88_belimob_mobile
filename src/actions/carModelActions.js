/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:16:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-19 14:34:03
 */
import {GET_CAR_MODEL, GET_CAR_MODEL_SUCCESS, GET_CAR_MODEL_FAILURE, OPEN_DIALOG_CAR_MODEL, CLOSE_DIALOG_CAR_MODEL} from './type';
/**
 * @function getCarModel
 * @returns {Object} Object tipe state GET_CAR_MODEL
 * @params {String} carBrandId - ID dari Car Brand yang dipilih
 * @description Reducer state untuk request get data car model
 */
export const getCarModel = carBrandId => ({
    type: GET_CAR_MODEL,
    payload: carBrandId
});
/**
 * @function getCarModelSuccess
 * @returns {Object} Object tipe state GET_CAR_MODEL_SUCCESS
 * @param {Object} response - sukses response dari REST API
 * @description Reducer state untuk response dari get data car model
 */
export const getCarModelSuccess = response => ({
    type: GET_CAR_MODEL_SUCCESS,
    payload: response
});
/**
 * @function getCarModelFailure
 * @returns {Object} Object tipe state GET_CAR_BRAND_FAILURE
 * @param {(Object|String)} error - error response dari REST API
 * @description Reducer state untuk error response dari get data car brand
 */
export const getCarModelFailure = error => ({
    type: GET_CAR_MODEL_FAILURE,
    payload: error
});
/**
 * @function openCarModel
 * @returns {Object} Object tipe state OPEN_DIALOG_CAR_MODEL
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk membuka dialog daftar car model
 */
export const openCarModel = dialog => ({
    type: OPEN_DIALOG_CAR_MODEL,
    payload: dialog
});
/**
 * @function closeCarModel
 * @returns {Object} Object tipe state CLOSE_DIALOG_CAR_MODEL
 * @param {Boolean} dialog - true/false
 * @description Reducer state untuk menutup dialog daftar car model
 */
export const closeCarModel = dialog => ({
    type: CLOSE_DIALOG_CAR_MODEL,
    payload: dialog
});