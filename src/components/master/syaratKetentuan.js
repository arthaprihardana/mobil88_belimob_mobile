/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-19 10:47:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-19 10:48:58
 */
import React from 'react';
import { Modal, View, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as _ from 'lodash';
import { Text } from '../component'
import { PRIMARY_COLOR, WHITE } from '../../libraries/colors';
import { closeDialogMrpDescription } from '../../actions';

const SyaratKetentuan = props => (
    <Modal
        animationType="slide"
        transparent={false}
        visible={props.dialog}
        onRequestClose={() => props.closeDialogMrpDescription() }>
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <TouchableOpacity onPress={() => props.closeDialogMrpDescription()} style={styles.closeButton}>
                    <Ionicons name="md-close" color={WHITE} size={20} />
                </TouchableOpacity>
                <View style={styles.navBarTitle}>
                    <Text bold style={{ color: WHITE }}>{props.title}</Text>
                </View>
            </View>
            <View style={{ width: '100%', height: '92%' }}>
                {props.children}
            </View>
        </View>
    </Modal>
)

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        width: '100%', 
        flexDirection: 'row', 
        backgroundColor: PRIMARY_COLOR, 
        height: 56, 
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center',
        alignItems: 'center' 
    },
    navBarTitle: { 
        height: 56, 
        width: '80%', 
        alignItems: 'flex-start', 
        justifyContent: "center"
    }
})

const mapStateToProps = ({ mrpReducer }) => {
    const { dialog } = mrpReducer;
    return { dialog }
}

export default connect(mapStateToProps, {
    closeDialogMrpDescription
})(SyaratKetentuan);