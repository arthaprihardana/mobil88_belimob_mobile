/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-28 14:25:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-28 14:45:36
 */
import React from 'react';
import { Modal, View, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as _ from 'lodash';
import { Text } from '../component'
import { PRIMARY_COLOR, WHITE } from '../../libraries/colors';
import { closeDialogResponse } from '../../actions'

const Response = props => (
    <Modal
        animationType="slide"
        transparent={false}
        visible={props.dialog}
        onRequestClose={props.onClose}>
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <TouchableOpacity onPress={props.onClose} style={styles.closeButton}>
                    <Ionicons name="md-close" color={'#818182'} size={20} />
                </TouchableOpacity>
                <View style={styles.navBarTitle}>
                    <Text bold style={{ color: WHITE }}>{props.title}</Text>
                </View>
            </View>
            <Ionicons name="md-checkmark-circle" size={350} color="#c3e6cb" style={{ position: 'absolute', top: -20, right: -50 }} />
            <View>
                {props.children}
            </View>
        </View>
    </Modal>
)

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        width: '100%', 
        flexDirection: 'row', 
        backgroundColor: WHITE, 
        height: 56, 
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center',
        alignItems: 'center' 
    },
    navBarTitle: { 
        height: 56, 
        width: '80%', 
        alignItems: 'flex-start', 
        justifyContent: "center"
    }
})

const mapStateToProps = ({ responseReducer }) => {
    const { dialog } = responseReducer
    return { dialog };
}

export default connect(mapStateToProps, {
    closeDialogResponse
})(Response);