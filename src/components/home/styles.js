/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 14:37:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-07 15:07:09
 */
import {StyleSheet} from 'react-native';
import {GREY_50, WHITE} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26,
    },
    boxContent: {
        width: '100%', 
        backgroundColor: WHITE,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    pieChart: {
        height: 260, 
        width: 300
    },
    legendWrapper: {
        width: '100%', 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        flexWrap: "wrap", 
        padding: 20
    },
    legendItem: { 
        width: '40%', 
        flexDirection: 'row', 
        padding: 0, 
        alignItems: 'center', 
        justifyContent: 'flex-start' 
    },
    legendCircle: {
        width: 20, 
        height: 20, 
        borderRadius: 10,
    }
});

export default styles;