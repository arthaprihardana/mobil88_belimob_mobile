/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 14:33:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-21 11:37:36
 */
import React, {Component} from 'react';
import {ScrollView, View, processColor, TouchableOpacity, AsyncStorage, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {PieChart} from 'react-native-charts-wrapper';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader, Dialog} from '../component';
import styles from './styles';
import { GREY_600, RED_500, GREY_400, GREY_300, GREEN_500 } from '../../libraries/colors';
import {openDialogMonthYear, closeDialogMonthYear, getDashboard, getCheckToken} from '../../actions';

/**
 * Home Component (Page Dashboard)
 *
 * @class Home
 * @extends {Component}
 * 
 * @constructs Home
 */
class Home extends Component {

    state = {
        legend: {
            enabled: false,
            textSize: 10,
            form: 'CIRCLE',
            horizontalAlignment: "CENTER",
            verticalAlignment: "BOTTOM",
            orientation: "HORIZONTAL",
            wordWrapEnabled: true
        },
        data: {},
        total: null,
        highlights: [{x:2}],
        description: {
            text: '',
            textSize: 15,
            textColor: processColor('darkgray'),
        },
        dialog: false,
        title: "",
        month: moment().format('MM'),
        year: moment().format('YYYY'),
        member: null,
        label: {
            countleadnew: "New",
            countleadassigned: "Assigned",
            countleadappraised: "Appraises",
            countleaddeal: "Deal",
            countleadclosed: "Closed",
            countleadcancelled: "Canceled"
        },
        dataSource: []
    };
    
    /**
     * @async
     * @method componentDidMount
     * @description Get Member from local storage, request dashboard data, and check token
     *
     * @memberof Home
     */
    async componentDidMount() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember);
        this.setState({ member: member });
        this.props.getDashboard({
            MemberCode: member.MemberCode,
            Month: this.state.month,
            Year: this.state.year
        });
        this.props.getCheckToken();
    }

    /**
     * @async
     * @method handleSelect
     * @description Handle select chart
     *
     * @param {Object} event
     * @memberof Home
     */
    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({...this.state, selectedEntry: null})
        } else {
            this.setState({...this.state, selectedEntry: JSON.stringify(entry)})
        }
    }

    /**
     * @async
     * @method PieChartData
     *
     * @param {Object} data
     * @returns {Array}
     * @memberof Home
     */
    async PieChartData(data) {
        let arr = [];
        let dt = await _.map(data, (val, key) => {
            if(val.count > 0) {
                arr.push({value: val.count, label: val.label, color: val.colour});
            }
        });
        return arr;
    }

    /**
     * @method getSnapshotBeforeUpdate
     *
     * @param {Object} prevProps
     * @param {Object} prevState
     * @returns {null|Object}
     * @memberof Home
     */
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.value !== prevProps.value) {
            return { selectBox: this.props.value };
        }
        if(this.props.dashboard !== prevProps.dashboard) {
            return { dashboard: this.props.dashboard };
        }
        if(this.props.check !== prevProps.check) {
            return { check: this.props.check }
        }
        return null;   
    }

    /**
     * @method componentDidUpdate
     *
     * @param {Object} prevProps
     * @param {Object} prevState
     * @param {Any} snapshot
     * @memberof Home
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.selectBox !== undefined) {
                if(snapshot.selectBox !== null) {
                    switch (snapshot.selectBox.tipe) {
                        case "month":
                            this.setState(prevState => {
                                return { month: moment().month(snapshot.selectBox.value).format('MM') }
                            })
                            break;
                        case "year":
                            this.setState(prevState => {
                                return { year: snapshot.selectBox.value }
                            })
                            break;
                        default:
                            break;
                    }
                }
            }
            if(snapshot.dashboard !== undefined) {
                if(snapshot.dashboard.status) {
                    let self = this;
                    const { data } = snapshot.dashboard;
                    this.PieChartData(data.dataleadstatus).then(value => {
                        self.setState({
                            total: data.countleadall,
                            dataSource: data.dataleadstatus,
                            data: {
                                dataSets: [{
                                    values: value,
                                    label: '',
                                    config: {
                                        colors: value.map(val => processColor(val.color)),
                                        valueTextSize: 10,
                                        valueTextColor: processColor('white'),
                                        sliceSpace: 2,
                                        selectionShift: 13,
                                        valueFormatter: "#.#",
                                        valueLinePart1Length: 0.5
                                    }
                                }],
                            },
                        })
                    });
                }
            }
            if(snapshot.check !== undefined) {
                if(snapshot.check !== null) {
                    if(snapshot.check.status) {
                        if(snapshot.check.data.invalid) {
                            AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                            Actions.reset('login', { invalid: true });
                        }
                    } else {
                        AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                        Actions.reset('login', { session: true });
                    }
                } else {
                    AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                    Actions.reset('login', { session: true });
                }
            }
        }
    }

    /**
     * @method render
     *
     * @returns
     * @memberof Home
     */
    render() {
        const { loading } = this.props;
        const { dataSource } = this.state;
        return (
            <ScrollView style={{ flex: 1 }}>
                <BlockHeader />
                <View style={styles.container}>
                    <View style={styles.boxContent}>
                        <View style={{ padding: 10, borderBottomWidth: 0.5, borderBottomColor: GREY_300, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#383d41' }}>Periode Leads</Text>
                        </View>
                        <View style={{ flexDirection: 'row', borderBottomWidth: 0.5, borderBottomColor: GREY_300, width: '100%', height: 50, marginBottom: 20, justifyContent: 'space-between'}}>
                            <TouchableOpacity onPress={() => {
                                this.setState({ title: "Pilih Bulan" })
                                this.props.openDialogMonthYear("month") 
                                }} style={{ flexDirection: 'row', borderRightColor: GREY_300, borderRightWidth: 0.5, width: '50%', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16 }}>
                                <Text>{moment().month(parseInt(this.state.month) - 1).format('MMMM')}</Text>
                                <FontAwesome5 name="angle-down" size={16} color={GREY_600} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                this.setState({ title: "Pilih Tahun" })
                                this.props.openDialogMonthYear("year")
                                }} style={{ flexDirection: 'row', width: '35%', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16, borderRightColor: GREY_300, borderRightWidth: 0.5,  }}>
                                <Text>{this.state.year}</Text>
                                <FontAwesome5 name="angle-down" size={16} color={GREY_600} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: '15%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#e2e3e5'}} onPress={() => {
                                if(!loading) {
                                    this.props.getDashboard({
                                        MemberCode: this.state.member.MemberCode,
                                        Month: this.state.month,
                                        Year: this.state.year
                                    })
                                }
                            }}>
                                { loading ? <ActivityIndicator color="#383d41" /> : <FontAwesome5 name="search" style={{ color: '#383d41' }} />}
                            </TouchableOpacity>
                        </View>
                        <PieChart
                            style={styles.pieChart}
                            logEnabled={false}
                            chartBackgroundColor={processColor('white')}
                            chartDescription={this.state.description}
                            data={this.state.data}
                            legend={this.state.legend}
                            highlights={this.state.highlights}

                            entryLabelColor={processColor('white')}
                            entryLabelTextSize={12}
                            drawEntryLabels={false}

                            rotationEnabled={false}
                            rotationAngle={45}
                            usePercentValues={false}
                            styledCenterText={{text:`Total\n${this.state.total}`, color: processColor(GREY_600), size: 18}}
                            centerTextRadiusPercent={100}
                            holeRadius={60}
                            holeColor={processColor('#f0f0f0')}
                            transparentCircleRadius={50}
                            transparentCircleColor={processColor('#f0f0f088')}
                            maxAngle={400}
                            onSelect={this.handleSelect.bind(this)}
                            onChange={(event) => console.log(event.nativeEvent)}
                        />
                        <View style={styles.legendWrapper}>
                            { loading ? Array.apply(0, Array(6)).map((val, key) => (
                                <View key={key} style={[styles.legendItem, { marginBottom: 10 }]}>
                                    <View style={[styles.legendCircle, { backgroundColor: '#eee' }]} />
                                    <View style={{ marginLeft: 10, width: 100, height: 18, backgroundColor: '#eee', borderRadius: 4 }}></View>
                                </View>
                            )) : dataSource.length > 0 && dataSource.map((val, key) => (
                                <View style={styles.legendItem} key={key}>
                                    <View style={[styles.legendCircle, { backgroundColor: val.colour }]} />
                                    <Text style={{ color: GREY_600, marginLeft: 10 }}>{val.label}</Text>
                                </View>
                            )) }
                        </View>
                    </View>
                </View>
                <Dialog 
                    title={this.state.title}
                    onRequestClose={() => this.props.closeDialogMonthYear(null)}
                    visible={this.props.dialog}>
                    {this.props.tipe !== null && <Konten tipe={this.props.tipe} />}
                </Dialog>
            </ScrollView>
        );
    }
}

const KontenDialog = props => {
    switch (props.tipe) {
        case "month":
            return Array.apply(0, Array(12)).map((_, i) => (
                <TouchableOpacity onPress={() => props.closeDialogMonthYear({
                    tipe: props.tipe,
                    value: i
                    })} key={i} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                    <Text>{moment().month(i).format('MMMM')}</Text>
                </TouchableOpacity>
            ))
        case "year":
            return Array.apply(0, Array(20)).map((_, i) => (
                <TouchableOpacity onPress={() => props.closeDialogMonthYear({
                    tipe: props.tipe,
                    value: moment().year() - i
                    })} key={i} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                    <Text>{moment().year() - i}</Text>
                </TouchableOpacity>
            ))
        default:
            return <View />
    }
}

const mapStateToProps = ({ dashboardReducer, checkTokenReducer }) => {
    const { dashboard, loading, dialog, tipe, value } = dashboardReducer;
    const { check } = checkTokenReducer;
    return { dashboard, loading, check, dialog, tipe, value };
};

const Konten = connect(mapStateToProps, {
    closeDialogMonthYear
})(KontenDialog);

export default connect(mapStateToProps, {
    openDialogMonthYear,
    closeDialogMonthYear,
    getDashboard,
    getCheckToken
})(Home);