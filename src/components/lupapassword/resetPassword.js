/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-13 10:39:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-10 14:45:02
 */
import React, { Component } from 'react';
import {View, Keyboard, ActivityIndicator, AsyncStorage, Alert} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Actions} from 'react-native-router-flux';
import validation from 'validate.js';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {Text, Button, TextInput} from '../component';
import globalStyles from '../../assets/styles';
import {PRIMARY_COLOR, WHITE} from '../../libraries/colors';
import styles from './styles';
// import fetchWithTimeout from '../../actions/fetch';
// import {RESET_PASSWORD} from '../../constant';
import {getResetPassword, openDialogResponse, closeDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    kataSandi: string,
    kataSandi2: string
};

var constraintsKonfirmasiPassword = {
    konfirmasiKataSandi: {
      equality: "kataSandi"
    }
};

class ResetPassword extends Component<Props,State> {
    
    state = {
        // loading: false,
        kataSandi: '',
        errorKataSandi: '',
        kataSandi2: '',
        errorKataSandi2: '',
        tempResetPassword: null,
        readyToSubmit: false
    };

    componentDidMount() {
        AsyncStorage.getItem('@temporary:resetpassword', (error, result) => {
            if(result !== null) {
                this.setState({
                    tempResetPassword: JSON.parse(result)
                })
            }
        })
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(prevProps.resetPassword !== this.props.resetPassword) {
            return this.props.resetPassword;
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.status) {
                AsyncStorage.removeItem('@temporary:resetpassword');
                this.props.openDialogResponse();
            } else {
                Alert.alert(
                    'Info', 
                    snapshot.message, 
                    [
                        {text: 'OK', onPress: () => {} },
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    onSubmit = () => {
        const { kataSandi, kataSandi2, tempResetPassword } = this.state;
        this.props.getResetPassword({
            memberpassword: kataSandi,
            memberconfirmpassword: kataSandi2,
            devicetoken: tempResetPassword.devicetoken,
            verifycode: tempResetPassword.verifycode,
            memberimgidcard: '',
            membercode: tempResetPassword.membercode,
            memberid: tempResetPassword.memberid
        })
        // const { kataSandi, kataSandi2, tempResetPassword } = this.state;
        // this.setState({ loading: true })
        // console.log('body ==>', {
        //     memberpassword: kataSandi,
        //     memberconfirmpassword: kataSandi2,
        //     devicetoken: tempResetPassword.devicetoken,
        //     verifycode: tempResetPassword.verifycode,
        //     memberimgidcard: '',
        //     membercode: tempResetPassword.membercode,
        //     memberid: tempResetPassword.memberid
        // });
        
        // fetchWithTimeout(RESET_PASSWORD, {
        //     method: 'PUT',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         memberpassword: kataSandi,
        //         memberconfirmpassword: kataSandi2,
        //         devicetoken: tempResetPassword.devicetoken,
        //         verifycode: tempResetPassword.verifycode,
        //         memberimgidcard: '',
        //         membercode: tempResetPassword.membercode,
        //         memberid: tempResetPassword.memberid
        //     })
        // })
        // .then(response => {
        //     this.setState({ loading: false })
        //     if(response.status) {
        //         AsyncStorage.removeItem('@temporary:resetpassword');
        //         Alert.alert(
        //             'Info', 
        //             response.message, 
        //             [
        //                 {text: 'OK', onPress: () => Actions.reset('login') },
        //             ],
        //             { cancelable: false }
        //         )
        //     } else {
        //         Alert.alert(
        //             'Info', 
        //             'Oops, terjadi kesalahan dalam mengirim data', 
        //             [
        //                 {text: 'OK', onPress: () => {} },
        //             ],
        //             { cancelable: false }
        //         )
        //     }
        //     // Actions.reset('login')
        // })
        // .catch(err => {
        //     this.setState({ loading: false })
        //     Alert.alert(
        //         'Info', 
        //         'Request Timed Out', 
        //         [
        //             {text: 'OK', onPress: () => {} },
        //         ],
        //         { cancelable: false }
        //     )
        // });
    }

    render() {
        const { loading } = this.state;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <View style={{ position: 'absolute', backgroundColor: PRIMARY_COLOR, top: 0, width: '100%', height: 120 }} />
                <View style={styles.container}>
                    <View style={styles.boxlupapassword}>
                        <Text style={styles.title}>Atur Ulang Kata Sandi</Text>
                        <Text style={{ marginBottom: 30 }}>Masukan kata sandi baru anda</Text>

                        <TextInput 
                            refs={ref => this._kataSandi = ref }
                            type="password"
                            label="Kata Sandi Baru"
                            value={this.state.kataSandi}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({kataSandi: text}) }
                            onSubmitEditing={() => this._kataSandi2.focus() }
                        />
                        <TextInput 
                            refs={ref => this._kataSandi2 = ref }
                            type="password"
                            label="Konfirmasi Kata Sandi Baru"
                            value={this.state.kataSandi2}
                            returnKeyType="next"
                            onChangeText={ text => {
                                this.setState({kataSandi2: text}, () => {
                                    let v = validation({ "kataSandi": this.state.kataSandi.toString(), "konfirmasiKataSandi": this.state.kataSandi2.toString() }, constraintsKonfirmasiPassword );
                                    this.setState(prevState => {
                                        if(v !== undefined) {
                                            return { 
                                                errorKataSandi2: v.konfirmasiKataSandi[0],
                                                readyToSubmit: false
                                            }
                                        } else {
                                            return {
                                                errorKataSandi2: '',
                                                readyToSubmit: true
                                            }
                                        }
                                    });
                                });
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                                if(this.state.readyToSubmit) {
                                    if(this.props.loading !== true) {
                                        this.onSubmit();
                                    }
                                }
                            }}
                            returnKeyType="go"
                            errorMessage={this.state.errorKataSandi2}
                        />
                    </View>

                    <Button style={ this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        Keyboard.dismiss();
                        if(this.state.readyToSubmit) {
                            if(this.props.loading !== true) {
                                this.onSubmit();
                            }
                        }
                    }}>
                        {this.props.loading ? <ActivityIndicator color={WHITE} />  : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Kirim</Text> }
                    </Button>
                </View>
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.reset('login');
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>Your Pasword has been Changed</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.reset('login');
                        }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Close</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = ({ lupaPasswordReducer, responseReducer }) => {
    const { loading, resetPassword } = lupaPasswordReducer;
    return { loading, resetPassword, responseReducer }
};

export default connect(mapStateToProps, {
    getResetPassword,
    openDialogResponse,
    closeDialogResponse
})(ResetPassword);