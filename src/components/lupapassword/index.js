/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-13 07:35:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-10 14:45:07
 */
import React, {Component} from 'react';
import {View, Keyboard, Alert, AsyncStorage, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Actions} from 'react-native-router-flux';

import {Text, Button, TextInput, BlockHeader} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_900, PRIMARY_COLOR} from '../../libraries/colors';
import styles from './styles';
import fetchWithTimeout from '../../actions/fetch';
import {LUPA_PASSWORD} from '../../constant';
import {getLupaPassword} from '../../actions';

type Props = {};
type State = {
    email: String,
    readyToSubmit: Boolean,
    errorEmail: String
};

class LupaPassword extends Component<Props, State> {

    state = {
        readyToSubmit: false,
        loading: false,
        email: '',
        errorEmail: null
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(prevProps.lupaPassword !== this.props.lupaPassword) {
            return this.props.lupaPassword;
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.status) {
                AsyncStorage.setItem('@temporary:resetpassword', JSON.stringify(snapshot.data));
                Actions.push('verifikasi',{ type: 'lupapassword' })
            } else {
                Alert.alert(
                    'Info', 
                    snapshot.message, 
                    [
                        {text: 'OK', onPress: () => {} },
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    onSubmit = () => {
        this.props.getLupaPassword({ membermail: this.state.email });
    }

    render() {
        const { loading } = this.state;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={globalStyles.boxContainer}>
                    <View style={styles.boxlupapassword}>
                        <Text style={styles.title}>Lupa Kata Sandi</Text>
                        <Text style={{ marginBottom: 30 }}>Masukan email anda untuk mengatur ulang password</Text>

                        <TextInput 
                            refs={ref => this._email = ref}
                            label="Email"
                            value={this.state.email}
                            keyboardType="email-address"
                            returnKeyType="go"
                            onChangeText={ text => {
                                // this.setState({email: text}) 
                                this.setState(prevState => {
                                    return { 
                                        email: text,
                                        readyToSubmit: text.length > 0 ? true : false
                                    }
                                })
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                                if(this.state.readyToSubmit) {
                                    if(!this.props.loading) {
                                        this.onSubmit();
                                    }
                                }
                            }}
                        />
                    </View>

                    <Button style={ this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled } onPress={() => {
                        Keyboard.dismiss();
                        if(this.state.readyToSubmit) {
                            if(!this.props.loading) {
                                this.onSubmit();
                            }
                        }
                    }}>
                        {this.props.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Kirim</Text> }
                    </Button>
                </View>
            </KeyboardAwareScrollView>
        );
    }

}

const mapStateToProps = ({ lupaPasswordReducer }) => {
    const { loading, lupaPassword } = lupaPasswordReducer;
    return { loading, lupaPassword };
};

export default connect(mapStateToProps, {
    getLupaPassword
})(LupaPassword);