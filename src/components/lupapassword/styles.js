/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-13 07:44:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-13 14:37:57
 */
import {StyleSheet} from 'react-native';
import {GREY_300, GREY_400, WHITE, PRIMARY_COLOR} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26
    },
    boxlupapassword: {
        width: '100%', 
        // borderColor: GREY_300, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1,
        shadowOffset:{  width: 2,  height: 2  },
        shadowColor: '#eee',
        shadowOpacity: 1.0,
    },
    title: { 
        color: PRIMARY_COLOR, 
        fontSize: 18
    }
});

export default styles;