/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-12 12:59:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-27 14:26:26
 */
import React, {Component} from 'react';
import {View, Keyboard, Alert, ActivityIndicator, AsyncStorage} from 'react-native';
import {connect} from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import validation from 'validate.js';

import {Text, Button, TextInput, BlockHeader} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_900, PRIMARY_COLOR} from '../../libraries/colors';
import styles from './styles';
import {getRegister} from '../../actions';

type Props = {};
type State = {
    namaLengkap: string,
    email: string,
    noHandphone: string,
    kataSandi: string,
    konfirmasiKataSandi: string,
    kodeReferal: string,
    readyToSubmit: boolean
};

const validateConstraints = {
    namaLengkap: {
        presence: true
    },
    email: {
        presence: true,
        email: true
    },
    noHandphone: {
        presence: true,
        format: {
            pattern: /(\+62 ((\d{3}([ -]\d{3,})([- ]\d{4,})?)|(\d+)))|(\(\d+\) \d+)|\d{3}( \d+)+|(\d+[ -]\d+)|\d+/,
            message: '^is not a valid phone number'
        } 
    },
    kataSandi: {
        presence: true,
        length: {
            minimum: 6,
            message: "must be at least 6 characters"
        }
    },
}

var constraintsKonfirmasiPassword = {
    konfirmasiKataSandi: {
      equality: "kataSandi"
    }
};

class Register extends Component<Props, State> {

    state = {
        namaLengkap: "",
        errornamaLengkap: "",
        email: "",
        erroremail: "",
        noHandphone: "",
        errornoHandphone: "",
        kataSandi: "",
        errkataSandi: "",
        konfirmasiKataSandi: "",
        errorkonfirmasiKataSandi: "",
        kodeReferal: "",
        readyToSubmit: false,
    }

    componentDidMount() {
        this._namaLengkap.focus();
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.register !== prevProps.register) {
            return this.props.register;
        }
        return null;
    }
    
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.status) {
                AsyncStorage.multiSet([
                    ['@temporary:userRegister', JSON.stringify({
                        membername: this.state.namaLengkap,
                        membermail: this.state.email,
                        memberphone: this.state.noHandphone,
                        memberpassword: this.state.kataSandi,
                        memberconfirmpassword: this.state.konfirmasiKataSandi,
                        membercodereferal: this.state.kodeReferal
                    })],
                    ['@temporary:register', JSON.stringify(snapshot.data)]
                ]);
                Actions.push('verifikasi', {type: 'register'});
            } else {
                // this._formRegister.scrollToPosition({ y: 0, animated: true })
                this._email.focus();
                this.setState(prevState => {
                    return { erroremail: snapshot.message }
                })
            }
        }
    }

    omitField = async (currentField) => {
        let except = ['errornamaLengkap', 'erroremail', 'errornoHandphone', 'errkataSandi', 'errorkonfirmasiKataSandi', 'readyToSubmit', 'kodeReferal'];
        except.push(currentField);
        let o = _.omit(this.state, except)
        let a = await _.findKey(o, val => {
            if(_.isEmpty(val)) { return true }
        });
        return a;
    }
    
    render() {
        const { loading } = this.props;
        return (
            <KeyboardAwareScrollView ref={ref => this._formRegister = ref } style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={styles.container}>
                    <View style={[styles.boxregister, {paddingLeft: 20, paddingRight: 20}]}>
                        <Text style={styles.title}>DAFTAR</Text>
                        <Text style={{ marginBottom: 30 }}>dan mulai kumpulkan poin</Text>
                        
                        <TextInput 
                            refs={ref => this._namaLengkap = ref}
                            label="Nama Lengkap"
                            value={this.state.namaLengkap}
                            returnKeyType="next"
                            onChangeText={ text => {
                                this.omitField('namaLengkap').then(val => {
                                    this.setState(prevState => {
                                        return { 
                                            namaLengkap: text, 
                                            readyToSubmit: text !== "" && val === undefined ? true : false,

                                        }
                                    })
                                });
                            }}
                            onBlur={ () => {
                                let v = validation.single(this.state.namaLengkap.length > 0 ? this.state.namaLengkap.toString() : null, validateConstraints.namaLengkap);
                                this.setState({ errornamaLengkap: v !== undefined ? v[0] : '' });
                            }}
                            onSubmitEditing={() => this._email.focus() }
                            autoCapitalize="words"
                            errorMessage={this.state.errornamaLengkap || "*) Wajib diisi"}
                        />
                        <TextInput 
                            refs={ref => this._email = ref}
                            label="Email"
                            value={this.state.email}
                            returnKeyType="next"
                            keyboardType="email-address"
                            textContentType="emailAddress"
                            onChangeText={ text => {
                                this.omitField('email').then(val => {
                                    let v = validation.single(text.length > 0 ? text.toString() : null, validateConstraints.email);
                                    this.setState(prevState => {
                                        return { 
                                            email: text, 
                                            readyToSubmit: val === undefined && text !== "" ? true : false,
                                            erroremail: v !== undefined ? v[0] : ''
                                        }
                                    })
                                });
                            }}
                            onBlur={ () => {
                                let v = validation.single(this.state.email.length > 0 ? this.state.email.toString() : null, validateConstraints.email);
                                this.setState({ erroremail: v !== undefined ? v[0] : '' });
                            }}
                            onSubmitEditing={() => this._noHandphone.focus() }
                            autoCapitalize="none"
                            errorMessage={this.state.erroremail || "*) Wajib diisi"}
                        />
                        <TextInput 
                            refs={ref => this._noHandphone = ref}
                            label="No Handphone"
                            value={this.state.noHandphone}
                            returnKeyType="next"
                            keyboardType="number-pad"
                            onChangeText={ text => {
                                this.omitField('noHandphone').then(val => {
                                    this.setState(prevState => {
                                        return { 
                                            noHandphone: text, 
                                            readyToSubmit: text !== "" && val === undefined ? true : false 
                                        }
                                    })
                                });
                            }}
                            onBlur={ () => {
                                let v = validation.single(this.state.noHandphone.length > 0 ? this.state.noHandphone.toString() : null, validateConstraints.noHandphone);
                                this.setState({ errornoHandphone: v !== undefined ? v[0] : '' });
                            }}
                            onSubmitEditing={() => this._kataSandi.focus() }
                            errorMessage={this.state.errornoHandphone || "*) Wajib diisi"}
                            maxLength={13}
                        />
                        <TextInput 
                            refs={ref => this._kataSandi = ref }
                            type="password"
                            label="Kata Sandi"
                            value={this.state.kataSandi}
                            returnKeyType="next"
                            onChangeText={ text => {
                                this.omitField('kataSandi').then(val => {
                                    let v = validation.single(text.length > 0 ? text.toString() : null, validateConstraints.kataSandi);
                                    let vv = validation({ "kataSandi": text.toString(), "konfirmasiKataSandi": this.state.konfirmasiKataSandi.toString() }, constraintsKonfirmasiPassword );
                                    this.setState(prevState => {
                                        return { 
                                            errkataSandi: v !== undefined ? v[0] : '',
                                            errorkonfirmasiKataSandi: vv !== undefined ? vv.konfirmasiKataSandi[0] : '',
                                            kataSandi: text, 
                                            readyToSubmit: val === undefined && text !== "" && v === undefined && vv === undefined ? true : false 
                                        }
                                    })
                                });
                            }}
                            onBlur={ () => {
                                let v = validation.single(this.state.kataSandi.length > 0 ? this.state.kataSandi.toString() : null, validateConstraints.kataSandi);
                                let vv = validation({ "kataSandi": this.state.kataSandi.toString(), "konfirmasiKataSandi": this.state.konfirmasiKataSandi.toString() }, constraintsKonfirmasiPassword );
                                this.setState(prevState => {
                                    return { 
                                        errkataSandi: v !== undefined ? v[0] : '',
                                        errorkonfirmasiKataSandi: vv !== undefined ? vv.konfirmasiKataSandi[0] : ''
                                    }
                                })
                            }}
                            onSubmitEditing={() => this._konfirmasiKataSandi.focus() }
                            errorMessage={this.state.errkataSandi || "*) Wajib diisi"}
                        />
                        <TextInput 
                            refs={ref => this._konfirmasiKataSandi = ref }
                            type="password"
                            label="Konfirmasi Kata Sandi"
                            value={this.state.konfirmasiKataSandi}
                            returnKeyType="next"
                            onChangeText={ text => {
                                this.omitField('konfirmasiKataSandi').then(val => {
                                    let v = validation({ "kataSandi": this.state.kataSandi.toString(), "konfirmasiKataSandi": text.toString() }, constraintsKonfirmasiPassword );
                                    this.setState(prevState => {
                                        return { 
                                            errorkonfirmasiKataSandi: v !== undefined ? v.konfirmasiKataSandi[0] : '',
                                            konfirmasiKataSandi: text, 
                                            readyToSubmit: text !== "" && v === undefined && val === undefined ? true : false
                                        }
                                    })
                                });
                            }}
                            onBlur={ () => {
                                let v = validation({ "kataSandi": this.state.kataSandi.toString(), "konfirmasiKataSandi": this.state.konfirmasiKataSandi.toString() }, constraintsKonfirmasiPassword );
                                this.setState(prevState => { 
                                    return { errorkonfirmasiKataSandi: v !== undefined ? v.konfirmasiKataSandi[0] : '' }
                                })
                            }}
                            onSubmitEditing={() => this._kodeReferal.focus() }
                            errorMessage={this.state.errorkonfirmasiKataSandi || "*) Wajib diisi"}
                        />
                        <TextInput 
                            refs={ref => this._kodeReferal = ref}
                            label="Kode Referal"
                            value={this.state.kodeReferal}
                            returnKeyType="go"
                            onChangeText={ text => this.setState({kodeReferal: text}) }
                            onSubmitEditing={() => {
                                if(this.state.readyToSubmit) {
                                    Keyboard.dismiss();
                                    this.props.getRegister({
                                        membername: this.state.namaLengkap,
                                        membermail: this.state.email,
                                        memberphone: this.state.noHandphone,
                                        memberpassword: this.state.kataSandi,
                                        memberconfirmpassword: this.state.konfirmasiKataSandi,
                                        membercodereferal: this.state.kodeReferal
                                    })
                                }
                            }}
                        />
                    </View>
                    <Button style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        if(this.state.readyToSubmit) {
                            Keyboard.dismiss();
                            if(loading !== true) {
                                this.props.getRegister({
                                    membername: this.state.namaLengkap,
                                    membermail: this.state.email,
                                    memberphone: this.state.noHandphone,
                                    memberpassword: this.state.kataSandi,
                                    memberconfirmpassword: this.state.konfirmasiKataSandi,
                                    membercodereferal: this.state.kodeReferal
                                })
                            }
                        }
                        }}>
                        {loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Daftar</Text> }
                    </Button>

                    <Button style={[globalStyles.buttonLink, { marginTop: 20, marginBottom: 20 }]} onPress={() => Actions.popTo('login')}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{ color: GREY_900, letterSpacing: 1 }}>Sudah punya akun? </Text>
                            <Text style={{ color: PRIMARY_COLOR }}>Login disini</Text>
                        </View>
                    </Button>
                </View>
            </KeyboardAwareScrollView>
        );
    }

}

const mapStateToProps = ({ registerReducer }) => {
    return registerReducer;
}

export default connect(mapStateToProps, {
    getRegister
})(Register);