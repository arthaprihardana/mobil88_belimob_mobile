/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-13 10:57:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-27 14:39:34
 */
import React, {Component} from 'react';
import {View, Keyboard, Image, AsyncStorage, Alert, ActivityIndicator, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Actions} from 'react-native-router-flux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import validation from 'validate.js';
import {Text, Button, TextInput, BlockHeader, Select, Dialog} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_500, GREY_400, RED_300} from '../../libraries/colors';
import styles from './styles';
import {getDetailRegister, updateFotoProfil, getJabatan, openDialogJabatan, closeDialogJabatan, openCameraRoll, closeCameraRoll, openCamera, closeCamera, openDialogResponse, closeDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    readyToSubmit: Boolean,
    perusahaan: String,
    errorperusahaan: String,
    jabatan: String,
    errorjabatan: String,
    foto: String,
    ktp: String,
    errorktp: String,
    idcardperusahaan: String,
    erroridcardperusahaan: String,
    tempRegister: Object,
    tempUserRegister: Object,
    masterJabatan: Object
};

const validateConstraints = {
    perusahaan: {
        presence: true
    },
    ktp: {
        presence: true,
    },
    idcard: {
        presence: true
    }
}

class DetailRegister extends Component<Props, State> {

    state = {
        readyToSubmit: false,
        perusahaan: null,
        errorperusahaan: '',
        jabatan: null,
        memberotherposition: null,
        errorjabatan: '',
        foto: null,
        ktp: null,
        errorktp: '',
        idcardperusahaan: null,
        erroridcardperusahaan: '',
        tempRegister: null,
        tempUserRegister: null,
        masterJabatan: null
    };

    componentDidMount() {
        this.props.getJabatan();
        AsyncStorage.multiGet(['@temporary:register', '@temporary:userRegister'], (error, result) => {
            let tempRegister = JSON.parse(result[0][1]);
            let tempUserRegister = JSON.parse(result[1][1]);
            this.setState({
                tempRegister: tempRegister,
                tempUserRegister: tempUserRegister
            })
        });
    }

    handleCameraOrGallery = (type) => {
        Alert.alert(
            'Info',
            'Unggah foto dari?',
            [
                {text: 'Cancel', onPress: () => console.log('cancel') },
                {text: 'Galeri', onPress: () => {
                    this.props.openCameraRoll(type)
                }},
                {text: 'Kamera', onPress: () => {
                    this.props.openCamera(type);
                }},
            ],
            { cancelable: false }
        )
    }

    onSubmit = () => {
        const {perusahaan, jabatan, ktp, idcardperusahaan, tempRegister, memberotherposition} = this.state;
        this.props.getDetailRegister({
            membercompany: perusahaan,
            memberposition: jabatan,
            memberotherposition: memberotherposition,
            memberimgktp: ktp !== null ? ktp.base64 : ktp,
            memberimgidcard: idcardperusahaan !== null ? idcardperusahaan.base64 : idcardperusahaan,
            devicetoken: tempRegister.devicetoken,
            verifycode: tempRegister.verifycode,
            membercode: tempRegister.membercode,
            memberid: tempRegister.memberid
        });
    }

    openGaleri = (type) => {
        Actions.push('galeriCamera', {type: type, onBack: () => this.props.closeCameraRoll() });
    }

    openCamera = (type) => {
        Actions.push('camera', {type: type, onBack: () => this.props.closeCamera() });
    }

    checkNotNull = () => {
        // if(this.state.foto === null) {
        //     return false;
        // }
        if(this.state.ktp === null) {
            return false;
        }
        if(this.state.idcardperusahaan === null) {
            return false;
        }
        if(this.state.perusahaan === null) {
            return false;
        }
        if(this.state.jabatan === null) {
            return false;
        }
        return true;
    }
    
    componentDidUpdate(prevProps, prevState) {
        const { registerDetail } = this.props.registerReducer;
        const { tempRegister } = this.state;

        if(this.props.cameraRollOpen !== prevProps.cameraRollOpen) {
            if(this.props.cameraRollOpen) {
                this.openGaleri(this.props.type);
            }
        }

        if(this.props.cameraOpen !== prevProps.cameraOpen) {
            if(this.props.cameraOpen) {
                this.openCamera(this.props.type)
            }
        }

        if(this.props.pictureProfile !== prevProps.pictureProfile) {
            // this.setState({foto: this.props.pictureProfile}, () => {
            //     if(this.checkNotNull()) {
            //         this.setState({
            //             readyToSubmit: true
            //         })
            //     }
            // });
            this.setState({
                foto: this.props.pictureProfile
            });
        }

        if(this.props.pictureKtp !== prevProps.pictureKtp) {
            this.setState({ ktp: this.props.pictureKtp }, () => {
                if(this.checkNotNull()) {
                    this.setState({
                        readyToSubmit: true
                    })
                }
            });
        }

        if(this.props.pictureCardId !== prevProps.pictureCardId) {
            this.setState({idcardperusahaan: this.props.pictureCardId}, () => {
                if(this.checkNotNull()) {
                    this.setState({
                        readyToSubmit: true
                    })
                }
            })
        }

        if(this.props.jabatan !== prevProps.jabatan) {
            if(this.props.jabatan.status) {
                this.setState({ masterJabatan: this.props.jabatan.data});
            }
        }

        if(registerDetail !== prevProps.registerReducer.registerDetail) {
            if(registerDetail.status) {
                AsyncStorage.multiRemove(['@temporary:register', '@temporary:userRegister']);
                this.props.openDialogResponse();
            } else {
                Alert.alert(
                    'Info',
                    'Terjadi kesalahan dalam mengirim data. Coba lagi?',
                    [
                        {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'Ya', onPress: () => this.onSubmit()},
                    ],
                    { cancelable: false }
                )
            }
        }

    }

    render() {
        const { tempRegister, tempUserRegister, ktp, idcardperusahaan, foto } = this.state;
        const { loading } = this.props;
        const imageProfile = foto !== null ? {uri: `${this.state.foto.path}`} : require('../../assets/images/user-default.png');
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false}>
                <BlockHeader />
                <View style={styles.container}>
                    
                    <View style={styles.boxProfile}>
                        <View style={styles.boxUserInfo}>
                            <View style={{ width: '40%' }}>
                                <Image source={imageProfile} style={{ width: 100, height: 100, borderRadius: 4 }} resizeMode={'cover'} />
                                <Button style={styles.buttonUbahFoto} onPress={() => this.handleCameraOrGallery("imageprofile") }>
                                    <FontAwesome5 name="camera" color={GREY_400} size={10} style={{ marginRight: 5}} />
                                    <Text style={{ color: WHITE, fontSize: 10, lineHeight: 15 }}>Ubah Foto</Text>
                                </Button>
                            </View>
                            <View style={{ width: '60%', justifyContent: 'center' }}>
                                <Text style={[styles.title, { fontSize: 22 }]}>{tempUserRegister !== null ? tempUserRegister.membername : null}</Text>
                                <Text small>{tempUserRegister !== null ? tempUserRegister.membermail : null}</Text>
                                <Text small>{tempUserRegister !== null ? tempUserRegister.memberphone : null}</Text>
                            </View>
                        </View>
                        <View style={styles.boxButtonUpload}>
                            {ktp !== null ? 
                                <View style={{borderRightWidth: 1, borderRightColor: '#eee', width: '50%', flexDirection: 'row', padding: 10 }}>
                                    <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={() => this.handleCameraOrGallery("memberimgktp") }>
                                            <Image source={{ uri: this.state.ktp.path }} style={{ width: 70, height: 70, marginRight: 10, borderRadius: 4 }} resizeMode="cover" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                                        <View>
                                            <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>KTP Terlampir</Text>
                                            <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>(klik pada gambar untuk update)</Text>
                                        </View>
                                    </View>
                                </View> : 
                                <TouchableOpacity style={[styles.buttonUpload, {borderRightWidth: 1, borderRightColor: '#eee'}]} onPress={() => this.handleCameraOrGallery("memberimgktp") }>
                                    <FontAwesome5 name="camera" color={GREY_400} size={28} style={{ marginRight: 10 }} />
                                    <View>
                                        <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>Unggah KTP</Text>
                                        <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>(asli, bukan fotokopi)</Text>
                                        <Text style={{ fontSize: 10, color: RED_300, lineHeight: 15 }}>*) harus diisi</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            {idcardperusahaan !== null ?
                                <View style={{ width: '50%', flexDirection: 'row', padding: 10 }}>
                                    <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={() => this.handleCameraOrGallery("memberimgidcard") }>
                                            <Image source={{ uri: this.state.idcardperusahaan.path }} style={{ width: 70, height: 70, marginRight: 10, borderRadius: 4 }} resizeMode="cover" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                                        <View>
                                            <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>ID Terlampir</Text>
                                            <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>(klik pada gambar untuk update)</Text>
                                        </View>
                                    </View>
                                </View> : 
                                <TouchableOpacity style={styles.buttonUpload} onPress={() => this.handleCameraOrGallery("memberimgidcard")}>
                                    <FontAwesome5 name="camera" color={GREY_400} size={28} style={{ marginRight: 10 }} />
                                    <View>
                                        <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>Unggah ID Perusahaan</Text>
                                        <Text style={{ fontSize: 10, color: GREY_500, lineHeight: 15 }}>(asli, bukan fotokopi)</Text>
                                        <Text style={{ fontSize: 10, color: RED_300, lineHeight: 15 }}>*) harus diisi</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>

                    <View style={styles.boxregister}>
                        <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                            <Text style={styles.title}>LENGKAPI PENDAFTARAN</Text>
                            <Text style={{ marginBottom: 30 }}>Lengkapi data diri anda</Text>
                        </View>
                        
                        <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                            <TextInput 
                                refs={ref => this._perusahaan = ref}
                                label="Perusahaan"
                                value={this.state.perusahaan}
                                returnKeyType="next"
                                onChangeText={ text => {
                                    this.setState({perusahaan: text}, () => {
                                        if(this.checkNotNull()) {
                                            this.setState({
                                                readyToSubmit: true
                                            })
                                        }
                                    })
                                }}
                                onSubmitEditing={() => {} }
                                errorMessage={this.state.errorperusahaan || "*) Wajib diisi"}
                                onBlur={ () => {
                                    let v = validation.single(this.state.perusahaan.length > 0 ? this.state.perusahaan.toString() : null, validateConstraints.perusahaan);
                                    this.setState({ errorperusahaan: v !== undefined ? v[0] : '' });
                                }}
                            />

                            <Select 
                                label="Jabatan"
                                onPress={() => this.props.openDialogJabatan()}
                                value={this.state.jabatan || "Pilih"}
                                loading={this.props.loading}
                                errorMessage={this.state.errorperusahaan || "*) Wajib diisi"}
                                />

                            {/Lain/i.test(this.state.jabatan) ? 
                                <TextInput 
                                    refs={ref => this._memberotherposition = ref}
                                    label="Jabatan Lainnya"
                                    value={this.state.memberotherposition}
                                    returnKeyType="next"
                                    onChangeText={ text => this.setState({memberotherposition: text}) }
                                    onSubmitEditing={() => {} }
                                />
                            : <View />}
                        </View>
                    </View>

                    <Button style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        if(this.state.readyToSubmit) {
                            Keyboard.dismiss();
                            if(loading !== true) {
                                this.onSubmit();
                            }
                        }
                    }}>
                        {this.props.registerReducer.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Kirim</Text> }
                    </Button>
                </View>
                <Dialog 
                    title={"Jabatan"}
                    onRequestClose={() => this.props.closeDialogJabatan()}
                    visible={this.props.dialog}>
                    {this.state.masterJabatan && this.state.masterJabatan.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            let self = this;
                            this.props.closeDialogJabatan();
                            if(! /Lain/i.test(value.MemberPositionName)) {
                                self.setState({ memberotherposition: null })
                            }
                            this.setState({ jabatan: value.MemberPositionName }, () => {
                                if(self.checkNotNull()) {
                                    self.setState({
                                        readyToSubmit: true
                                    })
                                }
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.MemberPositionName}</Text>
                        </TouchableOpacity>
                    ))}
                </Dialog>
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.reset('login');
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.props.registerReducer.registerDetail !== null && this.props.registerReducer.registerDetail.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.reset('login');
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Close</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </KeyboardAwareScrollView>
        );
    }

}

const mapStateToProps = ({ registerReducer, memberReducer, jabatanReducer, takePictureReducer, responseReducer }) => {
    const { updatefotoprofil } = memberReducer;
    const { loading, jabatan, dialog } = jabatanReducer;
    const { cameraOpen, cameraRollOpen, type, pictureProfile, pictureKtp, pictureCardId } = takePictureReducer;
    return {registerReducer, updatefotoprofil, loading, jabatan, dialog, cameraOpen, cameraRollOpen, type, pictureProfile, pictureKtp, pictureCardId, responseReducer };
}

export default connect(mapStateToProps, {
    getDetailRegister,
    updateFotoProfil,
    getJabatan,
    openDialogJabatan,
    closeDialogJabatan,
    openCamera,
    closeCamera,
    openCameraRoll,
    closeCameraRoll,
    openDialogResponse,
    closeDialogResponse
})(DetailRegister);