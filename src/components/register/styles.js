/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-12 13:08:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 14:01:54
 */
import {StyleSheet} from 'react-native';
import {GREY_300, WHITE, PRIMARY_COLOR, GREEN_400} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26,
        paddingBottom: 26
    },
    boxregister: {
        width: '100%', 
        // borderColor: GREY_300, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        // paddingLeft: 20,
        // paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 16,
        borderRadius: 3,
        elevation: 1,
        shadowOffset:{  width: 2,  height: 2  },
        shadowColor: '#eee',
        shadowOpacity: 1.0,
    },
    boxProfile: {
        width: '100%', 
        // borderColor: GREY_300, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1
    },
    title: { 
        color: PRIMARY_COLOR, 
        fontSize: 18
    },
    boxUserInfo: { 
        flexDirection: 'row', 
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingTop: 15, 
        paddingBottom: 15 
    },
    boxButtonUpload: { 
        flexDirection: 'row', 
        borderTopWidth: 1, 
        borderTopColor: '#eee' 
    },
    buttonUpload: {
        width: '50%', 
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center', 
        padding: 10,
        height: 80
    },
    buttonUbahFoto: { 
        position: 'absolute', 
        flexDirection: 'row', 
        width: 100, 
        height: 20, 
        backgroundColor: 'rgba(0,0,0,0.3)', 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4
    }
});

export default styles;