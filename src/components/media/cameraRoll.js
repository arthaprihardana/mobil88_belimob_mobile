/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-24 14:12:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-21 14:05:54
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CameraRoll, ScrollView, Image, View, ListView, Dimensions, TouchableOpacity, Platform } from 'react-native';
// import RNFetchBlob from 'react-native-fetch-blob';
import { Actions } from "react-native-router-flux";
import {requestReadStorage} from '../../libraries/permissions';
import { closeCameraRoll, takePictureProfile, takePictureProfileSuccess, takePictureProfileFailure, takePictureKtp, takePictureKtpSuccess, takePictureKtpFailure, takePictureCardId, takePictureCardIdSuccess, takePrictureCardIdFailure } from '../../actions'

type Props = {};
type State = {
    photos: array
};

const {height, width} = Dimensions.get('window');

// const RNFetchBlob = Platform.select({
//     android: () => require('react-native-fetch-blob').default,
// })();

class GaleriCamera extends Component<Props, State> {

    ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    state = {
        photos: this.ds.cloneWithRows([]),
        memberimgktp: null,
        memberimgidcard: null,
        imageprofile: null
    };

    componentWillUnmount() {
        this.props.closeCameraRoll();
    }

    componentDidMount() {
        requestReadStorage().then(value => {
            CameraRoll.getPhotos({
                first: 20,
                assetType: 'Photos'
            })
            .then(r => {
                this.setState({ photos: this.state.photos.cloneWithRows(r.edges) });
            })
            .catch((err) => {
                console.log('error ==>', err);
            });
        }).catch(err => {

        });
    }

    renderRow = (rowData, sectionID, rowID) => {
        return <TouchableOpacity onPress={() => this.getPhotos(rowData.node.image.uri)}>
            <Image
                key={rowID}
                style={{
                    width: width / 3,
                    height: width / 3,
                }}
                source={{ uri: rowData.node.image.uri }}
            />
        </TouchableOpacity>
    }

    getPhotos = (image) => {
        if(Platform.OS == "android") {
            const RNFetchBlob = require('react-native-fetch-blob').default;
            RNFetchBlob.fs.readFile(image, 'base64')
            .then(data => {
                switch (this.props.type) {
                    case "memberimgktp":
                        this.setState({
                            memberimgktp: {
                                path: image,
                                base64: data
                            } 
                        }, () => {
                            this.props.takePictureKtpSuccess(this.state.memberimgktp)
                            Actions.popTo('detailregister')
                        })
                        break;
                    case "memberimgidcard":
                        this.setState({
                            memberimgidcard: {
                                path: image,
                                base64: data
                            } 
                        }, () => {
                            this.props.takePictureCardIdSuccess(this.state.memberimgidcard)
                            Actions.popTo('detailregister')
                        })
                        break;
                    case "imageprofile":
                        this.setState({
                            imageprofile: {
                                path: image,
                                base64: data
                            }
                        }, () => {
                            this.props.takePictureProfileSuccess(this.state.imageprofile)
                            Actions.popTo('detailregister')
                        })
                        break;
                    case "updatememberimgktp":
                        this.setState({
                            memberimgktp: {
                                path: image,
                                base64: data
                            } 
                        }, () => {
                            this.props.takePictureKtpSuccess(this.state.memberimgktp)
                            Actions.popTo('aturProfil') 
                        })
                        break;
                    case "updatememberimgidcard":
                        this.setState({
                            memberimgidcard: {
                                path: image,
                                base64: data
                            } 
                        }, () => {
                            this.props.takePictureCardIdSuccess(this.state.memberimgidcard)
                            Actions.popTo('aturProfil') 
                        })
                        break;
                    case "updatememberimageprofile":
                        this.setState({
                            imageprofile: {
                                path: image,
                                base64: data
                            }
                        }, () => {
                            this.props.takePictureProfileSuccess(this.state.imageprofile)
                            Actions.popTo('aturProfil')
                        })
                    default:
                        break;
                }
            }).catch(err => {
                alert('Error while getting photos')
            })
        }
    }

    render() {
        const { photos } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <ListView 
                    enableEmptySections={true}
                    contentContainerStyle={{
                        justifyContent: 'space-around',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'flex-start'
                    }}
                    dataSource={photos}
                    scrollRenderAheadDistance={500}
                    renderRow={this.renderRow}
                />
            </View>
        );
    }

}

const mapStateToProps = ({ takePictureReducer }) => {
    const { cameraOpen, cameraRollOpen, pictureProfile, pictureKtp, pictureCardId } = takePictureReducer;
    return { cameraOpen, cameraRollOpen };
}

export default connect(mapStateToProps, {
    closeCameraRoll,
    takePictureProfile, 
    takePictureProfileSuccess,
    takePictureProfileFailure,
    takePictureKtp,
    takePictureKtpSuccess,
    takePictureKtpFailure,
    takePictureCardId,
    takePictureCardIdSuccess,
    takePrictureCardIdFailure
})(GaleriCamera);