/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-28 13:00:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-27 13:55:23
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Platform,
    ActivityIndicator
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {WHITE, BLACK, GREY_300} from '../../libraries/colors';
import { Text } from '../component';
import {connect} from 'react-redux';
import { closeCamera, takePictureProfile, takePictureProfileSuccess, takePictureProfileFailure, takePictureKtp, takePictureKtpSuccess, takePictureKtpFailure, takePictureCardId, takePictureCardIdSuccess, takePrictureCardIdFailure } from '../../actions';

class Camera extends Component {

    state = {
        flashModeCode: 0,
        flashMode: [RNCamera.Constants.FlashMode.auto, RNCamera.Constants.FlashMode.on, RNCamera.Constants.FlashMode.off, RNCamera.Constants.FlashMode.torch],
        cameraType: true,
        memberimgktp: null,
        memberimgidcard: null,
        imageprofile: null
    }

    // componentWillUnmount() {
    //     Actions.refresh({ 
    //         memberimgktp: this.state.memberimgktp,
    //         memberimgidcard: this.state.memberimgidcard,
    //         imageprofile: this.state.imageprofile
    //     })
    // }

    componentWillUnmount() {
        this.props.closeCamera();
    }
    
    takePicture = async function() {
        if (this._camera) {
            const options = { quality: 0.3, base64: true };
            const data = await this._camera.takePictureAsync(options)
            switch (this.props.type) {
                case "memberimgktp":
                    this.setState({
                        memberimgktp: {
                            path: data.uri,
                            base64: data.base64
                        } 
                    }, () => {
                        this.props.takePictureKtpSuccess(this.state.memberimgktp)
                        Actions.popTo('detailregister')
                    })
                    break;
                case "memberimgidcard":
                    this.setState({
                        memberimgidcard: {
                            path: data.uri,
                            base64: data.base64
                        } 
                    }, () => {
                        this.props.takePictureCardIdSuccess(this.state.memberimgidcard)
                        Actions.popTo('detailregister')
                    })
                    break;
                case "imageprofile":
                    this.setState({
                        imageprofile: {
                            path: data.uri,
                            base64: data.base64
                        }
                    }, () => {
                        this.props.takePictureProfileSuccess(this.state.imageprofile)
                        Actions.popTo('detailregister')
                    })
                    break;
                case "updatememberimgktp":
                    this.setState({
                        memberimgktp: {
                            path: data.uri,
                            base64: data.base64
                        } 
                    }, () => {
                        this.props.takePictureKtpSuccess(this.state.memberimgktp)
                        Actions.popTo('aturProfil') 
                    })
                    break;
                case "updatememberimgidcard":
                    this.setState({
                        memberimgidcard: {
                            path: data.uri,
                            base64: data.base64
                        } 
                    }, () => {
                        this.props.takePictureCardIdSuccess(this.state.memberimgidcard)
                        Actions.popTo('aturProfil') 
                    })
                    break;
                case "updatememberimageprofile":
                    this.setState({
                        imageprofile: {
                            path: data.uri,
                            base64: data.base64
                        }
                    }, () => {
                        this.props.takePictureProfileSuccess(this.state.imageprofile)
                        Actions.popTo('aturProfil')
                    })
                    break;
                default:
                    break;
            }
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => this._camera = ref }
                    style = {styles.preview}
                    type={this.state.cameraType ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front }
                    flashMode={this.state.flashMode[this.state.flashModeCode]}
                    // zoom={0.5}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                />
                <View style={styles.containerBottom}>
                    <TouchableOpacity style={{
                        paddingHorizontal: 20,
                        alignSelf: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        margin: 20,
                    }} onPress={() => this.setState({ cameraType: !this.state.cameraType })}>
                        <Ionicons name={Platform.OS === "ios" ? "ios-reverse-camera" : "md-reverse-camera"} color={WHITE} size={40} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.takePicture.bind(this)}
                        style = {styles.capture}>
                        <View style={styles.smallCapture}></View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        paddingHorizontal: 20,
                        alignSelf: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        margin: 13
                    }} onPress={() => {
                        this.setState(previousState => {
                            switch (previousState.flashModeCode) {
                                case 0:
                                    return { flashModeCode: 1 }
                                case 1:
                                    return { flashModeCode: 2 }
                                case 2:
                                    return { flashModeCode: 3 }
                                case 3:
                                    return { flashModeCode: 0 }
                                default:
                                    break;
                            }
                        })
                    }}>
                    {this.state.flashModeCode === 0 ? 
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50 }}>
                            <Ionicons name={Platform.OS === "ios" ? "ios-flash" : "md-flash"} color={WHITE} size={40} />
                            <Text style={{ color: WHITE }} small>Auto</Text>
                        </View> : null}
                    {this.state.flashModeCode === 1 ? 
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50 }}>
                            <Ionicons name={Platform.OS === "ios" ? "ios-flash" : "md-flash"} color={WHITE} size={40} />
                        </View> : null}
                    {this.state.flashModeCode === 2 ? 
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50 }}>
                            <Ionicons name={Platform.OS === "ios" ? "ios-flash-off" : "md-flash-off"} color={WHITE} size={40} /> 
                        </View> : null}
                    {this.state.flashModeCode === 3 ? 
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50 }}>
                            <Ionicons name={Platform.OS === "ios" ? "ios-flashlight" : "md-flashlight"} color={WHITE} size={40} />
                        </View> : null}
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: BLACK
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    containerBottom: {
        flex: 0, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        position: 'absolute', 
        width: '100%', 
        bottom: 0, 
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    capture: {
        flex: 0,
        backgroundColor: WHITE,
        width: 80,
        height: 80,
        borderRadius: 40,
        paddingHorizontal: 20,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    smallCapture: { 
        width: 60, 
        height: 60, 
        borderRadius: 30, 
        backgroundColor: GREY_300 
    }
});

const mapStateToProps = ({ takePictureReducer }) => {
    const { cameraOpen } = takePictureReducer;
    return { cameraOpen };
}

export default connect(mapStateToProps, {
    closeCamera,
    takePictureProfile, 
    takePictureProfileSuccess,
    takePictureProfileFailure,
    takePictureKtp,
    takePictureKtpSuccess,
    takePictureKtpFailure,
    takePictureCardId,
    takePictureCardIdSuccess,
    takePrictureCardIdFailure
})(Camera);