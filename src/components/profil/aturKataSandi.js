/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 14:16:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-14 14:33:31
 */
import React, { Component } from 'react';
import {View, Keyboard, AsyncStorage, Alert, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import validation from 'validate.js';
import {Text, Button, TextInput, BlockHeader} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_900, PRIMARY_COLOR} from '../../libraries/colors';
import styles from './styles';
import {updatePassword, openDialogResponse, closeDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    memberoldpassword: String,
    errormemberoldpassword: String,
    memberpassword: String,
    errormemberpassword: String,
    memberconfirmpassword: String,
    errormemberconfirmpassword: String,
    memberid: String,
    readyToSubmit: Boolean
};

const validatePassword = {
    memberpassword: {
        presence: true,
        length: {
            minimum: 6,
            message: "must be at least 6 characters"
        }
    }
}

const confirmVaildatePassword = {
    memberconfirmpassword: {
        equality: {
            attribute: "memberpassword",
            message: "^Kata Sandi Baru tidak sesuai"
        },
    }
}

class AturKataSandi extends Component<Props, State> {

    state = {
        memberoldpassword: '',
        errormemberoldpassword: '',
        memberpassword: '',
        errormemberpassword: '',
        memberconfirmpassword: '',
        errormemberconfirmpassword: '',
        memberid: '',
        readyToSubmit: false
    }

    componentDidMount() {
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.setState({ memberid: member.MemberId })
            }
        })
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.updatepassword !== prevProps.updatepassword) {
            return this.props.updatepassword;
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.status) {
                this.props.openDialogResponse();
            } else {
                this.setState(prevState => {
                    return {errormemberoldpassword: snapshot.message}
                });
                this._memberoldpassword.focus();
            }
        }
    }

    omitField = async (currentField) => {
        let except = ['memberid', 'readyToSubmit', 'errormemberoldpassword', 'errormemberpassword', 'errormemberconfirmpassword'];
        except.push(currentField);
        let o = _.omit(this.state, except)
        let a = await _.findKey(o, val => {
            if(_.isEmpty(val)) { return true }
        });
        return a;
    }

    render() {
        const { loading } = this.props;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={styles.container}>
                    <View style={[styles.boxInputForm, { paddingLeft: 20, paddingRight: 20 }]}>
                        <Text style={styles.title}>ATUR KATA SANDI</Text>
                        <Text style={{ marginBottom: 30 }}>Atur kata sandi anda</Text>

                        <TextInput 
                            refs={ref => this._memberoldpassword = ref }
                            type="password"
                            label="Kata Sandi Lama"
                            value={this.state.memberoldpassword}
                            returnKeyType="next"
                            onSubmitEditing={() => this._memberpassword.focus() }
                            onChangeText={ text => {
                                this.omitField('memberoldpassword').then(val => {
                                    this.setState({
                                        errormemberoldpassword: '',
                                        memberoldpassword: text,
                                        readyToSubmit: text !== "" && val === undefined ? true : false
                                    }) 
                                });
                            }}
                            errorMessage={this.state.errormemberoldpassword || "*) Wajib diisi"}
                        />

                        <TextInput 
                            refs={ref => this._memberpassword = ref }
                            type="password"
                            label="Kata Sandi Baru"
                            value={this.state.memberpassword}
                            returnKeyType="next"
                            onSubmitEditing={() => this._memberconfirmpassword.focus() }
                            onChangeText={ text => {
                                this.omitField('memberpassword').then(val => {
                                    let v = validation.single(text.length > 0 ? text.toString() : null, validatePassword.memberpassword);
                                    let vv = validation({ "memberpassword": text.toString(), "memberconfirmpassword": this.state.memberconfirmpassword.toString() }, confirmVaildatePassword );
                                    this.setState({
                                        memberpassword: text,
                                        errormemberpassword: v !== undefined ? v[0] : '',
                                        errormemberconfirmpassword: vv !== undefined ? vv.memberconfirmpassword[0] : '',
                                        readyToSubmit: text !== "" && val === undefined && vv === undefined ? true : false
                                    }) 
                                });
                            }}
                            errorMessage={this.state.errormemberpassword || "*) Wajib diisi"}
                        />

                        <TextInput 
                            refs={ref => this._memberconfirmpassword = ref }
                            type="password"
                            label="Ulangi Kata Sandi Baru"
                            value={this.state.memberconfirmpassword}
                            returnKeyType="go"
                            onSubmitEditing={() => {
                                if(this.state.readyToSubmit) {
                                    this.props.updatePassword(this.state) 
                                }
                            }}
                            onChangeText={ text => {
                                this.omitField('memberconfirmpassword').then(val => {
                                    let v = validation({ "memberpassword": this.state.memberpassword.toString(), "memberconfirmpassword": text.toString() }, confirmVaildatePassword );
                                    this.setState({
                                        memberconfirmpassword: text,
                                        readyToSubmit: text !== "" && val === undefined && v === undefined ? true : false,
                                        errormemberconfirmpassword: v !== undefined ? v.memberconfirmpassword[0] : ''
                                    }) 
                                });
                            }}
                            errorMessage={this.state.errormemberconfirmpassword || "*) Wajib diisi"}
                        />

                    </View>

                    <Button style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        Keyboard.dismiss();
                        if(loading !== true && this.state.readyToSubmit) {
                            this.props.updatePassword(this.state);
                        }
                    }}>
                        {loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Kirim</Text> }
                    </Button>
                </View>
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.pop();
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.props.updatepassword !== null && this.props.updatepassword.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.pop();
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Close</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </KeyboardAwareScrollView>
        );
    }

}

const mapStateToProps = ({ memberReducer, responseReducer }) => {
    const {updatepassword, loading} = memberReducer;
    return {updatepassword, loading, responseReducer};
}

export default connect(mapStateToProps, {
    updatePassword,
    openDialogResponse,
    closeDialogResponse
})(AturKataSandi);