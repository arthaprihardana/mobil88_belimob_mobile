/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 10:18:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-27 14:44:53
 */
import React, {Component} from 'react';
import {View, Keyboard, AsyncStorage, ActivityIndicator, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Placeholder from 'rn-placeholder';
import moment from 'moment';
import 'moment/locale/id';

import globalStyles from '../../assets/styles';
import {Text, Button, BlockHeader, Image} from '../component';
import styles from './styles';
import {WHITE, PRIMARY_COLOR, GREY_400, GREY_300, GREY_500, GREY_800, GREY_200, GREY_900} from '../../libraries/colors';
import {getMember, getLogout , getCheckToken} from '../../actions';

let version = require('../../../version.json').version;

type Props = {};
type State = {
    nama_member: string,
    email_member: string,
    phone_member: string
};

class Profil extends Component<Props, State> {

    state = {
        nama_member: null,
        email_member: null,
        phone_member: null,
        image_profile: null,
        memberid: null
    }

    componentDidMount() {
        this.getMember();
        this.props.getCheckToken();
    }

    getMember = () => {
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.props.getMember(member.MemberCode);
            }
        })
    }

    onSignOut = () => {
        Keyboard.dismiss();
        this.props.getLogout({memberid: this.state.memberid});
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.member !== prevProps.member) {
            if(this.props.member.status) {
                let member = this.props.member.data;
                return { member: member }
            } else {
                return { member: undefined }
            }
        }
        if(this.props.logout !== prevProps.logout) {
            return { logout: this.props.logout }
        }
        if(this.props.check !== prevProps.check) {
            return { check: this.props.check }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.member !== undefined) {
                this.setState(prevState => {
                    AsyncStorage.setItem('@global:detailMember', JSON.stringify(snapshot.member));
                    return {
                        nama_member: snapshot.member.MemberName,
                        email_member: snapshot.member.MemberMail,
                        phone_member: snapshot.member.MemberPhoneNumber,
                        image_profile: snapshot.member.MemberImgProfileUrl,
                        memberid: snapshot.member.MemberId
                    }
                })
            }
            if(snapshot.logout !== undefined) {
                if(snapshot.logout.status) {
                    AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                    Actions.reset('login');
                }
            }
            if(snapshot.check !== undefined) {
                if(snapshot.check !== null) {
                    if(snapshot.check.status) {
                        if(snapshot.check.data.invalid) {
                            AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                            Actions.reset('login', { invalid: true });
                        }
                    } else {
                        AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                        Actions.reset('login', { session: true });
                    }
                } else {
                    AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                    Actions.reset('login', { session: true });
                }
            }
        }
    }

    render() {
        const { loading } = this.props;
        const { nama_member, email_member, phone_member, image_profile} = this.state;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false}>
                <BlockHeader />
                <View style={styles.container}>
                    <View style={styles.boxProfile}>
                        <View style={styles.boxUserInfo}>
                            <View style={{ width: '40%' }}>
                                {loading ? 
                                <Placeholder.Box
                                    height={100}
                                    width={100}
                                    radius={5}
                                    animate="fade"
                                    /> : 
                                    <Image 
                                        style={{ width: 100, height: 100, borderRadius: 4 }} 
                                        thumbnailSource={require('../../assets/images/user-default.png')} 
                                        source={{ uri: image_profile }} 
                                        placeholderColor='#b3e5fc'/>
                                }
                            </View>
                            <View style={{ width: '60%' }}>
                                {loading ? 
                                <Placeholder.Paragraph
                                    animate="fade"
                                    lineNumber={3}
                                    textSize={16}
                                    lineSpacing={5}
                                    color={GREY_200}
                                    width="100%"
                                    lastLineWidth="70%"
                                    firstLineWidth="50%"
                                    /> :
                                    <View>
                                        <Text style={[styles.title, { fontSize: 22 }]}>{nama_member !== null ? nama_member.toUpperCase() : ""}</Text>
                                        <Text small>{email_member}</Text>
                                        <Text small>{phone_member}</Text>
                                    </View>
                                }
                            </View>
                        </View>
                        <TouchableOpacity style={styles.buttonKonfigurasi} onPress={() => Actions.push('aturProfil')}>
                            <Text style={{ color: GREY_800 }}>Atur Profil</Text>
                            <Ionicons name={'ios-arrow-forward'} size={16} color={GREY_800} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonKonfigurasi} onPress={() => Actions.push('aturKataSandi')}>
                            <Text style={{ color: GREY_800 }}>Atur Kata Sandi</Text>
                            <Ionicons name={'ios-arrow-forward'} size={16} color={GREY_800} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonKonfigurasi} onPress={() => Actions.push('aturPembayaran')}>
                            <Text style={{ color: GREY_800 }}>Atur Pembayaran</Text>
                            <Ionicons name={'ios-arrow-forward'} size={16} color={GREY_800} />
                        </TouchableOpacity>
                    </View>

                    <Button style={[globalStyles.buttonPrimary, { flexDirection: 'row' }]} onPress={this.onSignOut}>
                        {this.props.loadingLogout ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: WHITE, letterSpacing: 2 }}>Keluar</Text> }
                    </Button>
                    <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                        <Text small style={{ color: '#ccc' }}>version {version}</Text>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = ({ memberReducer, loginReducer, checkTokenReducer }) => {
    const { member, loading } = memberReducer;
    const { check } = checkTokenReducer;
    const { logout } = loginReducer;
    const loadingLogout = loginReducer.loading;
    return { member, loading, loadingLogout, logout, check };
}

export default connect(mapStateToProps, {
    getMember,
    getLogout,
    getCheckToken,
})(Profil);