/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 14:37:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-14 13:57:54
 */
import {StyleSheet} from 'react-native';
import {GREY_300, WHITE, PRIMARY_COLOR, GREY_500, RED_100, RED_700} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26
    },
    boxProfile: {
        width: '100%', 
        // borderColor: GREY_300, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1
    },
    boxInputForm: {
        width: '100%', 
        // borderColor: GREY_300, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        // paddingLeft: 20,
        // paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1
    },
    title: { 
        color: PRIMARY_COLOR, 
        fontSize: 20,
        // fontWeight: 'bold'
    },
    boxUserInfo: { 
        flexDirection: 'row', 
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingTop: 15, 
        paddingBottom: 15,
        // minHeight: 110,
    },
    buttonKonfigurasi: { 
        justifyContent: 'space-between', 
        paddingLeft: 20, 
        paddingRight: 20, 
        alignItems: 'center', 
        flexDirection: 'row', 
        height: 55, 
        borderTopWidth: 0.7, 
        borderTopColor: '#eee'
    },
    boxButtonUpload: { 
        flexDirection: 'row', 
        borderTopWidth: 1, 
        borderTopColor: '#eee' 
    },
    buttonUpload: {
        width: '50%', 
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center', 
        padding: 10,
        height: 80
        // paddingLeft: 24
    },
    buttonUbahFoto: { 
        position: 'absolute', 
        flexDirection: 'row', 
        width: 100, 
        height: 20, 
        backgroundColor: 'rgba(0,0,0,0.3)', 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    }
});

export default styles;