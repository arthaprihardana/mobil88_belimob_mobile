/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 13:16:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-27 14:42:28
 */
import React, { Component } from 'react';
import {View, Keyboard, Alert, AsyncStorage, ActivityIndicator, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Actions} from 'react-native-router-flux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Placeholder from 'rn-placeholder';
import _ from 'lodash';
import {Text, TextInput, Button, BlockHeader, Select, Image, Dialog} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_400, GREY_200} from '../../libraries/colors';
import styles from './styles';
import {getMember, updateMember, updateFotoProfil, member, getJabatan, openDialogJabatan, closeDialogJabatan, openDialogResponse, closeDialogResponse, openCamera, openCameraRoll, closeCamera, closeCameraRoll } from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    MemberId: String,
    MemberCode: String,
    MemberName: String,
    MemberMail: String,
    MemberAddress: String,
    MemberPhoneNumber: String,
    MemberIdCardNumber: String,
    MemberCompanyName: String,
    MemberPosition: String,
    MemberOtherPosition: String,
    MemberSosmedFacebook: String,
    MemberSosmedTwitter: String,
    MemberSosmedGPlus: String,
    MemberSosmedInstagram: String,
    MemberSosmedLinkedin: String,
    ReferenceMemberCode: String,
    MemberBankAccountName: String,
    MemberBankAccountNumber: String,
    MemberBankName: String,
    MemberBankCode: String,
    MemberTaxNumber: String,
    MemberImgProfileUrl: String,
    MemberImgIdCardUrl: String,
    MemberImgIdCompanyCardUrl: String,
    MemberStatus: String,
    MemberReasonId: String,
    MemberOtherReason: String,
    masterJabatan: Array,
    ktp: String,
    idcardperusahaan: String,
    foto:String,
    readyToSubmit: Boolean
};

class AturProfil extends Component<Props, State> {
    
    state = {
        MemberId: null,
        MemberCode: null,
        MemberName: null,
        MemberMail: null,
        MemberAddress: null,
        MemberPhoneNumber: null,
        MemberIdCardNumber: null,
        MemberCompanyName: null,
        MemberPosition: null,
        MemberOtherPosition: null,
        MemberSosmedFacebook: null,
        MemberSosmedTwitter: null,
        MemberSosmedGPlus: null,
        MemberSosmedInstagram: null,
        MemberSosmedLinkedin: null,
        ReferenceMemberCode: null,
        MemberBankAccountName: null,
        MemberBankAccountNumber: null,
        MemberBankName: null,
        MemberBankCode: null,
        MemberTaxNumber: null,
        MemberImgProfileUrl: null,
        MemberImgIdCardUrl: null,
        MemberImgIdCompanyCardUrl: null,
        MemberStatus: null,
        MemberReasonId: null,
        MemberOtherReason: null,
        masterJabatan: [],
        ktp: null,
        idcardperusahaan: null,
        foto:null,
        readyToSubmit: false
    }

    handleCameraOrGallery = (type) => {
        Alert.alert(
            'Info',
            'Unggah foto dari?',
            [
                {text: 'Cancel', onPress: () => console.log('cancel') },
                {text: 'Galeri', onPress: () => {
                    this.props.openCameraRoll(type)
                }},
                {text: 'Kamera', onPress: () => {
                    this.props.openCamera(type);
                }},
            ],
            { cancelable: false }
        )
    }

    omitField = async (currentField) => {
        let except = ['MemberId', 'MemberCode', 'MemberName', 'MemberMail', 'MemberAddress', 'MemberPhoneNumber', 'MemberCompanyName', 'MemberPosition', 'MemberOtherPosition', 'MemberSosmedFacebook', 'MemberSosmedTwitter', 'MemberSosmedGPlus', 'MemberSosmedInstagram', 'MemberSosmedLinkedin', 'ReferenceMemberCode', 'MemberBankAccountName', 'MemberBankAccountNumber', 'MemberBankName', 'MemberBankCode', 'MemberTaxNumber', 'MemberImgProfileUrl', 'MemberImgIdCardUrl', 'MemberImgIdCompanyCardUrl', 'MemberStatus', 'MemberReasonId', 'MemberOtherReason', 'masterJabatan', 'ktp', 'idcardperusahaan', 'foto', 'readyToSubmit'];
        except.push(currentField);
        let o = _.omit(this.state, except)
        let a = await _.findKey(o, val => {
            if(_.isEmpty(val)) { return true }
        });
        return a;
    }

    componentDidMount() {
        this.getMember();
        this.props.getJabatan();
    }

    componentDidUpdate(prevProps, prevState) {

        if(this.props.cameraRollOpen !== prevProps.cameraRollOpen) {
            if(this.props.cameraRollOpen) {
                this.openGaleri(this.props.type);
            }
        }

        if(this.props.cameraOpen !== prevProps.cameraOpen) {
            if(this.props.cameraOpen) {
                this.openCamera(this.props.type)
            }
        }

        if(this.props.updatemember !== prevProps.updatemember) {
            let response = this.props.updatemember;
            if(response.status) {
                this.props.openDialogResponse();
            } else {
                Alert.alert(
                    'Info',
                    response.message || response.membernoktp[0],
                    [
                        {text: 'OK', onPress: () => {}},
                    ],
                    { cancelable: false }
                )
            }
        }

        if(this.props.pictureProfile !== prevProps.pictureProfile) {
            this.setState({foto: this.props.pictureProfile}, () => {
                this.props.updateFotoProfil({
                    memberimgprofile: this.props.pictureProfile.base64,
                    memberid: this.state.MemberId
                })
            });
        }

        if(this.props.jabatan !== prevProps.jabatan) {
            if(this.props.jabatan.status) {
                this.setState({ masterJabatan: this.props.jabatan.data});
            }
        }
    }

    componentWillUnmount() {
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.props.getMember(member.MemberCode);
            }
        })
    }

    openGaleri = (type) => {
        Actions.push('galeriCamera', {type: type, onBack: () => this.props.closeCameraRoll() });
    }

    openCamera = (type) => {
        Actions.push('camera', {type: type, onBack: () => this.props.closeCamera() });
    }

    getMember = () => {
        AsyncStorage.getItem('@global:detailMember', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.setState(prevState => {
                    return {
                        "MemberId": member.MemberId,
                        "MemberCode": member.MemberCode,
                        "MemberName": member.MemberName,
                        "MemberMail": member.MemberMail,
                        "MemberAddress": member.MemberAddress,
                        "MemberPhoneNumber": member.MemberPhoneNumber,
                        "MemberIdCardNumber": member.MemberIdCardNumber,
                        "MemberCompanyName": member.MemberCompanyName,
                        "MemberPosition": member.MemberPosition,
                        "MemberOtherPosition": member.MemberOtherPosition,
                        "MemberSosmedFacebook": member.MemberSosmedFacebook,
                        "MemberSosmedTwitter": member.MemberSosmedTwitter,
                        "MemberSosmedGPlus": member.MemberSosmedGPlus,
                        "MemberSosmedInstagram": member.MemberSosmedInstagram,
                        "MemberSosmedLinkedin": member.MemberSosmedLinkedin,
                        "ReferenceMemberCode": member.ReferenceMemberCode,
                        "MemberBankAccountName": member.MemberBankAccountName,
                        "MemberBankAccountNumber": member.MemberBankAccountNumber,
                        "MemberBankName": member.MemberBankName,
                        "MemberBankCode": member.MemberBankCode,
                        "MemberTaxNumber": member.MemberTaxNumber,
                        "MemberImgProfileUrl": member.MemberImgProfileUrl,
                        "MemberImgIdCardUrl": member.MemberImgIdCardUrl,
                        "MemberImgIdCompanyCardUrl": member.MemberImgIdCompanyCardUrl,
                        "MemberStatus": member.MemberStatus,
                        "MemberReasonId": member.MemberReasonId,
                        "MemberOtherReason": member.MemberOtherReason,
                    }
                }, () => {
                    this.omitField().then(val => {
                        this.setState(prevState => {
                            return { readyToSubmit: val === undefined ? true : false }
                        })
                    })
                })
            }
        });
    }

    onSubmit = () => {
        this.props.updateMember({
            memberaddress: this.state.MemberAddress,
            membernoktp: this.state.MemberIdCardNumber,
            membercompany: this.state.MemberCompanyName,
            memberposition: this.state.MemberPosition,
            memberotherposition: this.state.MemberOtherPosition,
            membermedsosfacebook: this.state.MemberSosmedFacebook,
            membermedsostwitter: this.state.MemberSosmedTwitter,
            membermedsosinstagram: this.state.MemberSosmedInstagram,
            membermedsosgoogleplus: this.state.MemberSosmedGPlus,
            membermedsoslinkedin: this.state.MemberSosmedLinkedin,
            kodereferral: this.state.ReferenceMemberCode,
            memberid: this.state.MemberId
        });
    }

    render() {
        const { loading } = this.props;
        const { MemberId, MemberCode, MemberName, MemberMail, MemberAddress, MemberPhoneNumber, MemberIdCardNumber, MemberCompanyName, MemberPosition, MemberOtherPosition, MemberSosmedFacebook, MemberSosmedTwitter, MemberSosmedGPlus, MemberSosmedInstagram, MemberSosmedLinkedin, ReferenceMemberCode, MemberBankAccountName, MemberBankAccountNumber, MemberBankName, MemberBankCode, MemberTaxNumber, MemberImgProfileUrl, MemberImgIdCardUrl, MemberImgIdCompanyCardUrl, MemberStatus, MemberReasonId, MemberOtherReason } = this.state;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={styles.container}>
                    <View style={styles.boxProfile}>
                        <View style={styles.boxUserInfo}>
                            <View style={{ width: '40%' }}>
                                {loading ? 
                                    <Placeholder.Box
                                        height={100}
                                        width={100}
                                        radius={5}
                                        animate="fade"
                                        /> 
                                        : 
                                    <View>
                                        <Image 
                                            style={{ width: 100, height: 100, borderRadius: 4 }} 
                                            thumbnailSource={require('../../assets/images/user-default.png')} 
                                            source={{ uri: this.state.foto !== null ? this.state.foto.path : MemberImgProfileUrl }} 
                                            placeholderColor='#b3e5fc'/>
                                        <Button style={styles.buttonUbahFoto} onPress={() => this.handleCameraOrGallery("updatememberimageprofile") }>
                                            <FontAwesome5 name="camera" color={GREY_400} size={10} style={{ marginRight: 5}} />
                                            <Text style={{ color: WHITE, fontSize: 10, lineHeight: 15 }}>Ubah Foto</Text>
                                        </Button>
                                    </View>}
                            </View>
                            <View style={{ width: '60%' }}>
                                {loading ? <Placeholder.Paragraph
                                    animate="fade"
                                    lineNumber={3}
                                    textSize={16}
                                    lineSpacing={5}
                                    color={GREY_200}
                                    width="100%"
                                    lastLineWidth="70%"
                                    firstLineWidth="50%"
                                    /> :
                                <View>
                                    <Text style={[styles.title, { fontSize: 22 }]}>{MemberName}</Text>
                                    <Text small>{MemberMail}</Text>
                                    <Text small>{MemberPhoneNumber}</Text>
                                </View> }
                            </View>
                        </View>
                    </View>
                    <View style={[styles.boxInputForm, { paddingLeft: 20, paddingRight: 20 }]}>
                        
                        <Text style={styles.title}>PROFIL</Text>
                        <Text style={{ marginBottom: 30 }}>Atur profil anda</Text>
                        
                        <TextInput 
                            refs={ref => this._kodereferal = ref}
                            label="Kode Referal"
                            value={this.state.ReferenceMemberCode}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({ReferenceMemberCode: text}) }
                        />
                        
                        <TextInput 
                            refs={ref => this._email = ref}
                            label="Email"
                            value={this.state.MemberMail}
                            editable={false}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberMail: text}) }
                        />

                        <TextInput 
                            refs={ref => this._noHandphone = ref}
                            label="No Handphone"
                            value={this.state.MemberPhoneNumber}
                            keyboardType="number-pad"
                            maxLength={13}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberPhoneNumber: text}) }
                        />
                        
                        <TextInput 
                            refs={ref => this._alamat = ref}
                            label="Alamat"
                            value={this.state.MemberAddress}
                            autoCapitalize="words"
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberAddress: text}) }
                        />

                        <TextInput 
                            refs={ref => this._noktp = ref}
                            label="No KTP"
                            value={this.state.MemberIdCardNumber}
                            keyboardType="number-pad"
                            maxLength={16}
                            returnKeyType="next"
                            onChangeText={ text => {
                                this.omitField('MemberIdCardNumber').then(val => {
                                    this.setState({
                                        MemberIdCardNumber: text,
                                        readyToSubmit: text !== '' ? true : false
                                    }) 
                                });
                            }}
                            errorMessage={"*) Wajib diisi"}
                        />

                        <TextInput 
                            refs={ref => this._perusahaan = ref}
                            label="Perusahaan"
                            value={this.state.MemberCompanyName}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberCompanyName: text}) }
                        />

                        <Select 
                            label="Jabatan"
                            onPress={() => this.props.openDialogJabatan()}
                            value={this.state.MemberPosition || "Pilih"}
                            loading={this.props.jabatanLoading}
                            />

                        {/Lain/i.test(this.state.MemberPosition) ? 
                            <TextInput 
                                refs={ref => this._memberotherposition = ref}
                                label="Jabatan Lainnya"
                                value={this.state.MemberOtherPosition}
                                returnKeyType="next"
                                onChangeText={ text => this.setState({MemberOtherPosition: text}) }
                                onSubmitEditing={() => {} }
                            />
                        : <View />}
                        
                    </View>

                    <View style={[styles.boxInputForm, { paddingLeft: 20, paddingRight: 20 }]}>
                        <Text style={styles.title}>AKUN SOSIAL MEDIA</Text>
                        <Text style={{ marginBottom: 30 }}>Atur sosial media anda</Text>

                        <TextInput 
                            refs={ref => this._facebook = ref}
                            label="Facebook"
                            value={this.state.MemberSosmedFacebook}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberSosmedFacebook: text}) }
                        />

                        <TextInput 
                            refs={ref => this._twitter = ref}
                            label="Twitter"
                            value={this.state.MemberSosmedTwitter}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberSosmedTwitter: text}) }
                        />

                        <TextInput 
                            refs={ref => this._instagram = ref}
                            label="Instagram"
                            value={this.state.MemberSosmedInstagram}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberSosmedInstagram: text}) }
                        />

                        <TextInput 
                            refs={ref => this._googleplus = ref}
                            label="Google Plus"
                            value={this.state.MemberSosmedGPlus}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberSosmedGPlus: text}) }
                        />

                        <TextInput 
                            refs={ref => this._linkedin = ref}
                            label="Linkedin"
                            value={this.state.MemberSosmedLinkedin}
                            returnKeyType="next"
                            onChangeText={ text => this.setState({MemberSosmedLinkedin: text}) }
                        />
                    </View>

                    <Button style={[this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled, { marginBottom: 20 }]} onPress={() => {
                        Keyboard.dismiss();
                        this.onSubmit();
                        }}>
                        {this.props.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Kirim</Text> }
                    </Button>
                </View>
                <Dialog 
                    title={"Jabatan"}
                    onRequestClose={() => this.props.closeDialogJabatan()}
                    visible={this.props.dialog}>
                    {this.state.masterJabatan && this.state.masterJabatan.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            let self = this;
                            this.props.closeDialogJabatan();
                            this.setState({ MemberPosition: value.MemberPositionName });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.MemberPositionName}</Text>
                        </TouchableOpacity>
                    ))}
                </Dialog>
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.pop();
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.props.updatemember !== null && this.props.updatemember.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.pop();
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Close</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </KeyboardAwareScrollView>
        );
    }

}

const mapStateTprops = ({ memberReducer, jabatanReducer, responseReducer, takePictureReducer }) => {
    const { jabatan, dialog } = jabatanReducer;
    const jabatanLoading = jabatanReducer.loading;
    const { loading, updatemember, updatefotoprofil } = memberReducer;
    const { cameraOpen, cameraRollOpen, type, pictureProfile, pictureKtp, pictureCardId } = takePictureReducer;
    return { jabatanLoading, jabatan, dialog, loading, updatemember, updatefotoprofil, responseReducer, cameraOpen, cameraRollOpen, type, pictureProfile, pictureKtp, pictureCardId };
}

export default connect(mapStateTprops, {
    getMember,
    member,
    updateMember,
    getJabatan,
    updateFotoProfil,
    openDialogJabatan,
    closeDialogJabatan,
    openDialogResponse,
    closeDialogResponse,
    openCamera,
    closeCamera,
    openCameraRoll,
    closeCameraRoll,
})(AturProfil);