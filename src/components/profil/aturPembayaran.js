/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 14:29:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-17 14:33:19
 */
import React, { Component } from 'react';
import {View, Keyboard, AsyncStorage, Alert, TouchableOpacity, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import {Text, Button, TextInput, BlockHeader, Dialog, Select} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_400} from '../../libraries/colors';
import styles from './styles';
import {updatePembayaran, openDialogBank, closeDialogBank, getMember, getBank, openDialogResponse, closeDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    memberbankaccountname: String,
    errormemberbankaccountname: String,
    memberbankaccountnumber: String,
    errormemberbankaccountnumber: String,
    memberbankname: String,
    errormemberbankname: String,
    memberbankcode: String,
    membertaxnumber: String,
    errormembertaxnumber: String,
    memberid: String,
    arrBank: Array,
    masterBank: Array,
    readyToSubmit: Boolean
};

class AturPembayaran extends Component<Props, State> {
    
    state = {
        memberbankaccountname: '',
        errormemberbankaccountname: '',
        memberbankaccountnumber: '',
        errormemberbankaccountnumber: '',
        memberbankname: '',
        errormemberbankname: '',
        memberbankcode: '',
        membertaxnumber: '',
        errormembertaxnumber: '',
        memberid: '',

        arrBank: [],
        masterBank: [],
        readyToSubmit: false
    }

    componentDidMount() {
        this.props.getBank();
        AsyncStorage.getItem('@global:detailMember', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.setState(prevState => { 
                    return {
                        memberid: member.MemberId,
                        memberbankaccountname: member.MemberBankAccountName,
                        memberbankaccountnumber: member.MemberBankAccountNumber,
                        memberbankname: member.MemberBankName,
                        memberbankcode: member.MemberBankCode,
                        membertaxnumber: member.MemberTaxNumber
                    }
                }, () => {
                    this.omitField().then((val => {
                        if(val === undefined) {
                            this.setState({ readyToSubmit: true })
                        }
                    }))
                })
            }
        })
    }

    componentWillUnmount() {
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.props.getMember(member.MemberCode);
            }
        })
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.bank !== prevProps.bank) {
            let data = this.props.bank.data;
            return { masterBank: data };
        }
        if(this.props.updatepembayaran !== prevProps.updatepembayaran) {
            return { updatePembayaran: this.props.updatepembayaran }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.masterBank !== undefined) {
                this.setState(prevState => { 
                    return { masterBank: snapshot.masterBank }
                });
            }
            if(snapshot.updatePembayaran !== undefined) {
                if(snapshot.updatePembayaran.status) {
                    this.props.openDialogResponse();
                } else {
                    Alert.alert(
                        'Info',
                        snapshot.updatePembayaran.membertaxnumber[0],
                        [
                            {text: 'OK', onPress: () => {} },
                        ],
                        { cancelable: false }
                    )
                }
            }
        }
    }

    onSubmit = () => {
        this.props.updatePembayaran({
            memberbankaccountname: this.state.memberbankaccountname,
            memberbankaccountnumber: parseInt(this.state.memberbankaccountnumber),
            memberbankname: this.state.memberbankname,
            memberbankcode: this.state.memberbankcode,
            membertaxnumber: this.state.membertaxnumber,
            memberid: this.state.memberid,
        })
    }

    omitField = async (currentField) => {
        let except = ['errormemberbankaccountname', 'errormemberbankaccountnumber', 'errormemberbankname', 'memberbankcode', 'errormembertaxnumber', 'memberid', 'arrBank', 'masterBank', 'readyToSubmit'];
        except.push(currentField);
        let o = _.omit(this.state, except)
        let a = await _.findKey(o, val => {
            if(_.isEmpty(val)) { return true }
        });
        return a;
    }

    render() {
        const { loading } = this.props;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false}>
                <BlockHeader />
                <View style={[styles.container, { paddingBottom: 20 }]}>
                    <View style={[styles.boxInputForm, { paddingLeft: 20, paddingRight: 20 }]}>
                        <Text style={styles.title}>ATUR PEMBAYARAN</Text>
                        <Text style={{ marginBottom: 30 }}>Atur pembayaran anda</Text>

                        <TextInput 
                            refs={ref => this._namaPemilikRekening = ref }
                            label="Nama Pemilik Rekening"
                            value={this.state.memberbankaccountname}
                            returnKeyType="next"
                            onSubmitEditing={() => this._nomorRekening.focus() }
                            onChangeText={ text => {
                                this.omitField('memberbankaccountname').then(val => {
                                    this.setState({
                                        memberbankaccountname: text,
                                        readyToSubmit: text !== '' && val === undefined ? true : false
                                    }) 
                                })
                            }}
                            errorMessage={"*) Wajib diisi"}
                        />

                        <TextInput 
                            refs={ref => this._nomorRekening = ref }
                            label="No Rekening"
                            value={this.state.memberbankaccountnumber}
                            returnKeyType="next"
                            onSubmitEditing={() => this.props.openDialogBank() }
                            onChangeText={ text => {
                                this.omitField('memberbankaccountnumber').then(val => {
                                    this.setState({
                                        memberbankaccountnumber: text,
                                        readyToSubmit: text !== '' && val === undefined ? true : false
                                    }) 
                                })
                            }}
                            keyboardType="number-pad"
                            errorMessage={"*) Wajib diisi"}
                        />
                        
                        <Select 
                            ref={ref => this._memberbankname = ref }
                            label="Nama Bank"
                            onPress={() => this.props.openDialogBank()}
                            value={this.state.memberbankname || "Pilih"}
                            loading={this.props.loadingBank}
                            errorMessage={"*) Wajib diisi"}
                            />

                        <TextInput 
                            refs={ref => this._npwp = ref }
                            label="NPWP (untuk kepentingan pajak)"
                            value={this.state.membertaxnumber}
                            returnKeyType="go"
                            maxLength={15}
                            onSubmitEditing={() => {
                                if(this.state.readyToSubmit) {
                                    this.onSubmit();
                                }
                            }}
                            onChangeText={ text => {
                                this.omitField('membertaxnumber').then(val => {
                                    this.setState({
                                        membertaxnumber: text,
                                        readyToSubmit: text !== '' && val === undefined ? true : false
                                    }) 
                                })
                            }}
                            keyboardType="number-pad"
                            errorMessage={"*) Wajib diisi"}
                        />

                    </View>

                    <Button style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        Keyboard.dismiss();
                        if(loading !== true && this.state.readyToSubmit) {
                            this.onSubmit();
                        }
                    }}>
                        {loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Kirim</Text> }
                    </Button>
                </View>
                <Dialog 
                    title={"Bank"}
                    onRequestClose={() => this.props.closeDialogBank()}
                    visible={this.props.dialog}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search Bank" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterBank: _.filter(this.props.bank.data, obj => new RegExp(text, 'i').test(obj.BankName) )
                                }
                            })
                        }} />
                    </View>
                    {this.state.masterBank && this.state.masterBank.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            let self = this;
                            this.props.closeDialogBank();
                            this.omitField('memberbankname').then(val => {
                                this.setState({
                                    masterBank: this.props.bank.data, 
                                    memberbankcode: value.BankCode,
                                    memberbankname: value.BankName,
                                    readyToSubmit: val === undefined ? true : false
                                })
                            });
                            this._npwp.focus();
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.BankName}</Text>
                        </TouchableOpacity>
                    ))}
                </Dialog>
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.pop();
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.props.updatepembayaran !== null && this.props.updatepembayaran.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.pop();
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Close</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = ({ memberReducer, bankReducer }) => {
    const { loading, updatepembayaran } = memberReducer;
    const { bank, dialog } = bankReducer;
    const loadingBank = bankReducer.loading;
    return { loadingBank, loading, updatepembayaran, bank, dialog };
}

export default connect(mapStateToProps, {
    getMember,
    updatePembayaran,
    getBank,
    openDialogBank,
    closeDialogBank,
    openDialogResponse,
    closeDialogResponse
})(AturPembayaran);