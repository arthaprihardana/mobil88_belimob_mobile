/**
 * @author: Artha Prihardana 
 * @Date: 2018-11-12 10:25:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-12 10:37:30
 */
import React, { Component } from 'react';
import { View } from 'react-native';
import Placeholder from 'rn-placeholder';

export default leadLoader = () => {
    return (
        <View style={{ flexDirection: 'row', marginBottom: 13 }}>
            <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                <Placeholder.Media
                    size={70}
                    color="#eee"
                    animate="fade"
                    />
            </View>
            <View style={{ width: '50%', paddingLeft: 16, justifyContent: 'center' }}>
                <Placeholder.Paragraph
                    lineNumber={3}
                    textSize={16}
                    lineSpacing={5}
                    color="#eee"
                    width="100%"
                    lastLineWidth="70%"
                    firstLineWidth="50%"
                    animate="fade"
                    />
            </View>
            <View style={{ width: '30%', justifyContent: 'center', paddingLeft: 20 }}>
                <Placeholder.Line
                    color="#eee"
                    width="70%"
                    animate="fade"
                    textSize={22}
                    />
            </View>
        </View>
    )
}