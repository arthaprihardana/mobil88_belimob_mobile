/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 14:37:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-02 16:13:29
 */
import {StyleSheet} from 'react-native';
import {GREY_100, WHITE, PRIMARY_COLOR, GREY_400, GREY_700} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26,
        paddingBottom: 16
    },
    boxContent: {
        width: '100%', 
        backgroundColor: WHITE,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: { 
        color: PRIMARY_COLOR, 
        fontSize: 18
    },
    list: {
        width: '100%', 
        height: 100, 
        borderBottomColor: GREY_100, 
        borderBottomWidth: 1, 
        padding: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    listDateSection: { 
        width: 100, 
        height: 100, 
        justifyContent: 'center', 
        alignItems: 'center',
        borderWidth: 1, 
        backgroundColor: '#fff3cd',
        borderColor: '#ffeeba', 
        borderRadius: 50 
    },
    listDateSectionText: {
        // color: '#856404'
        color: GREY_700
    },
    listStatusSection: { 
        width: 100, 
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        backgroundColor: '#eee',
    }
});

export default styles;