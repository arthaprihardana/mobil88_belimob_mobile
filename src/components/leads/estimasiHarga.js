/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-13 13:34:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-09 14:22:22
 */
import React, {Component} from 'react';
import {ScrollView, View, processColor, TouchableOpacity, Switch, ActivityIndicator, WebView} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as _ from 'lodash';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader, Dialog, TextInput, Select, Button} from '../component';
import styles from './styles';
import {GREY_300, RED_500, GREY_600, WHITE, PRIMARY_COLOR} from '../../libraries/colors';
import globalStyles from '../../assets/styles';
import { getMrp, leadsNotAppointment, closeDialogResponse, openDialogResponse, getMrpDescription, closeDialogMrpDescription, openDialogMrpDescription } from '../../actions';
import DialogResponse from '../master/response';
import DialogSyaratKetentuan from '../master/syaratKetentuan';

type Props = {};
type State = {
    minmrp: String,
    maxmrp: String,
    mrpprice: String,
    leadreferencecode: String,
    leadid: String,
    mrpdescription: String
};

class EstimasiHarga extends Component<Props, State> {

    state = {
        "minmrp": null,
        "maxmrp": null,
        "mrpprice": null,
        "leadreferencecode": null,
        "leadid": null,
        "mrpdescription": null,
        "message": null
    }

    componentDidMount() {
        this.props.getMrpDescription();
        this.props.getMrp(this.props.params);
        this.setState(prevState => {
            return {
                leadreferencecode: this.props.params.leadreferencecode,
                leadid: this.props.params.leadid
            }
        });
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.mrp !== prevProps.mrp) {
            return {mrp: this.props.mrp};
        }
        if(this.props.noAppointment !== prevProps.noAppointment) {
            return {noAppointment: this.props.noAppointment}
        }
        if(this.props.mrpdescription !== prevProps.mrpdescription) {
            return {mrpdescription: this.props.mrpdescription}
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.mrp !== undefined) {
                if(snapshot.mrp.status) {
                    this.setState({
                        "message": snapshot.mrp.data.message,
                        "minmrp": snapshot.mrp.data.minmrp,
                        "maxmrp": snapshot.mrp.data.maxmrp,
                        "mrpprice": snapshot.mrp.data.mrpprice
                    })
                }
            }
            if(snapshot.noAppointment !== undefined) {
                this.props.openDialogResponse();
            }
            if(snapshot.mrpdescription !== undefined) {
                this.setState(prevState => {
                    return { mrpdescription: snapshot.mrpdescription.data[0].MrpDescriptionName  }
                })
            }
        }
    }

    render() {
        const { minmrp, maxmrp, mrpprice, leadreferencecode, leadid, message } = this.state;
        return (
            <ScrollView style={{ flex: 1 }}>
                <BlockHeader />
                <View style={styles.container}>
                    <View style={[styles.boxContent, { padding: 20, alignItems: 'flex-start' }]}>
                        <View style={{ width: '100%', marginBottom: 16, alignItems: 'center'}}>
                            <Text h4 style={{ color: PRIMARY_COLOR }}>Estimasi Harga</Text>
                        </View>
                        <View style={{ width: '100%' }}>
                            {message !== null ? 
                            <View>
                                <Text>{message}</Text>
                            </View> :
                            <View>
                                <View style={{ marginBottom: 16, alignItems: 'center' }}>
                                    <Text small>Min Price</Text>
                                    <Text h2 style={{ color: '#155724', lineHeight: 32 }}>Rp. {minmrp || 0}</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text small>Max Price</Text>
                                    <Text h2 style={{ color: '#155724', lineHeight: 32 }}>Rp. {maxmrp || 0}</Text>
                                </View>
                            </View> 
                            }
                        </View>
                    </View>
                    <View style={[styles.boxContent, { alignItems: 'flex-start' }]}>
                        <TouchableOpacity onPress={() => this.props.openDialogMrpDescription() } style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                            <Text>Syarat dan Ketentuan</Text>
                            <Ionicons name={'ios-arrow-forward'} size={16} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '100%', padding: 6 }}>
                        <Text>Apakah Anda Ingin Melanjutkan ?</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Button style={[globalStyles.buttonSecondary, { width: '48%'}]} onPress={() => {
                                this.props.leadsNotAppointment({
                                    minimumpricemrp: minmrp,
                                    maximumpricemrp: maxmrp,
                                    pricemrp: mrpprice,
                                    leadreferencecode: leadreferencecode,
                                    leadid: leadid
                                })
                            }}>
                            {this.props.loadingNoAppointment ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: WHITE, letterSpacing: 2 }}>TIDAK</Text> }
                        </Button>
                        <Button style={[globalStyles.buttonPrimary, { width: '48%'}]} onPress={() => Actions.push('jadwalPertemuan', {
                            minimumpricemrp: minmrp,
                            maximumpricemrp: maxmrp,
                            pricemrp: mrpprice,
                            leadreferencecode: leadreferencecode,
                            leadid: leadid
                            })}>
                            <Text style={{ color: WHITE, letterSpacing: 2 }}>YA</Text>
                        </Button>
                    </View>
                </View>
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.reset('main');
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.props.noAppointment && this.props.noAppointment.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.reset('main');
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Kembali</Text>
                        </Button>
                    </View>
                </DialogResponse>
                <DialogSyaratKetentuan title="Syarat dan Ketentuan" onClose={() => this.props.closeDialogMrpDescription() }>
                    <WebView
                        source={{html: this.state.mrpdescription}}
                        style={{ width: '100%', padding: 20 }}
                        renderLoading={() => <ActivityIndicator color={'#dc3545'} />}
                        startInLoadingState={this.props.loading}
                    />
                </DialogSyaratKetentuan>
            </ScrollView>
        );
    }

}

const mapStateToProps = ({ mrpReducer, leadsReducer }) => {
    const { loading, mrp, mrpdescription } = mrpReducer;
    const { noAppointment } = leadsReducer;
    const loadingNoAppointment = leadsReducer.loading;
    return { loading, mrp, loadingNoAppointment, noAppointment, mrpdescription };
}

export default connect(mapStateToProps, {
    getMrp,
    getMrpDescription,
    leadsNotAppointment,
    closeDialogResponse,
    openDialogResponse,
    closeDialogMrpDescription,
    openDialogMrpDescription
})(EstimasiHarga);