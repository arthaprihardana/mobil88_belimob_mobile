/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 10:18:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-21 14:01:31
 */
import React, {Component} from 'react';
import {ScrollView, View, ListView, TouchableOpacity, AsyncStorage, ActivityIndicator, RefreshControl, FlatList, SectionList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import Placeholder from 'rn-placeholder';
import moment from 'moment';
import 'moment/locale/id';
import {Text} from '../component';
import styles from './styles';
import Filter from './Filter';
import {GREY_100, GREY_300, RED_500, BLUE_200, GREY_700, GREY_800, GREEN_500, BLUE_500, ORANGE_500, RED_900, WHITE, PRIMARY_COLOR} from '../../libraries/colors';
import {openDialogFilter, closeDialogFilter, getLeads, getCheckToken} from '../../actions';
import LeadLoader from './leadLoader';

/**
 * Index Leads Component (Page List Leads)
 *
 * @class Leads
 * @extends {Component}
 * 
 * @constructs Leads
 */
class Leads extends Component {

    state = {
        dataSource: [],
        isNull: false,
        refreshing: false,
        member: null,
        page: 1,
        leadstatus: null,
        startdate: null,
        enddate: null,
        loading: false
    }

    /**
     * @async
     * @method componentDidMount
     * @description Get member from local storage, request leads data, and check token
     *
     * @memberof Leads
     */
    async componentDidMount() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember)
        this.setState({ member: member })
        this.props.getLeads({
            MemberCode: member.MemberCode,
            page: this.state.page
        });
        this.props.getCheckToken();
    }

    /**
     * @method getSnapshotBeforeUpdate
     *
     * @param {Object} prevProps
     * @param {Object} prevState
     * @returns
     * @memberof Leads
     */
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(prevProps.leads !== this.props.leads) {
            return {leads: this.props.leads};
        }
        if(this.props.check !== prevProps.check) {
            return { check: this.props.check }
        }
        if(this.state.leadstatus !== prevState.leadstatus) {
            return { filter: true }
        }
        return null;
    }

    /**
     * @method componentDidUpdate
     *
     * @param {Object} prevProps
     * @param {Object} prevState
     * @param {Object} snapshot
     * @memberof Leads
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.leads !== undefined) {
                if(snapshot.leads.status) {
                    if(snapshot.leads.data.current_page === 1 && snapshot.leads.data.data.length === 0) {
                        this.setState({ isNull: true, loading: false })
                    } else {
                        this.setState(prevState => {
                            if(prevState.dataSource.length > 0) {
                                if(snapshot.leads.data.data.length > 0 ) {
                                    if(prevState.dataSource[0].LeadId !== snapshot.leads.data.data[0].LeadId) {
                                        return { 
                                            dataSource: _.concat(prevState.dataSource, snapshot.leads.data.data !== undefined ? snapshot.leads.data.data : []),
                                            page: snapshot.leads.data.to === null ? snapshot.leads.data.last_page : prevState.page,
                                            loading: false
                                        }
                                    }
                                }
                            } else {
                                return { 
                                    dataSource: _.concat(prevState.dataSource, snapshot.leads.data.data !== undefined ? snapshot.leads.data.data : []),
                                    page: snapshot.leads.data.to === null ? snapshot.leads.data.last_page : prevState.page,
                                    loading: false
                                }
                            }
                        }, () => this.setState({ isNull: this.state.dataSource.length > 0 ? false : true, loading: false }) );
                    }
                } else {
                    this.setState({ isNull: true, loading: false })
                }
            }
            if(snapshot.check !== undefined) {
                if(snapshot.check !== null) {
                    if(snapshot.check.status) {
                        if(snapshot.check.data.invalid) {
                            AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                            Actions.reset('login', { invalid: true });
                        }
                    } else {
                        AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                        Actions.reset('login', { session: true });
                    }
                } else {
                    AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                    Actions.reset('login', { session: true });
                }
            }
            if(snapshot.filter !== undefined) {
                if(snapshot.filter) {
                    this.setState(prevState => {
                        return { dataSource: [] }
                    })
                }
            }
        }
    }

    /**
     * @method loadMoreData
     * @description Load more data with pagination
     *
     * @memberof Leads
     */
    loadMoreData = () => {
        this._lead.scrollToEnd({ animated: true })
        this.setState(prevState => {
            return { 
                page: prevState.page + 1,
                loading: true
            }
        }, () => {
            if(this.state.leadstatus !== null) {
                this.props.getLeads({
                    MemberCode: this.state.member.MemberCode,
                    page: this.state.page,
                    leadstatus: this.state.leadstatus,
                    startdate: this.state.startdate,
                    enddate: this.state.enddate
                });
            } else {
                this.props.getLeads({
                    MemberCode: this.state.member.MemberCode,
                    page: this.state.page
                });
            }
        })
    }

    /**
     * @method onRefresh
     * @description Trigger refresh list
     *
     * @memberof Leads
     */
    onRefresh() {
        this.setState(prevState => {
            return { 
                dataSource: [],
                leadstatus: null,
                startdate: null,
                enddate: null,
                page: 1
            }
        }, () => {
            this.props.getLeads({
                MemberCode: this.state.member.MemberCode,
                page: this.state.page
            });
        });
    }

    /**
     * @method renderItem
     *
     * @description Render item list
     * @param {Object} item
     * @param {Integer} index
     * @memberof Leads
     */
    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity
                key={index}
                onPress={() => Actions.push('detailLeads', { RefCode: item.LeadReferenceCode }) }
                style={[styles.list, { padding: 0 }]}>
                <View style={{ width: '20%', height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#eee' }}>
                    <Ionicons name="md-car" size={32} color="#777" />
                </View>
                <View style={{ width: '50%', paddingLeft: 16 }}>
                    <Text small style={styles.listDateSectionText}>{moment(item.LeadStatusTransitionDate).format('lll')}</Text>
                    <Text h6 style={{ color: '#155724' }}>{item.LeadCarBrand} {item.LeadCarModel}</Text>
                    <Text bold style={{ lineHeight: 19, color: GREY_800 }}>{item.LeadReferenceCode}</Text>
                </View>
                <View style={{ width: '30%' }}>
                    <View style={[styles.listStatusSection, { backgroundColor: item.leadstatus.LeadStatusColour }]}>
                        <Text small bold style={{ color: WHITE }}>{item.leadstatus.LeadStatusName}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    /**
     * @method emptyComponent
     *
     * @description Render empty component
     * @memberof Leads
     */
    emptyComponent = () => {
        const { loading } = this.props;
        const { isNull } = this.state;
        return (
            <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 13 }}>
                {loading &&
                    <View style={{ width: '100%' }}>
                        { [0,1,2,3,4,5].map((val, key) => (
                            <LeadLoader key={key} />
                        )) }
                    </View>
                }
                {isNull && 
                    <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <FontAwesome5 name="search" size={14} style={{ marginBottom: 10 }} />
                        <Text small style={{ color: '#818182'}}>Data Not Found</Text>
                    </View>
                }
            </View>
        )
    }

    /**
     * @method headerComponent
     * 
     * @description Render header component
     * @memberof Leads
     */
    headerComponent = () => {
        return (
            <View style={{ width: '100%', height: 86, borderBottomColor: GREY_100, borderBottomWidth: 1, backgroundColor: WHITE }}>
                <View style={{ width: '100%', height: 56, flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: GREY_100, borderBottomWidth: 1 }}>
                    <TouchableOpacity onPress={() => this.props.openDialogFilter() } style={{ width: '85%', height: 56, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, borderRightColor: GREY_100, borderRightWidth: 1}}>
                        {this.state.leadstatus !== null ? 
                            <TouchableOpacity onPress={() => this.onRefresh()} style={{ marginRight: 16 }}>
                                <Ionicons name="md-close" color={PRIMARY_COLOR} size={16} />
                            </TouchableOpacity>
                            : 
                            <FontAwesome5 name="search" style={{ marginRight: 16 }} />
                        }
                        {this.state.leadstatus !== null ? 
                            <Text small style={{ color: GREY_700 }}>{`Status: ${_.startCase(this.state.leadstatus)}; From: ${moment(this.state.startdate).format('L')}; To: ${moment(this.state.enddate).format('L')}`}</Text>
                            :
                            <Text style={{ color: GREY_700 }}>Filter</Text>
                        }
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '15%', height: 56, justifyContent: 'center', alignItems: 'center'}} onPress={() => Actions.push('formLeads') }>
                        <FontAwesome5 name="plus" />
                    </TouchableOpacity>
                </View>
                <View style={{ height: 30, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                    <Ionicons name="ios-arrow-dropdown" size={9} />
                    <Text style={{ fontSize: 9, marginLeft: 10, marginRight: 10 }}>Tarik ke bawah untuk memperbarui lead</Text>
                    <Ionicons name="ios-arrow-dropdown" size={9} />
                </View>
            </View>
        )
    }

    /**
     * @method footerComponent
     * 
     * @description Render footer component
     * @memberof Leads
     */
    footerComponent = () => {
        return (
            this.state.loading ? 
            <View style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="small" color="#6c757d" />
            </View>
            :
            <View />
        )
    }

    /**
     * @method render
     *
     * @returns {Object} - Kompenen List Leads
     * @memberof Leads
     */
    render() {
        const { isNull, refreshing, dataSource, member } = this.state;
        return (
        <View>
            <FlatList 
                ref={ref => this._lead = ref}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={dataSource}
                stickyHeaderIndices={[0]}
                ListHeaderComponent={this.headerComponent}
                ListEmptyComponent={this.emptyComponent}
                // ListFooterComponent={this.footerComponent}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString() }
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={() => this.onRefresh()}
                    />
                }
                onEndReachedThreshold={0.5}
                onEndReached={() => this.loadMoreData()}
                getItemLayout={(data, index) => (
                    {length: 100, offset: 100 * index, index}
                )}
            />
            <Filter title={"Filter"} onCallback={ val => {
                this.setState(prevState => {
                    return {
                        dataSource: [],
                        isNull: false,
                        page: 1,
                        leadstatus: val.leadstatus,
                        startdate: val.startdate,
                        enddate: val.enddate
                    }
                }, () => this.props.getLeads({
                        MemberCode: this.state.member.MemberCode,
                        page: this.state.page,
                        leadstatus: this.state.leadstatus,
                        startdate: this.state.startdate,
                        enddate: this.state.enddate
                    })  
                )
            }} />
        </View>
        );
    }
}

const mapStateToProps = ({ leadsReducer, checkTokenReducer }) => {
    const { check } = checkTokenReducer;
    const { loading, dialog, leads } = leadsReducer;
    return { check, loading, dialog, leads }
};

export default connect(mapStateToProps, {
    openDialogFilter,
    closeDialogFilter,
    getLeads,
    getCheckToken
})(Leads);