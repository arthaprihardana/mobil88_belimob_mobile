/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-13 14:40:00 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-17 16:31:07
 */
import React, {Component} from 'react';
import {ScrollView, View, Keyboard, TouchableOpacity, ActivityIndicator} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import _ from 'lodash';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader, Dialog, TextInput, Select, Button, DatePicker, TimePicker} from '../component';
import {WHITE, GREY_400} from '../../libraries/colors';
import globalStyles from '../../assets/styles';
import {getProvince, openProvince, closeProvince, getCity, openCity, closeCity, leadsYesAppointment, openDialogResponse, closeDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {};

class JadwalPertemuan extends Component<Props, state> {
    
    state = {
        masterProvince: [],
        masterCity: [],
        masterDistrict:  [],

        tanggal: '',
        jam: '',

        minimumpricemrp: '',
        maximumpricemrp: '',
        pricemrp: '',
        leadappointmentdate: '',
        leadappointmentaddress: '',
        leadappointmentprovince: '',
        leadappointmentcity: '',
        leadappointmentbuilding: '',
        leadappointmentremark: '',
        leadreferencecode: '',
        leadid: '',

        message: '',
        readyToSubmit: false
    }

    componentDidMount() {
        this.props.getProvince();
        this.setState({
            minimumpricemrp: this.props.minimumpricemrp,
            maximumpricemrp: this.props.maximumpricemrp,
            pricemrp: this.props.pricemrp,
            leadreferencecode: this.props.leadreferencecode,
            leadid: this.props.leadid
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.province !== prevProps.province) {
            let data = this.props.province.data;
            this.setState({ masterProvince: data });
        }
        if(this.props.city !== prevProps.city) {
            let data = this.props.city.data;
            this.setState({ masterCity: data });
        }
        if(this.props.district !== prevProps.district) {
            let data = this.props.district.data;
            this.setState({ masterDistrict: data })
        }
        if(this.props.yesAppointment !== prevProps.yesAppointment) {
            if(this.props.yesAppointment.status) {
                this.setState({ message: this.props.yesAppointment.message })
                this.props.openDialogResponse()
            } else {
                alert('Oops Something went wrong');
            }
        }
    }

    omitField = async (currentField) => {
        let except = ['masterProvince', 'masterCity', 'masterDistrict', 'minimumpricemrp', 'maximumpricemrp', 'pricemrp', 'leadappointmentdate', 'leadappointmentbuilding', 'leadappointmentremark', 'leadreferencecode', 'leadid', 'message', 'readyToSubmit'];
        except.push(currentField);
        let o = _.omit(this.state, except);
        let a = await _.findKey(o, val => {
            if(_.isEmpty(val)) { return true }
        });
        return a;
    }

    render() {
        const { masterProvince, masterCity, masterDistrict } = this.state;
        return (
            <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={globalStyles.boxContainer}>
                    <View style={[globalStyles.boxContent, { paddingTop: 20, paddingLeft: 20, paddingRight: 20 }]}>
                        
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: '55%'}}>
                                <DatePicker 
                                    label="Tanggal" 
                                    value={this.state.tanggal} 
                                    callback={data => {
                                        this.omitField('tanggal').then(val => {
                                            this.setState({
                                                tanggal: moment(data.date).format('YYYY-MM-DD'),
                                                readyToSubmit: val !== undefined ? false : true
                                            })
                                        });
                                    }} 
                                    errorMessage={"*) Wajib diisi"} 
                                    />
                            </View>
                            <View style={{ width: '40%'}}>
                                <TimePicker 
                                    label="Jam" 
                                    value={this.state.jam} 
                                    callback={data => {
                                        this.omitField('jam').then(val => {
                                            this.setState({
                                                jam: data.time,
                                                readyToSubmit: val !== undefined ? false : true
                                            })
                                        });
                                    }} 
                                    errorMessage={"*) Wajib diisi"} 
                                    />
                            </View>
                        </View>
                        
                        <View style={{ width: '100%' }}>
                            <TextInput 
                                refs={ref => this._leadappointmentaddress = ref}
                                label="Alamat"
                                value={this.state.leadappointmentaddress}
                                onChangeText={text => {
                                    this.omitField('leadappointmentaddress').then(val => {
                                        this.setState({
                                            leadappointmentaddress: text,
                                            readyToSubmit: val === undefined && text !== "" ? true : false
                                        })
                                    });
                                }}
                                errorMessage={"*) Wajib diisi"}
                                />
                        </View>

                        <Select 
                            label="Provinsi"
                            onPress={() => this.props.openProvince()}
                            value={this.state.leadappointmentprovince || "Pilih Provinsi"}
                            loading={this.props.loadingProvince}
                            errorMessage={"*) Wajib diisi"}
                            />
                    
                        <Select 
                            label="Kota"
                            onPress={() => this.props.openCity()}
                            value={this.state.leadappointmentcity || "Pilih Kota"}
                            loading={this.props.loadingCity}
                            errorMessage={"*) Wajib diisi"}
                            />
                        
                        <View style={{ width: '100%' }}>
                            <TextInput 
                                refs={ref => this._leadappointmentbuilding = ref}
                                label="Gedung"
                                value={this.state.leadappointmentbuilding}
                                onChangeText={text => this.setState({ leadappointmentbuilding: text })}
                                />
                        </View>
                    
                        <View style={{ width: '100%' }}>
                            <TextInput 
                                refs={ref => this._leadappointmentremark = ref}
                                label="Keterangan"
                                value={this.state.leadappointmentremark}
                                onChangeText={text => this.setState({ leadappointmentremark: text })}
                                />
                        </View>
                    </View>
                    <Button style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => { 
                        Keyboard.dismiss(); 
                        if(this.state.readyToSubmit && this.props.loading !== true) {
                            var minimumpricemrp = this.state.minimumpricemrp !== 0 ? this.state.minimumpricemrp.split(".") : 0;
                            var maximumpricemrp = this.state.maximumpricemrp !== 0 ? this.state.maximumpricemrp.split(".") : 0;
                            var pricemrp = this.state.pricemrp !== 0 ? this.state.pricemrp.split(".") : 0;
                            this.props.leadsYesAppointment({
                                minimumpricemrp: this.state.minimumpricemrp !== 0 ? parseInt(minimumpricemrp.join('')) : minimumpricemrp,
                                maximumpricemrp: this.state.maximumpricemrp !== 0 ? parseInt(maximumpricemrp.join('')) : maximumpricemrp,
                                pricemrp: this.state.pricemrp !== 0 ? parseInt(pricemrp.join('')) : pricemrp,
                                leadappointmentdate: moment(`${this.state.tanggal} ${this.state.jam}`).format('YYYY-MM-DD HH:mm:ss'),
                                leadappointmentaddress: this.state.leadappointmentaddress,
                                leadappointmentprovince: this.state.leadappointmentprovince,
                                leadappointmentcity: this.state.leadappointmentcity,
                                leadappointmentdistrict: this.state.leadappointmentdistrict,
                                leadappointmentbuilding: this.state.leadappointmentbuilding,
                                leadappointmentremark: this.state.leadappointmentremark,
                                leadreferencecode: this.state.leadreferencecode,
                                leadid: this.state.leadid
                            })
                        }
                    }}>
                        {this.props.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Simpan</Text> }
                    </Button>
                </View>
                {/* Dialog Provinsi */}
                <Dialog 
                    title={"Province"}
                    onRequestClose={() => this.props.closeProvince()}
                    visible={this.props.dialogProvince}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search Province" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterProvince: _.filter(this.props.province.data, obj => new RegExp(text, 'i').test(obj.ProvinceName) )
                                }
                            })
                        }} />
                    </View>
                    { masterProvince && masterProvince.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeProvince();
                            this.props.getCity(value.ProvinceId);
                            this.omitField('leadappointmentprovince').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        masterProvince: this.props.province.data,
                                        leadappointmentprovince: value.ProvinceName,
                                        readyToSubmit: val === undefined ? true : false
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.ProvinceName}</Text>
                        </TouchableOpacity>
                    ))}
                </Dialog>
                {/* Dialog City */}
                <Dialog 
                    title={"City"}
                    onRequestClose={() => this.props.closeCity()}
                    visible={this.props.dialogCity}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search City" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterCity: _.filter(this.props.city.data, obj => new RegExp(text, 'i').test(obj.CityName) )
                                }
                            })
                        }} />
                    </View>
                    { masterCity && masterCity.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCity()
                            this.omitField('leadappointmentcity').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        masterCity: this.props.city.data,
                                        leadappointmentcity: value.CityName,
                                        readyToSubmit: val === undefined ? true : false
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CityName}</Text>
                        </TouchableOpacity>
                    )) }
                </Dialog>
                {/* Dialog Response */}
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.reset('main');
                }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.state.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.reset('main');
                        }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Next</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </ScrollView>
        );
    }

}

const mapStateToProps = ({ provinceReducer, cityReducer, leadsReducer }) => {
    const { province } = provinceReducer;
    const loadingProvince = provinceReducer.loading;
    const dialogProvince = provinceReducer.dialog;
    const { city } = cityReducer;
    const loadingCity = cityReducer.loading;
    const dialogCity = cityReducer.dialog;
    const { yesAppointment, loading } = leadsReducer;
    return { province, loadingProvince, dialogProvince, city, loadingCity, dialogCity, yesAppointment, loading };
}

export default connect(mapStateToProps, {
    getProvince,
    openProvince,
    closeProvince,

    getCity,
    openCity,
    closeCity,

    leadsYesAppointment,
    openDialogResponse,
    closeDialogResponse
})(JadwalPertemuan);