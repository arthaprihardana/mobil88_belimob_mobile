/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-19 13:26:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-12 10:39:32
 */
import React, {Component} from 'react';
import {ScrollView, View, Keyboard, TouchableOpacity, ActivityIndicator, Linking, Alert, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import _ from 'lodash';
import Placeholder from 'rn-placeholder';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader, Button } from '../component';
import styles from './styles';
import globalStyles from '../../assets/styles';
import {getLeadsDetail, getMember} from '../../actions';
import { WHITE } from '../../libraries/colors';

type Props = {};
type State = {
    showInformasiMobil: Boolean,
    showJadwalPertemuan: Boolean,
    showProfileSales: Boolean,
    detailLeads: Object
};

const Loading = () => (
    <View>
        <View style={{ backgroundColor: WHITE, paddingTop: 20, paddingLeft: 16, paddingRight: 16, paddingBottom: 20, borderRadius: 5, elevation: 1, marginBottom: 15 }}>
            <View style={{ width: '100%', marginBottom: 15 }}>
                <Placeholder.MultiWords
                    words={[
                        { color: '#eee', width: '50%' },
                        { color: '#eee', width: '50%' },
                    ]}
                    textSize={16}
                    lineSpacing={10}
                    animate="fade"
                    />
            </View>
            <View style={{ width: '100%', marginBottom: 15 }}>
                <Placeholder.MultiWords
                    words={[
                        { color: '#eee', width: '50%' },
                        { color: '#eee', width: '50%' },
                    ]}
                    textSize={16}
                    lineSpacing={10}
                    animate="fade"
                    />
            </View>
            <View style={{ width: '100%', marginBottom: 15 }}>
                <Placeholder.MultiWords
                    words={[
                        { color: '#eee', width: '50%' },
                        { color: '#eee', width: '50%' },
                    ]}
                    textSize={16}
                    lineSpacing={10}
                    animate="fade"
                    />
            </View>
            <View style={{ width: '100%' }}>
                <Placeholder.MultiWords
                    words={[
                        { color: '#eee', width: '50%' },
                        { color: '#eee', width: '50%' },
                    ]}
                    textSize={16}
                    lineSpacing={10}
                    animate="fade"
                    />
            </View>
        </View>

        <View style={[styles.boxContent, { alignItems: 'flex-start' }]}>
            <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                <Placeholder.Line width="77%" textSize={16} animate="fade" />
            </View>
            <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                <Placeholder.Line width="77%" textSize={16} animate="fade" />
            </View>
            <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                <Placeholder.Line width="77%" textSize={16} animate="fade" />
            </View>
        </View>
    </View>
)

class DetailLeads extends Component<Props, State> {

    state = {
        showInformasiMobil: false,
        showJadwalPertemuan: false,
        showProfileSales: false,
        detailLeads: {},
        nama_member: null,
        email_member: null,
        phone_member: null,
        memberid: null
    }

    componentDidMount() {
        this.props.getLeadsDetail(this.props.RefCode);
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.props.getMember(member.MemberCode);
            }
        })
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.detailLeads !== prevProps.detailLeads) {
            return { detailLeads: this.props.detailLeads }
        }
        if(this.props.member !== prevProps.member) {
            return { member: this.props.member }
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.detailLeads !== undefined) {
                this.setState(prevState => {
                    return { detailLeads: snapshot.detailLeads.data }
                });
            } 
            if(snapshot.member !== undefined) {
                let member = snapshot.member.data;
                this.setState(prevState => {
                    AsyncStorage.setItem('@global:detailMember', JSON.stringify(member));
                    return {
                        nama_member: member.MemberName,
                        email_member: member.MemberMail,
                        phone_member: member.MemberPhoneNumber,
                        memberid: member.MemberId
                    }
                })
            }
        }
    }

    render() {
        const { loading } = this.props;
        const { showInformasiMobil, showJadwalPertemuan, showProfileSales, detailLeads } = this.state;
        return (
            <ScrollView style={{ flex: 1 }}>
                <BlockHeader />
                <View style={styles.container}>
                    { loading ? <Loading /> : 
                    <View style={[styles.boxContent, { paddingTop: 20, alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16, paddingBottom: 20 }]}>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={styles.title}>NO REF</Text>
                            <Text style={styles.title}>{detailLeads.LeadReferenceCode}</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text>Tanggal Submit</Text>
                            <Text>{moment(detailLeads.CreatedDate).format('ll')}</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text>Status</Text>
                            <Text>{_.startCase(detailLeads.LeadStatusExt)}</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text>Apraiser</Text>
                            <Text>{detailLeads.DmsAppraiserName || "-"}</Text>
                        </View>
                    </View> }
                    { !loading && 
                    <View style={[styles.boxContent, { alignItems: 'flex-start' }]}>
                        <TouchableOpacity onPress={() => this.setState({ showInformasiMobil: !showInformasiMobil })} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                            <Text>Informasi Mobil</Text>
                            <FontAwesome5 name="angle-down" size={16} />
                        </TouchableOpacity>
                        {showInformasiMobil && 
                            <View style={{ width: '100%', flexDirection: 'column', borderBottomWidth: 1, borderBottomColor: '#eee', paddingTop: 20, paddingBottom: 20, backgroundColor: '#fcfcfc' }}>
                                <Text style={[styles.title, { paddingLeft: 16, paddingRight: 16, paddingBottom: 20 }]}>{detailLeads.LeadCarBrand} {detailLeads.LeadCarModel} {detailLeads.LeadCarVariant}</Text>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#eee', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-calendar" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{detailLeads.LeadCarYear}</Text>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-speedometer" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{detailLeads.LeadCarKm} KM</Text>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#eee', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-color-palette" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{detailLeads.LeadCarColour}</Text>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-settings" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{detailLeads.LeadCarTransmission}</Text>
                                    </View>
                                    <View style={{ width: '100%', height: 50, flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#eee', borderTopWidth: 1, borderTopColor: '#eee', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-card" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{detailLeads.LeadCarPrefixPlate} {detailLeads.LeadCarPlateNumber} {detailLeads.LeadCarSuffixPlate || ""}</Text>
                                    </View>
                                    {/* <View style={{ width: '50%', height: 50, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#eee', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        
                                    </View> */}
                                </View>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 20 }}>
                                    <Text small>Tanggal Pajak</Text>
                                    <Text small>{moment(detailLeads.LeadStnkTaxExpired).format('LL')}</Text>
                                </View>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 20 }}>
                                    <Text small>Tipe STNK</Text>
                                    <Text small>{detailLeads.LeadStnkType}</Text>
                                </View>
                            </View>}
                        <TouchableOpacity onPress={() => this.setState({ showJadwalPertemuan: !showJadwalPertemuan })} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                            <Text>Jadwal Pertemuan</Text>
                            <FontAwesome5 name="angle-down" size={16} />
                        </TouchableOpacity>
                        {showJadwalPertemuan && 
                            <View style={{ width: '100%', flexDirection: 'column', paddingTop: 20, backgroundColor: '#fcfcfc' }}>
                                <TouchableOpacity onPress={() => {
                                    let url = `https://www.google.com/maps/search/?api=1&query=${detailLeads.LeadAppointmentAddress},${detailLeads.LeadAppointmentCity},${detailLeads.LeadAppointmentProvince}`
                                    Linking.canOpenURL(url).then(supported => {
                                        if (!supported) {
                                            console.log('Can\'t handle url: ' + url);
                                        } else {
                                            return Linking.openURL(url);
                                        }
                                        }).catch(err => console.error('An error occurred', err));
                                    }} >
                                    <Text style={[styles.title, { paddingLeft: 16, paddingRight: 16, paddingBottom: 20 }]}>{detailLeads.LeadAppointmentAddress}, {detailLeads.LeadAppointmentCity}, {detailLeads.LeadAppointmentProvince}</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#eee', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-map" size={18} style={{ marginRight: 16 }} />
                                        <View style={{ width: '80%' }}>
                                            <Text style={{ fontSize: 10, lineHeight: 12 }}>{detailLeads.LeadAppointmentAddress}</Text>
                                        </View>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <FontAwesome5 name="building" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{detailLeads.LeadAppointmentBuilding || "-"}</Text>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#eee', borderTopWidth: 1, borderTopColor: '#eee', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-calendar" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{moment(detailLeads.LeadAppointmentDate).format('ll')}</Text>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#eee', borderBottomWidth: 1, borderBottomColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-time" size={18} style={{ marginRight: 16 }} />
                                        <Text small>{moment(detailLeads.LeadAppointmentDate).format('HH:mm')}</Text>
                                    </View>
                                </View>
                            </View> }
                        <TouchableOpacity onPress={() => this.setState({ showProfileSales: !showProfileSales })} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 20, paddingBottom: 20 }}>
                            <Text>Profil Sales</Text>
                            <FontAwesome5 name="angle-down" size={16} />
                        </TouchableOpacity>
                        { showProfileSales && 
                            <View style={{ width: '100%', flexDirection: 'column', borderTopWidth: 1, borderTopColor: '#eee', paddingTop: 20, backgroundColor: '#fcfcfc' }}>
                                <Text style={[styles.title, { paddingLeft: 16, paddingRight: 16, paddingBottom: 20 }]}>{this.state.nama_member}</Text>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderRightWidth: 1, borderRightColor: '#eee', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-mail" size={18} />
                                        <Text small style={{ paddingLeft: 16, paddingRight: 16 }}>{this.state.email_member}</Text>
                                    </View>
                                    <View style={{ width: '50%', height: 50, flexDirection: 'row', borderTopWidth: 1, borderTopColor: '#eee', paddingLeft: 16, paddingRight: 16, alignItems: 'center' }}>
                                        <Ionicons name="md-call" size={18} />
                                        <Text small style={{ paddingLeft: 16, paddingRight: 16 }}>{this.state.phone_member}</Text>
                                    </View>
                                </View>
                            </View> }
                    </View> }
                    { detailLeads.LeadStatusExt !== undefined && /(new|assign)/i.test(detailLeads.LeadStatusExt) === true && 
                    <Button style={globalStyles.buttonPrimary} onPress={() => {
                        Alert.alert(
                            'Cancel Leads',
                            'Apakah anda yakin akan membatalkan lead ini?',
                            [
                                {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {text: 'Ya', onPress: () => Actions.push('cancelLeads', {title: 'Cancel Leads', leadid: this.state.detailLeads.LeadId, leadreferencecode: this.state.detailLeads.LeadReferenceCode, dmsreferencecode: this.state.detailLeads.DmsReferenceCode })},
                            ],
                            { cancelable: false }
                        )
                        }}>
                        <Text style={{ color: WHITE, letterSpacing: 2 }}>Cancel Leads</Text>
                    </Button> }
                </View>
            </ScrollView>
        );
    }

}

const mapStateToProps = ({ leadsReducer, memberReducer }) => {
    const { member, loading } = memberReducer;
    const { detailLeads } = leadsReducer;
    return {detailLeads, member, loading}
};

export default connect(mapStateToProps, {
    getLeadsDetail,
    getMember
})(DetailLeads);