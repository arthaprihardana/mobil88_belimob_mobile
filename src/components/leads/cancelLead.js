/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-05 14:25:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-12 10:40:25
 */
import React, {Component} from 'react';
import {ScrollView, View, Keyboard, TouchableOpacity, AsyncStorage, ActivityIndicator, Alert} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { BlockHeader, Select, TextInput, Button, Text, Dialog } from '../component';
import styles from './styles';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_400} from '../../libraries/colors';
import {openDialogLeadsReason, closeDialogLeadsReason, getLeadsReason, cancelLeads, closeDialogResponse, openDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    leadstatusname: String,
    leadreason: String,
    leadreasonother: String,
    leadreferencecode: String,
    leadid: String,
    memberid: String,
    isOther: Boolean,
    readyToSubmit: Boolean
};

class CancelLeads extends Component<Props, State> {

    state = {
        masterLeadsReason: [],
        leadstatusname: 'cancelled',
        leadreason: '',
        leadreasonother: '',
        leadreferencecode: '',
        dmsreferencecode: '',
        leadid: '',
        memberid: '',
        isOther: false,
        readyToSubmit: false
    }

    componentDidMount() {
        this.props.getLeadsReason();
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.setState({
                    memberid: member.MemberId,
                    leadid: this.props.leadid,
                    leadreferencecode: this.props.leadreferencecode,
                    dmsreferencecode: this.props.dmsreferencecode
                })
            }
        })
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.reason !== prevProps.reason) {
            return {masterLeadsReason: this.props.reason};
        }
        if(this.props.cancel !== prevProps.cancel) {
            return {cancel: this.props.cancel};
        }
        if(this.state.leadreason !== prevState.leadreason) {
            let reg = /Lainnya/i
            if(reg.test(this.state.leadreason)) {
                return { isOther: true }
            } else {
                return { isOther: false }
            }
            
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.masterLeadsReason !== undefined) {
                if(snapshot.masterLeadsReason.status) {
                    this.setState(prevState => {
                        return { masterLeadsReason: snapshot.masterLeadsReason.data }
                    })
                }
            }
            if(snapshot.cancel !== undefined) {
                if(snapshot.cancel.status) {
                    this.props.openDialogResponse();
                } else {
                    Alert.alert(
                        'Info',
                        snapshot.cancel.message,
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                }
            }
            if(snapshot.isOther !== undefined) {
                this.setState(prevState => {
                    return { isOther: snapshot.isOther }
                })
            }
        }
    }
    
    render() {
        const { masterLeadsReason, isOther, readyToSubmit } = this.state;
        return (
            <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={styles.container}>
                    <View style={[styles.boxContent, { paddingTop: 20, paddingLeft: 20, paddingRight: 20 }]}>
                        <View style={{ width: '100%' }}>
                            <Select 
                                label="Alasan Membatalkan Lead"
                                onPress={() => this.props.openDialogLeadsReason()}
                                value={this.state.leadreason || "Pilih Alasan"}
                                loading={this.props.loadingLeadsReason}
                                />
                        </View>
                        {isOther && 
                        <View style={{ width: '100%' }}>
                            <TextInput 
                                label="Alasan Lainnya"
                                value={this.state.leadreasonother}
                                onChangeText={text => this.setState({leadreasonother: text})}
                                keyboardType="default"
                                />
                        </View> }
                    </View>
                    <Button style={readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        Keyboard.dismiss();
                        if(readyToSubmit && this.props.loading !== true) {
                            this.props.cancelLeads({
                                leadstatusname: this.state.leadstatusname,
                                leadreason: this.state.leadreason,
                                leadreasonother: this.state.leadreasonother,
                                leadreferencecode: this.state.leadreferencecode,
                                dmsreferencecode: this.state.dmsreferencecode,
                                leadid: this.state.leadid,
                                memberid: this.state.memberid
                            })
                        }
                    }}>
                        {this.props.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Simpan</Text> }
                    </Button>
                </View>
                <Dialog 
                    title={"Cancel Reason"}
                    onRequestClose={() => this.props.closeDialogLeadsReason()}
                    visible={this.props.dialogLeadsReason}>
                    { masterLeadsReason && masterLeadsReason.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeDialogLeadsReason();
                            this.setState(prevState => {
                                return {
                                    leadreason: value.LeadReasonName,
                                    readyToSubmit: true
                                }
                            })
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.LeadReasonName}</Text>
                        </TouchableOpacity>
                    )) }
                </Dialog>
                <DialogResponse onClose={() => { 
                    this.props.closeDialogResponse();
                    Actions.reset('main')
                }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>Cancel Leads has been submited</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.reset('main')
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Go To Main</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </ScrollView>
        );
    }

}

const mapStateToProps = ({ leadsReasonReducer, leadsReducer }) => {
    const { dialog, reason } = leadsReasonReducer;
    const { loading, cancel } = leadsReducer;
    const loadingLeadsReason = leadsReasonReducer.loading;
    const dialogLeadsReason = leadsReasonReducer.dialog;
    return { loadingLeadsReason, dialog, reason, loading, cancel, dialogLeadsReason }
};

export default connect(mapStateToProps, {
    openDialogLeadsReason,
    closeDialogLeadsReason,
    getLeadsReason,
    cancelLeads,
    closeDialogResponse, 
    openDialogResponse
})(CancelLeads);