/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-12 10:38:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-06 15:25:50
 */
import React, {Component} from 'react';
import {ScrollView, View, Keyboard, TouchableOpacity, AsyncStorage, ActivityIndicator, Alert} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import _ from 'lodash';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import numeral from 'numeral';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader, TextInput, Select, Button, DatePicker, Dialog} from '../component';
import styles from './styles';
import {WHITE, GREY_400} from '../../libraries/colors';
import globalStyles from '../../assets/styles';
import {getCarBrand, openCarBrand, closeCarBrand, getCarModel, openCarModel, closeCarModel, getCarVariant, openCarVariant, closeCarVariant, getCarTransmission, openCarTransmission, closeCarTransmission, getCarColor, openCarColor, closeCarColor, getCarPrefixPlate, openCarPrefixPlate, closeCarPrefixPlate, openDialogTahunPabrikan, closeDialogTahunPabrikan, openDialogFacelift, closeDialogFacelift, openDialogTipeStnk, closeDialogTipeStnk, addLeads, getRegion, openDialogRegion, getRegionFailure, getRegionSuccess, closeDialogRegion, closeDialogResponse, openDialogResponse} from '../../actions';
import DialogResponse from '../master/response';

type Props = {};
type State = {
    leadcarbrand: String,
    leadcarmodel: String,
    leadcarvariant: String,
    leadcarnoteother: String,
    leadcaryear: String,
    leadcartransmission: Boolean,
    leadcarkm: String,
    leadcarprefixplate: String,
    leadcarplatenumber: String,
    leadcarcolour: String,
    leadcarcolourother: String,
    leadstnktaxexpired: String,
    leadstnktype: String,
    // leadfacelift: String,
    regionid: String,
    regionname: String,
    membercode: String,
    memberid: String
};

function split(input, len) {
    return input.match(new RegExp('.{1,' + len + '}(?=(.{' + len + '})+(?!.))|.{1,' + len + '}$', 'g'))
}

class FormLeads extends Component<Props, State> {

    state = {
        masterCarBrand: [],
        carBrandId: null,
        masterCarModel: [],
        CarTypeId: null,
        masterCarVariant: [],
        CarVariantId: null,
        masterCarTransmission: [],
        masterColor: [],
        masterPrefixPlate: [],
        masterRegion: [],

        leadcarbrand: '',
        leadcarmodel: '',
        leadcarvariant: '',
        leadcarnoteother: '',
        leadcaryear: '',
        leadcartransmissionid: '',
        leadcartransmission: '',
        leadcarkm: '',
        leadcarprefixplate: '',
        leadcarprefixplateid: '',
        leadcarplatenumber: '',
        leadcarsuffixplate: '',
        leadcarcolourid: '',
        leadcarcolour: '',
        leadcarcolourother: '',
        leadstnktaxexpired: '',
        leadstnktype: '',
        // leadfacelift: '',
        regionid: '',
        // regionname: '',
        membercode: '',
        memberid: '',

        leadreferencecode: '',
        leadid: '',
        kmFormater: '',
        params: {},
        readyToSubmit: false
    }

    componentDidMount() {
        this.props.getCarBrand();
        this.props.getCarTransmission();
        this.props.getCarColor();
        this.props.getCarPrefixPlate();
        // this.props.getRegion();
        AsyncStorage.getItem('@global:member', (error, result) => {
            if(result !== null) {
                let member = JSON.parse(result);
                this.setState({
                    membercode: member.MemberCode,
                    memberid: member.MemberId
                })
            }
        })
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.carBrandReducer.carBrand !== prevProps.carBrandReducer.carBrand) {
            let data = this.props.carBrandReducer.carBrand.data;
            return { masterCarBrand: data }
        }
        if(this.state.carBrandId !== prevState.carBrandId) {
            this.props.getCarModel(this.state.carBrandId);
            return null;
        }
        if(this.props.carModelReducer.carModel !== prevProps.carModelReducer.carModel) {
            let data = this.props.carModelReducer.carModel.data;
            return { masterCarModel: data }
        }
        if(this.state.CarTypeId !== prevState.CarTypeId) {
            this.props.getCarVariant(this.state.CarTypeId);
            return null;
        }
        if(this.props.carVariantReducer.carVariant !== prevProps.carVariantReducer.carVariant) {
            let data = this.props.carVariantReducer.carVariant.data;
            return { masterCarVariant: data };
        }
        if(this.props.carTransmissionReducer.transmission !== prevProps.carTransmissionReducer.transmission) {
            let data = this.props.carTransmissionReducer.transmission.data;
            return { masterCarTransmission: data };
        }
        if(this.props.carColorReducer.color !== prevProps.carColorReducer.color) {
            let data = this.props.carColorReducer.color.data;
            return { masterColor: data }
        }
        if(this.props.carPrefixPlateReducer.prefixPlate !== prevProps.carPrefixPlateReducer.prefixPlate) {
            let data = this.props.carPrefixPlateReducer.prefixPlate.data;
            return { masterPrefixPlate: data };
        }
        // if(this.props.regionReducer.region !== prevProps.regionReducer.region) {
        //     let data = this.props.regionReducer.region.data;
        //     return { masterRegion: data }
        // }
        if(this.props.newLeads !== prevProps.newLeads) {
            return { newLeads: this.props.newLeads };
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        
        if(snapshot !== null) {
            if(snapshot.masterCarBrand !== undefined) {
                this.setState({ masterCarBrand: snapshot.masterCarBrand });
            }
            if(snapshot.masterCarModel !== undefined) {
                this.setState({ masterCarModel: snapshot.masterCarModel });
            }
            if(snapshot.masterCarVariant !== undefined) {
                this.setState({ masterCarVariant: snapshot.masterCarVariant });
            }
            if(snapshot.masterCarTransmission !== undefined) {
                this.setState({ masterCarTransmission: snapshot.masterCarTransmission });
            }
            if(snapshot.masterColor !== undefined) {
                this.setState({ masterColor: snapshot.masterColor });
            }
            if(snapshot.masterPrefixPlate !== undefined) {
                this.setState({ masterPrefixPlate: snapshot.masterPrefixPlate });
            }
            // if(snapshot.masterRegion !== undefined) {
            //     this.setState({ masterRegion: snapshot.masterRegion });
            // }
            if(snapshot.newLeads !== undefined) {
                if(snapshot.newLeads.status) {
                    this.setState({ params: snapshot.newLeads.data })
                    this.props.openDialogResponse();
                } else {
                    Alert.alert(
                        'Info',
                        snapshot.newLeads.message || snapshot.newLeads.errorMsg,
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                }
            }
        }
    }

    omitField = async (currentField) => {
        let except = ['carBrandId', 'masterCarBrand', 'CarTypeId', 'masterCarModel', 'CarVariantId', 'masterCarVariant', 'masterCarTransmission', 'masterColor', 'masterPrefixPlate', 'masterRegion', 'leadreferencecode', 'leadid', 'kmFormater', 'params', 'readyToSubmit', 'leadcarnoteother', 'leadcarsuffixplate', 'leadcarcolourother', 'regionid', 'memberid', 'membercode'];
        except.push(currentField);
        let o = _.omit(this.state, except);
        let a = await _.findKey(o, val => {
            if(_.isEmpty(val)) { return true }
        });
        return a;
    }

    onSubmit = () => {
        this.props.addLeads({ 
            leadcarbrandid: this.state.carBrandId, 
            leadcarbrand: this.state.leadcarbrand,
            leadcarmodelid: this.state.CarTypeId,
            leadcarmodel: this.state.leadcarmodel,
            leadcarvariantid: this.state.CarVariantId,
            leadcarvariant: this.state.leadcarvariant,
            leadcarnoteother: this.state.leadcarnoteother,
            leadcaryear: this.state.leadcaryear,
            leadcartransmissionid: this.state.leadcartransmissionid,
            leadcartransmission: this.state.leadcartransmission,
            leadcarkm: parseInt(this.state.leadcarkm),
            leadcarprefixplateid: this.state.leadcarprefixplateid,
            leadcarprefixplate: this.state.leadcarprefixplate,
            leadcarplatenumber: this.state.leadcarplatenumber,
            leadcarsuffixplate: this.state.leadcarsuffixplate,
            leadcarcolourid: this.state.leadcarcolourid,
            leadcarcolour: this.state.leadcarcolour,
            leadcarcolourother: this.state.leadcarcolourother,
            leadstnktaxexpired: this.state.leadstnktaxexpired,
            leadstnktype: this.state.leadstnktype,
            // leadfacelift: this.state.leadfacelift,
            regionid: this.state.regionid,
            regionname: this.state.regionname,
            membercode: this.state.membercode,
            memberid: this.state.memberid
        })
    }
    
    render() {
        const { masterCarBrand, masterCarModel, masterCarVariant, masterCarTransmission, masterColor, masterPrefixPlate, masterRegion } = this.state;
        return (
            <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="always">
                <BlockHeader />
                <View style={styles.container}>
                    <View style={[styles.boxContent, { paddingTop: 20, paddingLeft: 20, paddingRight: 20 }]}>
                        <Select 
                            label="Merk Mobil"
                            onPress={() => this.props.openCarBrand() }
                            value={this.state.leadcarbrand || "Pilih Mobil"}
                            loading={this.props.carBrandReducer.loading}
                            errorMessage={"*) Wajib diisi"}
                            />
                        <Select 
                            label="Model"
                            onPress={() => this.props.openCarModel() }
                            value={this.state.leadcarmodel || "Pilih Model"}
                            loading={this.props.carModelReducer.loading}
                            errorMessage={"*) Wajib diisi"}
                            />
                        <Select 
                            label="Varian"
                            onPress={() => this.props.openCarVariant() }
                            value={this.state.leadcarvariant || "Pilih Varian"}
                            loading={this.props.carVariantReducer.loading}
                            errorMessage={"*) Wajib diisi"}
                            />
                        {/* <View style={{ paddingLeft: 20, paddingRight: 20, width: '100%' }}>
                            <TextInput 
                                refs={ref => this._leadcarnoteother = ref}
                                label="Merk-Model-Varian Lainnya"
                                value={this.state.leadcarnoteother}
                                onChangeText={text => this.setState({ leadcarnoteother: text })}
                                />
                        </View> */}
                        <View style={{ width: '100%', flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                            <View style={{ width: '100%' }}>
                                <Select 
                                    label="Tahun Pabrikan"
                                    onPress={() => this.props.openDialogTahunPabrikan() }
                                    value={this.state.leadcaryear || "Pilih Tahun"}
                                    errorMessage={"*) Wajib diisi"}
                                    />
                            </View>
                            {/* <View style={{ width: '50%' }}>
                                <Select 
                                    label="Facelift Mode"
                                    onPress={() => this.props.openDialogFacelift()}
                                    value={this.state.leadfacelift || "Pilih"}
                                    errorMessage={"*) Wajib diisi"}
                                    />
                            </View> */}
                            <View style={{ width: '45%'}}>
                                <TextInput 
                                    refs={ref => this._leadcarkm = ref}
                                    label="KM"
                                    value={ this.state.leadcarkm !== '' ? numeral(this.state.leadcarkm).format('0,0') : '' }
                                    onChangeText={text => {
                                        this.omitField('leadcarkm').then(val => {
                                            this.setState(prevState => {
                                                return { 
                                                    leadcarkm: text.split(',').join(''),
                                                    readyToSubmit: val !== undefined ? false : true
                                                }
                                            })
                                        })
                                    }}
                                    keyboardType="number-pad"
                                    errorMessage={"*) Wajib diisi"}
                                    />
                            </View>
                            <View style={{ width: '50%' }}>
                                <Select 
                                    label="Transmision"
                                    onPress={() => this.props.openCarTransmission()}
                                    value={this.state.leadcartransmission || "Pilih"}
                                    loading={this.props.carTransmissionReducer.loading}
                                    errorMessage={"*) Wajib diisi"}
                                    />
                            </View>
                            <View style={{ width: '100%' }}>
                                <Select 
                                    label="Warna"
                                    onPress={() => this.props.openCarColor() }
                                    value={this.state.leadcarcolour || "Pilih"}
                                    loading={this.props.carColorReducer.loading}
                                    errorMessage={"*) Wajib diisi"}
                                    />
                            </View>
                            <View style={{ width: '100%' }}>
                                <Text h6 style={{ marginBottom: 10 }}>Nomor Polisi</Text>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: "space-between" }}>
                                    <View style={{ width: '35%' }}>
                                        <Select 
                                            onPress={() => this.props.openCarPrefixPlate()}
                                            style={{ paddingLeft: 0, paddingRight: 0 }}
                                            value={this.state.leadcarprefixplate || ""}
                                            loading={this.props.carPrefixPlateReducer.loading}
                                            errorMessage={"*) Wajib diisi"}
                                            />
                                    </View>
                                    <View style={{ width: '30%' }}>
                                        <TextInput 
                                            refs={ref => this._leadcarplatenumber = ref}
                                            value={this.state.leadcarplatenumber}
                                            onChangeText={text => {
                                                this.omitField('leadcarplatenumber').then(val => {
                                                    this.setState(prevState => {
                                                        return {
                                                            leadcarplatenumber: text,
                                                            readyToSubmit: val !== undefined ? false : true
                                                        }
                                                    })
                                                })
                                            }}
                                            keyboardType="number-pad"
                                            maxLength={4}
                                            />
                                    </View>
                                    <View style={{ width: '30%' }}>
                                        <TextInput 
                                            refs={ref => this._leadcarsuffixplate = ref}
                                            value={this.state.leadcarsuffixplate}
                                            onChangeText={text => this.setState({ leadcarsuffixplate: text.toUpperCase() })}
                                            maxLength={3}
                                            autoCapitalize="characters"
                                            />
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: '100%' }}>
                            <DatePicker label="Tanggal Pajak" value={ this.state.leadstnktaxexpired && moment(this.state.leadstnktaxexpired).format('LL')} callback={data => {
                                this.omitField('leadstnktaxexpired').then(val => {
                                    this.setState({
                                        leadstnktaxexpired: moment(data.date).format('YYYY-MM-DD'),
                                        readyToSubmit: val !== undefined ? false : true
                                    })
                                });
                            }} 
                            errorMessage={"*) Wajib diisi"} />
                        </View>
                        <View style={{ width: '100%' }}>
                            <Select 
                                label="Tipe STNK"
                                onPress={() => this.props.openDialogTipeStnk() }
                                style={{ paddingLeft: 0, paddingRight: 0 }}
                                value={this.state.leadstnktype || "Pilih"}
                                errorMessage={"*) Wajib diisi"}
                                />
                        </View>
                    </View>
                    <Button style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={() => {
                        Keyboard.dismiss();
                        if(this.state.readyToSubmit && this.props.loading !== true ) {
                            this.onSubmit();
                        }
                    }}>
                        {this.props.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Lanjutkan</Text> }
                    </Button>
                </View>
                {/* Dialog Car Brand */}
                <Dialog 
                    title={"Car Brand"}
                    onRequestClose={() => this.props.closeCarBrand()}
                    visible={this.props.carBrandReducer.dialog}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search Car Brand" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterCarBrand: _.filter(this.props.carBrandReducer.carBrand.data, obj => new RegExp(text, 'i').test(obj.CarBrandName) )
                                }
                            })
                        }} />
                    </View>
                    { masterCarBrand.length > 0 ? masterCarBrand.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCarBrand();
                            this.omitField('namaLengkap').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        masterCarBrand: this.props.carBrandReducer.carBrand.data,
                                        carBrandId: value.CarBrandId,
                                        leadcarbrand: value.CarBrandName,
                                        CarTypeId: null,
                                        leadcarmodel: '',
                                        CarVariantId: null,
                                        leadcarvariant: '',
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            })
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CarBrandName}</Text>
                        </TouchableOpacity>
                    )) : <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <FontAwesome5 name="search" size={14} />
                        <Text small>Data Not Found!</Text>
                    </View> }
                </Dialog>
                {/* Dialog Car Model */}
                <Dialog
                    title={"Car Model"}
                    onRequestClose={() => this.props.closeCarModel()}
                    visible={this.props.carModelReducer.dialog}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search Car Model" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterCarModel: _.filter(this.props.carModelReducer.carModel.data, obj => new RegExp(text, 'i').test(obj.CarTypeName) )
                                }
                            })
                        }} />
                    </View>
                    { masterCarModel.length > 0 ? masterCarModel.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCarModel();
                            this.omitField('leadcarmodel').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        masterCarModel: this.props.carModelReducer.carModel.data,
                                        CarTypeId: value.CarTypeId,
                                        leadcarmodel: value.CarTypeName,
                                        CarVariantId: null,
                                        leadcarvariant: '',
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            })
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CarTypeName}</Text>
                        </TouchableOpacity>
                    )) : <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <FontAwesome5 name="search" size={14} />
                        <Text small>Data Not Found!</Text>
                    </View> }
                </Dialog>
                {/* Dialog Car Variant */}
                <Dialog 
                    title={"Car Variant"}
                    onRequestClose={() => this.props.closeCarVariant()}
                    visible={this.props.carVariantReducer.dialog}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search Car Variant" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterCarVariant: _.filter(this.props.carVariantReducer.carVariant.data, obj => new RegExp(text, 'i').test(obj.CarVariantName) )
                                }
                            })
                        }} />
                    </View>
                    { masterCarVariant ? masterCarVariant.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCarVariant();
                            this.omitField('leadcarvariant').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        masterCarVariant: this.props.carVariantReducer.carVariant.data,
                                        CarVariantId: value.CarVariantId,
                                        leadcarvariant: value.CarVariantName,
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            })
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CarVariantName}</Text>
                        </TouchableOpacity>
                    )) : <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <FontAwesome5 name="search" size={14} />
                        <Text small>Data Not Found!</Text>
                    </View>}
                </Dialog>
                {/* Dialog Transmission */}
                <Dialog 
                    title={"Transmission"}
                    onRequestClose={() => this.props.closeCarTransmission()}
                    visible={this.props.carTransmissionReducer.dialog}>
                    { masterCarTransmission ? masterCarTransmission.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCarTransmission();
                            this.omitField('leadcartransmission').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        leadcartransmissionid: value.CarTransmissionId,
                                        leadcartransmission: value.CarTransmissionName,
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            })
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CarTransmissionName}</Text>
                        </TouchableOpacity>
                    )) : <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <FontAwesome5 name="search" size={14} />
                        <Text small>Data Not Found!</Text>
                    </View> }
                </Dialog>
                {/* Dialog Color */}
                <Dialog 
                    title={"Color"}
                    onRequestClose={() => this.props.closeCarColor()}
                    visible={this.props.carColorReducer.dialog}>
                    <View style={{ backgroundColor: '#eee', paddingLeft: 16, paddingRight: 16, paddingTop: 10, paddingBottom: 10, maxHeight: 72 }}>
                        <TextInput customStyle={{ backgroundColor: '#fff' }} placeholder="Search Color" onChangeText={ text => {
                            this.setState(prevState => {
                                return {
                                    masterColor: _.filter(this.props.carColorReducer.color.data, obj => new RegExp(text, 'i').test(obj.CarColourName) )
                                }
                            })
                        }} />
                    </View>
                    { masterColor ? masterColor.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCarColor();
                            this.omitField('leadcarcolour').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        masterColor: this.props.carColorReducer.color.data,
                                        leadcarcolourid: value.CarColourId,
                                        leadcarcolour: value.CarColourName,
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CarColourName}</Text>
                        </TouchableOpacity>
                    )) : <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <FontAwesome5 name="search" size={14} />
                        <Text small>Data Not Found!</Text>
                    </View>}
                </Dialog>
                {/* Dialog Prefix Plate */}
                <Dialog 
                    title={"Prefix Plate"}
                    onRequestClose={() => this.props.closeCarPrefixPlate()}
                    visible={this.props.carPrefixPlateReducer.dialog}>
                    { masterPrefixPlate && masterPrefixPlate.map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeCarPrefixPlate();
                            this.omitField('leadcarprefixplate').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        regionid: value.RegionId,
                                        leadcarprefixplateid: value.CarPrefixPlateId,
                                        leadcarprefixplate: value.CarPrefixPlateName,
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.CarPrefixPlateName}</Text>
                        </TouchableOpacity>
                    )) }
                </Dialog>
                {/* Dialog Tahun Pabrikan */}
                <Dialog 
                    title={"Tahun Pabrikan"}
                    onRequestClose={() => this.props.closeDialogTahunPabrikan()}
                    visible={this.props.dialogThn}>
                    { Array.apply(0, Array(20)).map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeDialogTahunPabrikan();
                            this.omitField('leadcaryear').then(val => {
                                this.setState(prevState => {
                                    let year = moment().year() - key;
                                    return { 
                                        leadcaryear: year.toString(),
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{moment().year() - key}</Text>
                        </TouchableOpacity>
                    )) }
                </Dialog>
                {/* Dialog Facelift */}
                {/* <Dialog 
                    title={"Facelift"}
                    onRequestClose={() => this.props.closeDialogFacelift()}
                    visible={this.props.dialogFc}>
                    { ["Old", "New"].map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeDialogFacelift();
                            this.omitField('leadfacelift').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        leadfacelift: value,
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value.toUpperCase()}</Text>
                        </TouchableOpacity>
                    )) }
                </Dialog> */}
                {/* Dialog Tipe STNK */}
                <Dialog 
                    title={"Tipe STNK"}
                    onRequestClose={() => this.props.closeDialogTipeStnk()}
                    visible={this.props.dialogTs}>
                    { ["Perorangan", "Perusahaan"].map((value, key) => (
                        <TouchableOpacity onPress={() => {
                            this.props.closeDialogTipeStnk();
                            this.omitField('leadstnktype').then(val => {
                                this.setState(prevState => {
                                    return { 
                                        leadstnktype: value,
                                        readyToSubmit: val !== undefined ? false : true
                                    }
                                })
                            });
                        }} key={key} style={{ width: '100%', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 16, paddingRight: 16 }}>
                            <Text>{value}</Text>
                        </TouchableOpacity>
                    )) }
                </Dialog>
                {/* Dialog Response */}
                <DialogResponse onClose={() => {
                    this.props.closeDialogResponse();
                    Actions.push('estimasiHarga', {params: this.state.params})
                    }}>
                    <View style={{ width: '100%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, paddingRight: 16, paddingTop: 180 }}>
                        <Text h2>Congratulations</Text>
                        <Text>{this.props.newLeads !== null && this.props.newLeads.message}</Text>
                        <Button onPress={() => {
                            this.props.closeDialogResponse();
                            Actions.push('estimasiHarga', {params: this.state.params})
                            }} style={[globalStyles.buttonPrimary, { marginTop: 30 }]}>
                            <Text style={{color:WHITE}}>Next</Text>
                        </Button>
                    </View>
                </DialogResponse>
            </ScrollView>
        );
    }
}

const mapStateToProps = ({ carBrandReducer, carModelReducer, carVariantReducer, carTransmissionReducer, carColorReducer, carPrefixPlateReducer, regionReducer, leadsReducer }) => {
    const { loading, newLeads, dialogThn, dialogFc, dialogTs } = leadsReducer;
    return { carBrandReducer, carModelReducer, carVariantReducer, carTransmissionReducer, carColorReducer, carPrefixPlateReducer, regionReducer, loading, newLeads, dialogThn, dialogFc, dialogTs };
}

export default connect(mapStateToProps, {
    getCarBrand,
    openCarBrand,
    closeCarBrand,

    getCarModel,
    openCarModel,
    closeCarModel,

    getCarVariant,
    openCarVariant,
    closeCarVariant,

    getCarTransmission,
    openCarTransmission,
    closeCarTransmission,

    getCarColor,
    openCarColor,
    closeCarColor,

    getCarPrefixPlate,
    openCarPrefixPlate,
    closeCarPrefixPlate,

    openDialogTahunPabrikan,
    closeDialogTahunPabrikan,
    openDialogFacelift,
    closeDialogFacelift,
    openDialogTipeStnk,
    closeDialogTipeStnk,

    addLeads,

    getRegion,
    openDialogRegion,
    getRegionFailure,
    getRegionSuccess,
    closeDialogRegion,

    closeDialogResponse,
    openDialogResponse
})(FormLeads);