/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 10:08:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 15:12:56
 */
import React, { Component } from 'react';
import { Modal, View, TouchableOpacity, ScrollView, StyleSheet, CheckBox, DatePickerAndroid, AsyncStorage, Platform } from 'react-native';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/id';
import globalStyles from '../../assets/styles'
import { Text, Button, Dialog, Select } from '../component'
import { PRIMARY_COLOR, WHITE , GREY_600, GREY_400} from '../../libraries/colors';
import {closeDialogFilter, getLeadStatus, openDialogLeadStatus, closeDialogLeadStatus, getLeads} from '../../actions';

type State = {
    listLeadStatus: Array,
    leadstatus: String,
    startdate: Date,
    enddate: Date
};

class Filter extends Component<State> {

    state = {
        listLeadStatus: [],
        leadstatus: null,
        startdate: moment().subtract(1, 'weeks').format('YYYY-MM-DD'),
        enddate: moment().format('YYYY-MM-DD')
    }

    componentDidMount() {
        this.props.getLeadStatus();
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.leadStatus !== prevProps.leadStatus) {
            return this.props.leadStatus
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            this.setState(prevState => {
                return { 
                    listLeadStatus: snapshot.data,
                    leadstatus: snapshot.data !== undefined ? snapshot.data[0].LeadStatusName : ""
                }
            })
        }
    }

    getFilter = async () => {
        this.props.onCallback({
            leadstatus: this.state.leadstatus,
            startdate: this.state.startdate,
            enddate: this.state.enddate
        });
        this.props.closeDialogFilter();
    }

    openCalendar = async (type) => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                date: new Date(),
                maxDate: new Date()
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                if(type == "startdate") {
                    this.setState(prevState => {
                        return { startdate: moment({year: year, month: month, day: day}).format("YYYY-MM-DD") }
                    })
                } else if (type == "enddate") {
                    this.setState(prevState => {
                        return { enddate: moment({year: year, month: month, day: day}).format("YYYY-MM-DD") }
                    })
                }
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }

    render() {
        const { listLeadStatus } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.dialog}
                onRequestClose={() => this.props.closeDialogFilter() }>
                <View style={styles.container}>
                    <View style={styles.wrapper}>
                        <TouchableOpacity onPress={() => this.props.closeDialogFilter()} style={styles.closeButton}>
                            <Ionicons name="md-close" color={WHITE} size={20} />
                        </TouchableOpacity>
                        <View style={styles.navBarTitle}>
                            <Text bold style={{ color: WHITE }}>{this.props.title}</Text>
                        </View>
                    </View>
                    <ScrollView keyboardShouldPersistTaps="always" style={{ padding: 20, backgroundColor: '#fefefe' }}>
                        <View style={{ borderWidth: 1, borderColor: '#d6d8db', width: '100%', borderRadius: 4, backgroundColor: '#FFFFFF', padding: 16, marginBottom: 20 }}>
                            <Text style={{
                                color: PRIMARY_COLOR, 
                                fontSize: 18
                            }}>Periode</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <TouchableOpacity onPress={() => this.openCalendar("startdate")} style={{ width: '45%', flexDirection: 'row', alignItems: 'center' }}>
                                    <FontAwesome5 name="calendar-alt" size={14} color={GREY_600} style={{ marginRight: 10 }} />
                                    <Text>{moment(this.state.startdate).format('ll')}</Text>
                                </TouchableOpacity>
                                <View style={{ width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                                    <FontAwesome5 name="angle-double-right"  />
                                </View>
                                <TouchableOpacity onPress={() => this.openCalendar("enddate")} style={{ width: '45%', flexDirection: 'row', alignItems: 'center' }}>
                                    <FontAwesome5 name="calendar-alt" size={14} color={GREY_600} style={{ marginRight: 10 }} />
                                    <Text>{moment(this.state.enddate).format('ll')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ borderWidth: 1, borderColor: '#d6d8db', width: '100%', borderRadius: 4, backgroundColor: '#FFFFFF', padding: 16, marginBottom: 20 }}>
                            <Text style={{
                                color: PRIMARY_COLOR, 
                                fontSize: 18
                            }}>Status</Text>
                            {/* <Text style={{ marginBottom: 20 }}>Filter berdasarkan Status</Text> */}
                            <View style={{ width: '100%' }}>
                                <Select 
                                    label="Filter berdasarkan Status"
                                    onPress={() => this.props.openDialogLeadStatus() }
                                    value={_.startCase(this.state.leadstatus)}
                                    />
                            </View>
                            {/* {listLeadStatus && listLeadStatus.length > 0 && listLeadStatus.map((val, key) => (
                                <View key={key} style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <CheckBox 
                                        onValueChange={value => {}}
                                        style={{ marginBottom: 0, marginRight: 10 }}
                                        value={false}
                                    />
                                    <View style={{ 
                                        width: 18, 
                                        height: 18, 
                                        borderRadius: 9,
                                        marginRight: 10,
                                        backgroundColor: val.LeadStatusColour 
                                    }} />
                                    <Text>{_.startCase(val.LeadStatusName)}</Text>
                                </View>
                            ))} */}
                        </View>
                        <Button style={globalStyles.buttonPrimary} onPress={() => this.getFilter() }>
                            <Text style={{ color: WHITE, letterSpacing: 2 }}>Filter</Text>
                        </Button>
                    </ScrollView>
                </View>
                <Dialog
                    title={"Status Lead"}
                    onRequestClose={() => this.props.closeDialogLeadStatus()}
                    visible={this.props.dialogLs}>
                    {listLeadStatus && listLeadStatus.length > 0 && listLeadStatus.map((val, key) => (
                        <TouchableOpacity onPress={() => {
                            this.setState(prevState => { return { leadstatus: val.LeadStatusName } })
                            this.props.closeDialogLeadStatus() 
                            }} key={key} style={{ width: '100%', flexDirection: 'row', height: 56, borderBottomWidth: 0.5, borderBottomColor: GREY_400, justifyContent: 'flex-start', alignItems: "center", paddingLeft: 16, paddingRight: 16 }}>
                            <View style={{ 
                                width: 18, 
                                height: 18, 
                                borderRadius: 9,
                                marginRight: 10,
                                backgroundColor: val.LeadStatusColour 
                            }} />
                            <Text>{_.startCase(val.LeadStatusName)}</Text>
                        </TouchableOpacity>
                    ))}
                </Dialog>
            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        width: '100%', 
        flexDirection: 'row', 
        backgroundColor: PRIMARY_COLOR, 
        height: Platform.OS == "ios" ? 65 : 56, 
        paddingTop: Platform.OS == "ios" ? 15 : 0,
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center',
        alignItems: 'center' 
    },
    navBarTitle: { 
        height: 56, 
        width: '80%', 
        alignItems: 'flex-start', 
        justifyContent: "center"
    }
})

const mapStateToProps = ({ leadsReducer }) => {
    const { leadStatus, dialog, dialogLs } = leadsReducer;
    return { leadStatus, dialog, dialogLs };
}

export default connect(mapStateToProps, {
    getLeadStatus,
    closeDialogFilter,
    openDialogLeadStatus, 
    closeDialogLeadStatus,
    getLeads
})(Filter);