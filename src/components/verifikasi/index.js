/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-12 23:10:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-21 14:27:46
 */
import React, {Component} from 'react';
import {View, Keyboard, Alert, AsyncStorage, ActivityIndicator } from 'react-native';
import {connect} from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';

import {Text, Button, TextInput} from '../component';
import globalStyles from '../../assets/styles';
import {WHITE, GREY_900, PRIMARY_COLOR} from '../../libraries/colors';
import styles from './styles';
import {getVerifikasi} from '../../actions/verifikasiActions';

type Props = {
    type: string
};
type State = {
    text1: string,
    text2: string,
    text3: string,
    text4: string,
};

class Verifikasi extends Component<Props,State> {
    
    state = {
        text1: '',
        text2: '',
        text3: '',
        text4: '',
        registerData: {},
    }

    componentDidMount() {
        switch (this.props.type) {
            case 'register':
                AsyncStorage.getItem('@temporary:register', (error, result) => {
                    if(result !== null) {
                        this.setState({ registerData: JSON.parse(result) });
                    }
                });
                Alert.alert(
                    'Info',
                    'Proses Registrasi berhasil, silakan masukan kode verifikasi yang dikirim melalui email',
                    [
                        {text: 'OK', onPress: () => {} },
                    ],
                    { cancelable: false }
                )
                break;
            case 'lupapassword':
                AsyncStorage.getItem('@temporary:resetpassword', (error, result) => {
                    if(result !== null) {
                        this.setState({ registerData: JSON.parse(result) });
                    }
                });
                Alert.alert(
                    'Info',
                    'Permintaan reset password berhasil, silakan masukan kode verifikasi yang dikirim melalui email',
                    [
                        {text: 'OK', onPress: () => {} },
                    ],
                    { cancelable: false }
                )
                break;
            default:
                break;
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.response !== prevProps.response) {
            return this.props.response;
        }
        return null;
    }
    
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot) {
                if(this.props.type === "register") {
                    Actions.push('detailregister');
                } else {
                    Actions.push('resetpassword');
                }
            } else {
                Alert.alert(
                    'Info',
                    'Kode verifikasi yang anda masukan tidak valid',
                    [
                        {text: 'ULANGI', onPress: () => {} },
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    render() {
        const { loading } = this.props;
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <View style={{ position: 'absolute', backgroundColor: PRIMARY_COLOR, top: 0, width: '100%', height: 120 }} />
                <View style={styles.container}>
                    <View style={styles.boxverifikasi}>
                        <Text style={styles.title}>Verifikasi Email Anda</Text>
                        <Text style={{ marginBottom: 30 }}>Masukan kode yang dikirimkan melalui email</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                            <TextInput 
                                refs={ref => this._text1 = ref}
                                label=""
                                customStyle={styles.boxInput}
                                value={this.state.text1}
                                returnKeyType="next"
                                maxLength={1}
                                keyboardType="number-pad"
                                autoFocus={true}
                                onChangeText={ text => {
                                    this.setState({text1: text})
                                    if(text.length > 0) {
                                        this._text2.focus()
                                    } else {
                                        this._text1.focus();
                                    }
                                }}
                                onSubmitEditing={() => this._text2.focus() }
                                onKeyPress={({ nativeEvent }) => {
                                    if(nativeEvent.key === 'Backspace') {
                                        this._text1.focus();
                                    } else {
                                        this._text2.focus()
                                    }
                                }}
                            />

                            <TextInput 
                                refs={ref => this._text2 = ref}
                                label=""
                                customStyle={styles.boxInput}
                                value={this.state.text2}
                                returnKeyType="next"
                                maxLength={1}
                                keyboardType="number-pad"
                                onChangeText={ text => {
                                    this.setState({text2: text})
                                    if(text.length > 0) {
                                        this._text3.focus()
                                    } else {
                                        this._text2.focus();
                                    }
                                }}
                                onSubmitEditing={() => this._text3.focus() }
                                onKeyPress={({ nativeEvent }) => {
                                    if(nativeEvent.key === 'Backspace') {
                                        this._text1.focus();
                                    } else {
                                        this._text3.focus()
                                    }
                                }}
                            />

                            <TextInput 
                                refs={ref => this._text3 = ref}
                                label=""
                                customStyle={styles.boxInput}
                                value={this.state.text3}
                                returnKeyType="next"
                                maxLength={1}
                                keyboardType="number-pad"
                                onChangeText={ text => {
                                    this.setState({text3: text}) 
                                    if(text.length > 0) {
                                        this._text4.focus()
                                    } else {
                                        this._text3.focus();
                                    }
                                }}
                                onSubmitEditing={() => this._text4.focus() }
                                onKeyPress={({ nativeEvent }) => {
                                    if(nativeEvent.key === 'Backspace') {
                                        this._text2.focus();
                                    } else {
                                        this._text4.focus()
                                    }
                                }}
                            />

                            <TextInput 
                                refs={ref => this._text4 = ref}
                                label=""
                                customStyle={styles.boxInput}
                                value={this.state.text4}
                                returnKeyType="next"
                                maxLength={1}
                                keyboardType="number-pad"
                                onChangeText={ text => this.setState({text4: text}) }
                                onSubmitEditing={() => Keyboard.dismiss() }
                                onKeyPress={({ nativeEvent }) => {
                                    if(nativeEvent.key === 'Backspace') {
                                        this._text3.focus();
                                    } else {
                                        Keyboard.dismiss()
                                    }
                                }}
                            />
                        </View>
                    </View>
                    <Button style={globalStyles.buttonPrimary} onPress={() => {
                        const { registerData } = this.state;
                        Keyboard.dismiss();
                        this.setState({ loading: true })
                        this.props.getVerifikasi({
                            insertVerifyCode: `${this.state.text1}${this.state.text2}${this.state.text3}${this.state.text4}`,
                            validateVerifiyCode: registerData.verifycode,
                            type: this.props.type
                        });
                        }}>
                        {loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: WHITE, letterSpacing: 2 }}>Verifikasi</Text> }
                    </Button>

                    <Button style={[globalStyles.buttonLink, { marginTop: 20, marginBottom: 50 }]} onPress={() => alert('Tautan belum tersedia')}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{ color: GREY_900 }}>Klik </Text>
                            <Text style={{ color: PRIMARY_COLOR }}>Tautan Ini</Text>
                            <Text style={{ color: GREY_900 }}> apabila tidak menerima kode verifikasi</Text>
                        </View>
                    </Button>
                </View>
            </KeyboardAwareScrollView>
        );
    }

}

const mapStateToProps = ({ verifikasiReducer }) => {
    return verifikasiReducer
}

export default connect(mapStateToProps, {
    getVerifikasi
})(Verifikasi);