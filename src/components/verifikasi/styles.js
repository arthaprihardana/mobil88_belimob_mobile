/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-12 23:14:40 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-12 23:20:10
 */
import {StyleSheet} from 'react-native';
import {GREY_300, GREY_400, WHITE, PRIMARY_COLOR} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26
    },
    boxverifikasi: {
        width: '100%', 
        // borderColor: GREY_300, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1
    },
    title: { 
        color: PRIMARY_COLOR, 
        fontSize: 18
    },
    boxInput: { 
        textAlign: 'center', 
        width: 60, 
        height: 60, 
        borderColor: GREY_400,
        fontSize: 22,
        fontWeight: 'bold'
    }
});

export default styles;