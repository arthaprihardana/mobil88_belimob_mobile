/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 13:47:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-20 13:40:30
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, DatePickerAndroid, DatePickerIOS, Platform, Modal } from 'react-native';
import { Text, Button } from './index';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {GREY_300, GREY_600, RED_300, WHITE} from '../../libraries/colors';
import moment from 'moment';
import 'moment/locale/id';
import globalStyles from '../../assets/styles';
/**
 * @class
 * @extends Component
 * @param {Object} props - Props data yang dikirim ke dalam DatePicker Component 
 * @example
 * import { DatePicker } from './path/to/component/index.js'
 * return (
 *  <DatePicker 
 *      label="Title"
 *      value="..."
 *      callback={ data => { ... }}
 *      errorMessage="..."
 *  />
 * )
 */
class DatePicker extends Component {

    state = {
        modalVisible: false,
        chosenDate: new Date()
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    /**
     * Open modal Calendar From Component
     * @method openCalendar
     * @async
     * @memberof DatePicker
     */
    openCalendar = async () => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                date: new Date()
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                this.props.callback({date: new Date(year, month, day) })
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }
    /**
     * Render Component DatePicker
     * @method render
     * @returns {Object} DatePicker Component
     * @memberof DatePicker
     */
    render() {
        return (
            <View style={[{ width: '100%', marginBottom: 20 }, , this.props.style ? this.props.style : {}]}>
                { this.props.label && <Text h6 style={{ marginBottom: 10 }}>{this.props.label}</Text> }
                <TouchableOpacity 
                    activeOpacity={0.7}
                    onPress={() => {
                        if(Platform.OS == "ios") {
                            this.setModalVisible(true);
                        } else {
                            this.openCalendar()
                        }
                    }}
                    style={{ 
                        flexDirection: 'row', 
                        justifyContent: 'space-between', 
                        alignItems: 'center', 
                        height: 50,
                        borderColor: '#ced4da',
                        borderWidth: 1,
                        paddingLeft: 12,
                        paddingRight: 16,
                        paddingTop: 6,
                        paddingBottom: 6,
                        fontFamily: 'Roboto-Regular',
                        borderRadius: 4,
                        fontSize: 16
                        }}>
                    <Text>{this.props.value}</Text>
                    <FontAwesome5 name="calendar-alt" size={16} color={GREY_600} />
                </TouchableOpacity>
                {this.props.errorMessage !== '' ? <Text small style={{ color: RED_300 }}>{this.props.errorMessage}</Text> : <View /> }
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { console.log('Modal has been closed.') }}>
                    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }}>
                        <View style={{ position: 'absolute', bottom: 0, width: '100%', backgroundColor: '#fff' }}>
                            <View style={{ height: 56, width: '100%', backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center' }}>
                                <Text>{this.props.label}</Text>
                            </View>
                            <DatePickerIOS
                                mode="date"
                                date={this.state.chosenDate}
                                onDateChange={(newDate) => this.setState({chosenDate: newDate}) }
                                />
                            <View style={{ paddingLeft: 16, paddingRight: 16, paddingBottom: 16 }}>
                                <Button style={globalStyles.buttonPrimary} onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible) 
                                    this.props.callback({ date: this.state.chosenDate })
                                }}>
                                    <Text style={{ color: WHITE, letterSpacing: 2 }}>Selesai</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

}

export default DatePicker;