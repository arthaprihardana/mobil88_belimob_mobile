/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 11:57:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-04 14:45:51
 */
import React from 'react';
import { StyleSheet, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { PRIMARY_COLOR, PRIMARY_COLOR_DARK } from '../../libraries/colors';

/** 
 * @class
 * @returns {Object} Block Header Component
 * @example
 * import {BlockHeader} from './path/to/component/index.js'
 * return (
 *  <BlockHeader />
 * )
 */
const BlockHeader = () => <View style={styles.container} />;

const styles = StyleSheet.create({
    container: { 
        position: 'absolute',
        backgroundColor: PRIMARY_COLOR, 
        top: 0, 
        width: '100%', 
        height: 120 
    }
})

export default BlockHeader;