/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-12 11:34:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-02 16:18:15
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Text } from './index';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {GREY_300, GREY_600, RED_300} from '../../libraries/colors';
/**
 * @class
 * @extends Component
 * @param {Object} props - Props data yang dikirim ke dalam SelectComponent
 * @example
 * import { Select } from './path/to/component/index.js'
 * <Select 
 *      label="..."
 *      onPress={() => {...}}
 *      value="..."
 * />
 */
class SelectComponent extends Component {
    /**
     * @method render
     *
     * @returns {Object} Return SelectComponent
     * @memberof SelectComponent
     */
    render() {
        return (
            <View style={[{ width: '100%', marginBottom: 20 }, , this.props.style ? this.props.style : {}]}>
                { this.props.label && <Text h6 style={{ marginBottom: 10 }}>{this.props.label}</Text> }
                <TouchableOpacity 
                    activeOpacity={0.7}
                    onPress={this.props.onPress} 
                    style={{ 
                        flexDirection: 'row', 
                        justifyContent: 'space-between', 
                        alignItems: 'center', 
                        height: 50,
                        borderColor: '#ced4da',
                        borderWidth: 1,
                        paddingLeft: 12,
                        paddingRight: 16,
                        paddingTop: 6,
                        paddingBottom: 6,
                        fontFamily: 'Roboto-Regular',
                        borderRadius: 4,
                        fontSize: 16
                        }}>
                    <View style={{ width: '80%' }}>
                        <Text>{this.props.value}</Text>
                    </View>
                    {this.props.loading && <ActivityIndicator size={12} color={'#721c24'} /> }
                    <FontAwesome5 name="angle-down" size={16} color={GREY_600} />
                </TouchableOpacity>
                {this.props.errorMessage !== '' ? <Text small style={{ color: RED_300 }}>{this.props.errorMessage}</Text> : <View /> }
            </View>
        );
    }

}

export default SelectComponent;