/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 13:22:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-16 13:27:20
 */
import React, { Component } from 'react';
import { View, Picker, Platform, ActionSheetIOS, StyleSheet } from 'react-native';
import { Text } from './index';
import {GREY_200, GREY_300, GREY_500, GREY_400} from '../../libraries/colors';

/**
 * @class
 * @extends Component
 * @param {Object} props - Props data yang dikirim ke dalam PickerComponent
 * @example
 * import {Picker} from './path/to/component/index.js'
 * return (
 *  <Picker />
 * )
 */
class PickerComponent extends Component {
    /**
     * @const state
     *
     * @memberof PickerComponent
     */
    state = {
        value: []
    }
    /**
     * @method onPress
     *
     * @memberof PickerComponent
     */
    onPress() {
        const { children } = this.props;
        let arr = [];
        let idx = 0;
        for(let i=0; i<children.length; i++) {
            arr.push(children[i].label);
            if(this.props.pickDefault == children[i].label) {
                idx = i+1
            }
            if(arr.length == children.length) {
                ActionSheetIOS.showActionSheetWithOptions({
                    options: ['Cancel', ...arr],
                    destructiveButtonIndex: idx,
                    cancelButtonIndex: 0,
                }, (buttonIndex) => {
                    for(let j=0; j<children.length; j++) {
                        if(buttonIndex == j+1) {
                            this.props.callbackWhenIos(children[j])
                        }
                    }
                });
            }
        }
    }
    /**
     * @method render
     *
     * @returns {Object} Return PickerComponent
     * @memberof PickerComponent
     */
    render() {
        const { pickDefault, children, label } = this.props;
        if(Platform.OS == 'ios') {
            return (
                <TouchableOpacity onPress={() => this.onPress()}>
                    <View style={{ width: '100%', height: 40, justifyContent: 'center'}}>
                        <Text>{pickDefault}</Text>
                    </View>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={[styles.boxTextInput, { minHeight: 66 }]}>
                    <Text bold h6>{label}</Text>
                    <View style={{
                        height: 50, 
                        borderColor: GREY_400, 
                        borderWidth: 1, 
                        paddingLeft: 10, 
                        paddingRight: 10, 
                        justifyContent: 'center',
                        borderRadius: 5
                    }}>
                        <Picker {...this.props}>
                            { children }
                        </Picker>
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    boxTextInput: {
        // height: 86, 
        justifyContent: 'flex-start', 
        marginBottom: 16,
    }
});

export default PickerComponent;