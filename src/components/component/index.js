/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 11:27:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-17 13:39:06
 */
import TextComponent from './Text';
import ButtonComponent from './Button';
import TextInputComponent from './TextInput';
import BlockHeaderComponent from './blockHeader';
import PickerComponent from './Picker';
import SelectComponent from './Select';
import DatePickerComponent from './DatePicker';
import TimePickerComponent from './TimePicker';
import ImageComponent from './Image';
import DialogComponent from './Dialog';

export const Text = TextComponent;
export const Button = ButtonComponent;
export const TextInput = TextInputComponent;
export const Picker = PickerComponent;
export const BlockHeader = BlockHeaderComponent;
export const Select = SelectComponent;
export const DatePicker = DatePickerComponent;
export const TimePicker = TimePickerComponent;
export const Image = ImageComponent;
export const Dialog = DialogComponent;