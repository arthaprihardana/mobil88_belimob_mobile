/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 16:51:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-12-27 14:21:55
 */
import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Platform, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Button, Text } from '../component'
import {GREY_200, WHITE, GREY_500, RED_400, RED_300, GREY_300, GREY_400, GREEN_500} from '../../libraries/colors';

// type Props = {
//     label: string,
//     value: string,
//     type: string
// };

// type State = {
//     secureText: boolean
// };
/**
 * @class
 * @param {Object} props - Props data yang dikirim ke dalam TextInputComponent
 * @returns {Object} Return TextInputComponent
 * @example
 * import { Text } from './path/to/component/index.js'
 * return (
 *  <TextInput 
 *      label="..."
 *      value="..."
 *      onChangeText={ text => { ... }}
 *      keyboardType="..."
 *      ...
 *  />
 * )
 */
class TextInputComponent extends Component {
    /**
     * @const state
     *
     * @memberof TextInputComponent
     */
    state = {
        secureText: true
    }
    /**
     * @method render
     *
     * @returns {Object} Return TextInputComponent
     * @memberof TextInputComponent
     */
    render() {
        const props = this.props;
        return (
            <View style={[styles.boxTextInput, { minHeight: 66 }]}>
                {props.label !== undefined && <Text h6 style={{ marginBottom: 10 }}>{props.label}</Text>}
                <TextInput 
                    ref={props.refs}
                    style={[{
                        height: 50, 
                        borderColor: '#ced4da', 
                        borderWidth: 1, 
                        paddingLeft: 12, 
                        paddingRight: props.type !== undefined ? 45 : 12, 
                        paddingTop: 6, 
                        paddingBottom: 6, 
                        fontFamily: 'Roboto-Regular',
                        borderRadius: 4,
                        fontSize: 16,
                        color: '#495057'
                        // lineHeight: 1.5
                    }, props.customStyle]}
                    value={props.value}
                    secureTextEntry={props.type === "password" || props.secureTextEntry !== undefined ? this.state.secureText : false }
                    underlineColorAndroid={"transparent"}
                    {...props}
                    />
                {props.type !== undefined ? 
                <TouchableOpacity onPress={() => this.setState({secureText: !this.state.secureText}) } style={styles.eye}>
                    <FontAwesome5 name={this.state.secureText ? "eye-slash" : "eye"} size={16} color={this.state.secureText ? '#ced4da' : '#155724'} />
                </TouchableOpacity>
                : <View /> }
                {props.errorMessage !== '' ? <Text small style={{ color: RED_300 }}>{props.errorMessage}</Text> : <View /> }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    boxTextInput: {
        // height: 86, 
        justifyContent: 'flex-start', 
        marginBottom: 16
    },
    eye: { 
        position: 'absolute', 
        right: Platform.OS === "ios" ? 10 : 15, 
        top: Platform.OS === "ios" ? '54%' : 54,
    }
});

export default TextInputComponent;