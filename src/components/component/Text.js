/**
 * @author: Artha Prihardana 
 * @Date: 2018-05-13 09:36:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-13 14:54:18
 */
import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { FontFamily, FontFamilyBold, FontFamilyLight, FontFamilyBlack, FontFamilyThin } from '../../libraries/fonts';
import {GREY_900, GREY_800, GREY_600} from '../../libraries/colors';

const styles = StyleSheet.create({
    black: FontFamilyBlack,
    thin: FontFamilyThin,
    light: FontFamilyLight,
    bold: FontFamilyBold,
    font: FontFamily,
    text: { fontSize: 16, color: '#383d41', lineHeight: 27 }
});
/**
 * @class
 * @param {Object} props - Props data yang dikirim ke dalam TextComponent
 * @returns {Object} Return TextComponent
 * @example
 * import { Text } from './path/to/component/index.js'
 * return (
 *  <Text {(h1|h2|h3|h4|h5|h6|small|bold)} style={{ ... }}>
 *      <AnotherComponent />
 *  </Text>
 * )
 */
const TextComponent = props => {
    const { style, children, h1, h2, h3, h4, h5, h6, small, bold, ...rest } = props;
  
    return (
        <Text
            style={[
            styles.font,
            styles.text,
            h1 && { fontSize: 40, lineHeight: 60 },
            h2 && { fontSize: 32, lineHeight: 48 },
            h3 && { fontSize: 28, lineHeight: 42 },
            h4 && { fontSize: 24, lineHeight: 36 },
            h5 && { fontSize: 20, lineHeight: 30 },
			h6 && { fontSize: 18, lineHeight: 27 },
			small && { fontSize: 12, lineHeight: 18 },
			bold && styles.bold,
            style && style,
            ]}
            {...rest}
        >
            {children}
        </Text>
    );
};
  
TextComponent.propTypes = {
    style: PropTypes.any,
    h1: PropTypes.bool,
    h2: PropTypes.bool,
    h3: PropTypes.bool,
    h4: PropTypes.bool,
    h5: PropTypes.bool,
    h6: PropTypes.bool,
    small: PropTypes.bool,
	bold: PropTypes.bool,
    fontFamily: PropTypes.string,
    children: PropTypes.any,
};

export default TextComponent;