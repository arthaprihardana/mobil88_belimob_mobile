/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 13:44:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 14:53:12
 */
import React from 'react';
import { Platform, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import Ripple from 'react-native-material-ripple';
/**
 * @class
 * @param {Object} props - Props data yang dikirim ke dalam Button Component
 * @returns {Object} Return Button Component
 * @example
 * import { Button } from './path/to/component/index.js'
 * return (
 *  <Button style={{ ... }} onPress={() => { ... }}>
 *      <AnotherComponent />
 *  </Button>
 * )
 */
const ButtonComponent = props => (
    (Platform.OS == 'ios') ?
        <TouchableOpacity activeOpacity={0.8} {...props}>
            {props.children}
        </TouchableOpacity>
    : <Ripple
        delayPressIn={0}
        rippleOpacity={0.1}
        rippleContainerBorderRadius={0}
        background={TouchableNativeFeedback.SelectableBackground()} // eslint-disable-line new-cap
        {...props}
    >
        {props.children}
    </Ripple>
)
  
export default ButtonComponent;