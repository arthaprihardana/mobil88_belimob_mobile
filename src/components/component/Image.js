/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-17 11:21:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-17 11:36:21
 */
import React, { Component, PureComponent } from 'react';
import { Animated, View, StyleSheet } from 'react-native';

/**
 * @class
 * @extends {Component}
 * @param {Object} props - Props data yang dikirim ke dalam ImageComponent
 * @example
 * import { Image } from './path/to/component/index.js'
 * <Image 
 *      style={{...}}
 *      thumbnailSource={require('./path/to/image/default')}
 *      source={{uri:'./path/to/image'}}
 *      placeholderColor="#FFFFFF"
 * />
 */
class ImageComponent extends PureComponent {
    
    thumbnailAnimated = new Animated.Value(0);

    imageAnimated = new Animated.Value(0);

    handleThumbnailLoad = () => {
        Animated.timing(this.thumbnailAnimated, {
            toValue: 1,
        }).start();
    }

    onImageLoad = () => {
        Animated.timing(this.imageAnimated, {
            toValue: 1,
        }).start();
    }

    render() {
        const {
            thumbnailSource,
            source,
            style,
            ...props
        } = this.props;

        return (
        <View>
            <Animated.Image
                {...props}
                source={thumbnailSource}
                style={[style, { opacity: this.thumbnailAnimated }]}
                onLoad={this.handleThumbnailLoad}
                blurRadius={1}
                />
            <Animated.Image
                {...props}
                source={source}
                style={[styles.imageOverlay, { opacity: this.imageAnimated }, style]}
                onLoad={this.onImageLoad}
                />
        </View>
        );
    }
}

const styles = StyleSheet.create({
    imageOverlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
    },
    container: {
        backgroundColor: '#e1e4e8',
    },
});

export default ImageComponent;