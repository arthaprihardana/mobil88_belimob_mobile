/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-17 13:35:16 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-17 13:49:20
 */
import React from 'react';
import { Modal, View, TouchableOpacity, ScrollView, StyleSheet, Platform } from 'react-native';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import { Text } from './index';
import { PRIMARY_COLOR, WHITE } from '../../libraries/colors';
/**
 * @class
 * @param {Object} props - Props data yang dikirim ke dalam Dialog Component
 * @returns {Object} Dialog Component
 * @example
 * import { Dialog } from './path/to/component/index.js'
 * return (
 *  <Dialog 
 *      title="..."
 *      onRequestClose={() => { ... }}
 *      visible={(true|false)}>
 *      <AnotherComponent />
 *  </Dialog>
 * )
 */
const Dialog = props => (
    <Modal
        animationType="slide"
        transparent={false}
        visible={props.visible}
        onRequestClose={props.onRequestClose}>
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <TouchableOpacity onPress={props.onRequestClose} style={styles.closeButton}>
                    <Ionicons name="md-close" color={WHITE} size={20} />
                </TouchableOpacity>
                <View style={styles.navBarTitle}>
                    <Text bold style={{ color: WHITE }}>{props.title}</Text>
                </View>
            </View>
            <ScrollView keyboardShouldPersistTaps="always">
                {props.children}
            </ScrollView>
        </View>
    </Modal>
)

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        width: '100%', 
        flexDirection: 'row', 
        backgroundColor: PRIMARY_COLOR, 
        height: Platform.OS == "ios" ? 65 : 56, 
        paddingTop: Platform.OS == "ios" ? 15 : 0,
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },
    closeButton: { 
        width: 56, 
        height: 56, 
        justifyContent: 'center',
        alignItems: 'center' 
    },
    navBarTitle: { 
        height: 56, 
        width: '80%', 
        alignItems: 'flex-start', 
        justifyContent: "center"
    }
});

export default Dialog;