/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 13:47:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-09 14:56:43
 */
import React, { Component } from 'react';
import { View, TouchableOpacity, TimePickerAndroid, Platform, DatePickerIOS, Modal } from 'react-native';
import { Text, Button } from './index';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {GREY_300, GREY_600, WHITE} from '../../libraries/colors';
import moment from 'moment';
import 'moment/locale/id';
import globalStyles from '../../assets/styles';
/**
 * Component TimePicker
 *
 * @export
 * @class TimePicker
 * @extends {Component}
 * 
 * @example
 * import { TimePicker } from './path/to/component/index.js'
 * return (
 *  <TimePicker 
 *      label="..."
 *      value="..."
 *      callback={ data => { ... }}
 *      errorMessage="...."
 *  />
 * )
 */
class TimePicker extends Component {

    state = {
        modalVisible: false,
        choosenTime: new Date()
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    /**
     * @method openWatch
     *
     * @memberof TimePicker
     */
    openWatch = async () => {
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
                hour: moment().get('hour'),
                minute: moment().get('minute'),
                is24Hour: true,
                mode: 'spinner'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
                this.props.callback({time: moment().set({hour: hour, minute: minute}).format('HH:mm') })
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }
    /**
     * @method render
     *
     * @returns
     * @memberof TimePicker
     */
    render() {
        return (
            <View style={[{ width: '100%', marginBottom: 20 }, , this.props.style ? this.props.style : {}]}>
                { this.props.label && <Text h6 style={{ marginBottom: 10 }}>{this.props.label}</Text> }
                <TouchableOpacity 
                    activeOpacity={0.7}
                    onPress={() => {
                        if(Platform.OS == "ios") {
                            this.setModalVisible(true);
                        } else {
                            this.openWatch();
                        }
                    }} 
                    style={{ 
                        flexDirection: 'row', 
                        justifyContent: 'space-between', 
                        alignItems: 'center', 
                        height: 50,
                        borderColor: '#ced4da',
                        borderWidth: 1,
                        paddingLeft: 12,
                        paddingRight: 16,
                        paddingTop: 6,
                        paddingBottom: 6,
                        fontFamily: 'Roboto-Regular',
                        borderRadius: 4,
                        fontSize: 16
                        }}>
                    <Text>{this.props.value}</Text>
                    <FontAwesome5 name="clock" size={16} color={GREY_600} />
                </TouchableOpacity>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { console.log('Modal has been closed.') }}>
                    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }}>
                        <View style={{ position: 'absolute', bottom: 0, width: '100%', backgroundColor: '#fff' }}>
                            <View style={{ height: 56, width: '100%', backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center' }}>
                                <Text>{this.props.label}</Text>
                            </View>
                            <DatePickerIOS
                                mode="time"
                                date={this.state.choosenTime}
                                onDateChange={(newDate) => this.setState({choosenTime: newDate}) }
                                />
                            <View style={{ paddingLeft: 16, paddingRight: 16, paddingBottom: 16 }}>
                                <Button style={globalStyles.buttonPrimary} onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible) 
                                    this.props.callback({ time: moment().set({ hour: this.state.choosenTime.getHours(), minute: this.state.choosenTime.getMinutes() }).format('HH:mm') })
                                }}>
                                    <Text style={{ color: WHITE, letterSpacing: 2 }}>Selesai</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

}

export default TimePicker;