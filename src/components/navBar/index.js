/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-13 14:53:18 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 13:30:05
 */
import React, { Component } from 'react';
import {connect} from 'react-redux';
import { View, StyleSheet, Platform, TouchableOpacity, AsyncStorage } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import { Button, Text } from '../component';
import {GREY_500, WHITE, GREY_900, PRIMARY_COLOR, GREY_300} from '../../libraries/colors';
import {getDashboard, getLeads, getInsentive, getTeamDownline, getMember} from '../../actions';
import moment from 'moment';
import 'moment/locale/id';

type Props = {
    selectedIndex: number
};

type State = {};

class NavBar extends Component<Props, State> {

    async _goToDashboard() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember);
        this.setState({ member: member });
        this.props.getDashboard({
            MemberCode: member.MemberCode,
            Month: moment().format('MM'),
            Year: moment().format('YYYY'),
        });
    }

    async _goToLeads() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember)
        this.setState({ member: member })
        this.props.getLeads({
            MemberCode: member.MemberCode,
            page: 1
        });
    }

    async _goToTeam() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember);
        this.props.getInsentive(member.MemberCode);
        this.props.getTeamDownline(member.MemberCode);
    }

    async _goToProfile() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember);
        this.props.getMember(member.MemberCode);
    }

    render() {
        const { selectedIndex } = this.props;
        return (
            <View style={styles.container}>
                <View style={[styles.tabs]}>
                    <Button 
                        onPressIn={() => {
                            Actions.jump('home');
                            this._goToDashboard();
                        }}
                        style={styles.tab}>
                        <Ionicons name="md-home" size={Platform.OS === "ios" ? 18 : 24} color={ selectedIndex == 0 ? PRIMARY_COLOR : GREY_900 } />
                        <Text style={{ color: selectedIndex == 0 ? PRIMARY_COLOR : GREY_900 , fontSize: 10, lineHeight: Platform.OS === "ios" ? 0 : 15 }}>Beranda</Text>
                    </Button>

                    <Button 
                        onPressIn={() => {
                            Actions.jump('leads')
                            this._goToLeads()
                        }}
                        style={styles.tab}>
                        <Ionicons name="md-filing" size={Platform.OS === "ios" ? 18 : 24} color={ selectedIndex == 1 ? PRIMARY_COLOR : GREY_900 } />
                        <Text style={{ color: selectedIndex == 1 ? PRIMARY_COLOR : GREY_900, fontSize: 10, lineHeight: Platform.OS === "ios" ? 0 : 15 }}>Leads</Text>
                    </Button>

                    <Button 
                        onPressIn={() => {
                            Actions.jump('team')
                            this._goToTeam()
                        }}
                        style={styles.tab}>
                        <Ionicons name="md-list-box" size={Platform.OS === "ios" ? 18 : 24} color={ selectedIndex == 2 ? PRIMARY_COLOR : GREY_900 } />
                        <Text style={{ color: selectedIndex == 2 ? PRIMARY_COLOR : GREY_900, fontSize: 10, lineHeight: Platform.OS === "ios" ? 0 : 15 }}>Tim</Text>
                    </Button>

                    <Button 
                        onPressIn={() => {
                            Actions.jump('profil')
                            this._goToProfile()
                        }} 
                        style={styles.tab}>
                        <Ionicons name="md-contact" size={Platform.OS === "ios" ? 18 : 24} color={ selectedIndex == 3 ? PRIMARY_COLOR : GREY_900 } />
                        <Text style={{ color: selectedIndex == 3 ? PRIMARY_COLOR : GREY_900, fontSize: 10, lineHeight: Platform.OS === "ios" ? 0 : 15 }}>Profil Saya</Text>
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { 
        marginBottom: -1, 
        marginTop: 0, 
        borderTopWidth: .5, 
        borderTopColor: GREY_300
    },
    tab: {
        flex: 1,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabs: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0,
        backgroundColor: WHITE
    },
});

const mapStateToProps = ({ dashboardReducer, leadsReducer, teamReducer, memberReducer }) => {
    const { dashboard } = dashboardReducer;
    const { leads } = leadsReducer;
    const { insentive, team } = teamReducer;
    const { member } = memberReducer;
    return { dashboard, leads, insentive, team, member };
}

export default connect(mapStateToProps, {
    getDashboard,
    getLeads,
    getInsentive,
    getTeamDownline,
    getMember
})(NavBar);

// export default NavBar;