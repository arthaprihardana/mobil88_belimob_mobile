/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 10:41:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-09 16:49:28
 */
import {StyleSheet} from 'react-native';
import {WHITE, PRIMARY_COLOR, GREY_100} from '../../libraries/colors';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 160
    },
    version: {
        position: 'absolute',
        textAlign: 'center',
        color: GREY_100,
        fontSize: 12,
        bottom: 140
    },
    siluet: {
        position: 'absolute',
        bottom: 0, 
        left: -100
    },
    logo: {
        width: 200, 
        height: 100
    }
})