/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-08 20:58:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-12 22:17:01
 */
import React, {Component} from 'react';
import { AsyncStorage, Image, ActivityIndicator, Keyboard } from 'react-native';
import { Text } from '../component';
import { Actions } from 'react-native-router-flux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from 'react-native-linear-gradient';

import {styles} from './styles';
import {WHITE, PRIMARY_COLOR, PRIMARY_COLOR_DARK, GREY_50} from '../../libraries/colors';

type Props = {}
type State = {
    login: Boolean,
    loading: Boolean
}

let timer = 3000;
let version = require('../../../version.json').version;

class Splashscreen extends Component<Props, State> {

    state = {
        login: false,
        loading: false
    }

    componentDidMount() {
        Keyboard.dismiss();
        this.setState({ loading: true });
        AsyncStorage.multiGet(['@global:skip', '@global:isLogin'], (error, result) => {
            let skip = result[0];
            let isLogin = result[1][1];
            setTimeout(() => {
                if(isLogin === "true") {
                    Actions.jump('home');
                } else {
                    Actions.jump('login');
                }
            }, timer)
        });        
    }
    
    render() {
        return (
            <LinearGradient
                colors={[PRIMARY_COLOR_DARK, PRIMARY_COLOR, PRIMARY_COLOR_DARK]}
                start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                style={styles.container}>
                <Image source={require('../../assets/images/logo1_white.png')} style={styles.logo} resizeMode={'contain'} />
                { this.state.loading ? <ActivityIndicator color={WHITE} /> : null }
                <Text style={styles.version}>version {version}</Text>
            </LinearGradient>
        );
    }
}

export default Splashscreen;