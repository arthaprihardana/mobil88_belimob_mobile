/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 14:26:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-04 14:36:23
 */
import {StyleSheet} from 'react-native';
import {GREY_200, WHITE, PRIMARY_COLOR, GREY_300, RED_700, GREY_900, GREY_100, RED_600, RED_400, RED_100, RED_200} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: { 
        paddingLeft: 16, 
        paddingRight: 16, 
        paddingTop: 30
    },
    boxlogin: {
        width: '100%', 
        // borderColor: GREY_200, 
        // borderWidth: 1, 
        backgroundColor: WHITE,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 16,
        borderRadius: 5,
        // shadowColor: GREY_200,
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        elevation: 1,
        shadowOffset:{  width: 2,  height: 2  },
        shadowColor: '#eee',
        shadowOpacity: 1.0,
    },
    signInTitle: { 
        color: PRIMARY_COLOR, 
        fontSize: 18, 
        marginBottom: 30 ,
    },
    imgLogo: { 
        width: '100%', 
        justifyContent: 'center', 
        alignItems: 'center',
        // marginBottom: 10
    }
});

export default styles;