/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 07:45:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-06 15:16:57
 */
import React, {Component} from 'react';
import {View, ActivityIndicator, Image, Keyboard, ImageBackground, AsyncStorage, Alert, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';

import {Text, Button, TextInput} from '../component';
import globalStyles from '../../assets/styles';
import styles from './styles';
import {WHITE, PRIMARY_COLOR, GREY_900, RED_300, RED_200, PRIMARY_COLOR_DARK} from '../../libraries/colors';
import { Actions } from '../../../node_modules/react-native-router-flux';
import { getLogin } from '../../actions';

type Props = {};
type State = {
    username: String,
    password: String,
    readyToSubmit: Boolean
};

class Login extends Component<Props, State> {

    state = {
        // username: "supportadmin@gmail.com",
        // password: "admin1234"
        username: "",
        password: "",
        readyToSubmit: false,
        emailError: null,
        passwordError: null,
        noConnection: false
    }

    componentDidMount() {
        if(this.props.invalid) {
            Alert.alert(
                'Warning!',
                'Kami telah mendeteksi bahwa Akun Anda digunakan di perangkat lain. Silhakan Login kembali untuk dapat mengakses Aplikasi',
                [
                    {text: 'Saya Mengerti', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                ],
                { cancelable: false }
            )
        }
        if(this.props.session) {
            Alert.alert(
                'Warning!',
                'Sesi login anda telah habis, silahkan login kembali',
                [
                    {text: 'Saya Mengerti', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                ],
                { cancelable: false }
            )
        }
    }
    

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(prevProps.login !== this.props.login) {
            return this.props.login
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.status !== undefined) {
                if(snapshot.status) {
                    let m = snapshot.data;
                    AsyncStorage.multiSet([
                        ['@global:token', m.DeviceToken],
                        ['@global:member', JSON.stringify({
                            MemberCode: m.MemberCode,
                            MemberId: m.MemberId
                        })],
                        ['@global:isLogin', 'true']
                    ]).then(response => Actions.reset('main') );
                } else {
                    Alert.alert(
                        'Info',
                        snapshot.message,
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed')}
                        ],
                        { cancelable: false }
                    )
                }
            } else {
                if(snapshot.connectionInfo && snapshot.connectionInfo === "none") {
                    this.setState(prevState => {
                        return { noConnection: true }
                    });
                } else {
                    this.setState(prevState => {
                        return {
                            emailError: snapshot.email,
                            passwordError: snapshot.password
                        }
                    })
                }
            }
        }
    }

    doLogin = () => {
        Keyboard.dismiss();
        if(this.state.readyToSubmit) {
            this.props.getLogin({
                email: this.state.username,
                password: this.state.password
            })
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView style={[globalStyles.container]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
                <LinearGradient
                    colors={[PRIMARY_COLOR_DARK, PRIMARY_COLOR]}
                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                    style={{ 
                        position: 'absolute',
                        top: 0, 
                        width: '100%', 
                        height: 250
                    }} />
                <View style={{ marginTop: 50 }}>
                    <View style={styles.imgLogo}>
                        <Image source={require('../../assets/images/logo_white.png')} style={{ width: 150, height: 80 }} resizeMode={'contain'} />
                    </View>
                    <View style={styles.container}>
                        <View style={styles.boxlogin}>
                            <Text style={styles.signInTitle}>SIGN IN</Text>
                            <TextInput 
                                    refs={ref => this._username = ref}
                                    label="Email"
                                    value={this.state.username}
                                    keyboardType="email-address"
                                    returnKeyType="next"
                                    onChangeText={ text => this.setState(prevState => {
                                        return {
                                            username: text, 
                                            readyToSubmit: prevState.password !== "" && text !== "" ? true : false,
                                            emailError: null
                                        }
                                    })}
                                    onSubmitEditing={() => this._password.focus() }
                                    errorMessage={this.state.emailError}
                                />

                            <TextInput 
                                refs={ref => this._password = ref }
                                type="password"
                                label="Password"
                                value={this.state.password}
                                returnKeyType="done"
                                onChangeText={ text => this.setState(prevState => {
                                    return { 
                                        password: text, 
                                        readyToSubmit: prevState.username !== "" && text !== "" ? true : false,
                                        passwordError: null 
                                    }
                                })}
                                onSubmitEditing={this.doLogin}
                                errorMessage={this.state.passwordError}
                            />

                            <Button accessible={true} accessibilityLabel="Login" style={this.state.readyToSubmit ? globalStyles.buttonPrimary : globalStyles.buttonDisabled} onPress={this.doLogin}>
                                {this.props.loading ? <ActivityIndicator color={WHITE} /> : <Text style={{ color: this.state.readyToSubmit ? WHITE : '#212529', letterSpacing: 2 }}>Login</Text> }
                            </Button>

                            <TouchableOpacity style={[globalStyles.buttonLink, { marginTop: 20 }]} onPress={() => {
                                Keyboard.dismiss();
                                Actions.push('lupapassword');
                                }}>
                                <Text style={{ color: GREY_900, letterSpacing: 1 }}>Lupa Kata Sandi?</Text>
                            </TouchableOpacity>
                        </View>
                        <Button style={globalStyles.buttonPrimaryOutline} onPress={() => Actions.push('register') }>
                            <Text style={{ color: PRIMARY_COLOR, letterSpacing: 2 }}>Daftar Sekarang</Text>
                        </Button>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = ({ loginReducer }) => {
    const { isLogin, loading, login } = loginReducer;
    return { isLogin, loading, login };
}

export default connect(mapStateToProps, {
    getLogin
})(Login);