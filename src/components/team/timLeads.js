/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-21 11:14:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 11:16:37
 */
import React, {Component} from 'react';
import {ScrollView, View, ListView, TouchableOpacity, FlatList, RefreshControl, ActivityIndicator, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Text, BlockHeader} from '../component';
import styles from './styles';
import {GREY_100, GREY_700, WHITE} from '../../libraries/colors';
import {getTeamDownline} from '../../actions';

type Props = {};
type State = {};

class TimLeads extends Component<Props, State> {

    state = {
        dataSource: [],
        isNull: false,
        refreshing: false,
        team: null
    }

    async componentDidMount() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember);
        if(this.props.type === "all") {
            this.props.getTeamDownline(member.MemberCode)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.team !== prevProps.team) {
            if(this.props.team.status) {
                this.setState({
                    dataSource: this.props.team.data
                })
            }
        }
    }

    render() {
        const { loading } = this.props;
        const { dataSource, refreshing, isNull, team } = this.state;
        return (
            <View>
                <FlatList 
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={dataSource}
                    stickyHeaderIndices={[0]}
                    ListHeaderComponent={() => {
                        return (
                            <View style={{ width: '100%', height: 112, flexDirection: 'column', justifyContent: 'space-between', borderBottomColor: GREY_100, borderBottomWidth: 1, backgroundColor: WHITE }}>
                                <TouchableOpacity onPress={() => {}} style={{ width: '100%', height: 56, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 16, borderBottomColor: GREY_100, borderBottomWidth: 1}}>
                                    <FontAwesome5 name="search" style={{ marginRight: 16 }} />
                                    <Text bold style={{ color: GREY_700 }}>Search</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 56, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 16, paddingRight: 16, backgroundColor: '#eee'}}>
                                    <View style={{ width: '60%' }}><Text>Nama</Text></View>
                                    <View style={{ width: '20%', alignItems: 'center' }}><Text>Proses</Text></View>
                                    <View style={{ width: '20%', alignItems: 'center' }}><Text>Selesai</Text></View>
                                </View>
                            </View>
                        )
                    }}
                    ListEmptyComponent={() => {
                        return (
                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 13 }}>
                            {loading &&
                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator color={'#6c757d'} />
                                    <Text small style={{ color: '#818182'}}>Loading Data ...</Text>
                                </View> }
                            {isNull && 
                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                    <FontAwesome5 name="search" size={14} />
                                    <Text small style={{ color: '#818182'}}>Data Not Found</Text>
                                </View>}
                        </View>
                        )
                    }}
                    renderItem={({item, index}) => {
                        return (
                            <View
                                style={{ 
                                    width: '100%', 
                                    height: 56, 
                                    borderBottomColor: GREY_100, 
                                    borderBottomWidth: 1, 
                                    padding: 16,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between', 
                                    alignItems: 'center',
                                }}>
                                <View style={{ width: '60%' }}><Text bold>{item.membername}</Text></View>
                                <TouchableOpacity onPress={() => Actions.jump('detailTeamLeads', {title: "Team Leads Process", status: "process", memberCode: item.membercode })} style={{ width: '20%', alignItems: 'center' }}>
                                    <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: '#fff3cd', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#856404' }}>{item.memberleadprocess}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.jump('detailTeamLeads', {title: "Team Leads Close", status: "close", memberCode: item.membercode})} style={{ width: '20%', alignItems: 'center' }}>
                                    <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: '#d4edda', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#155724' }}>{item.memberleadclosed}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }}
                    keyExtractor={(item, index) => item.membercode}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={() => {} }
                        />
                    }
                />
            </View>
        );
    }

}

const mapStateToProps = ({ teamReducer }) => {
    const { loading, team } = teamReducer;
    return { loading, team };
}

export default connect(mapStateToProps, {
    getTeamDownline
})(TimLeads);