/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-27 10:02:18 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 11:38:40
 */
import React, {Component} from 'react';
import {ScrollView, View, ListView, TouchableOpacity, AsyncStorage, ActivityIndicator, RefreshControl, FlatList, SectionList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Placeholder from 'rn-placeholder';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader} from '../component';
import styles from './styles';
// import Filter from './Filter';
import {GREY_100, GREY_300, RED_500, BLUE_200, GREY_700, GREY_800, GREEN_500, BLUE_500, ORANGE_500, RED_900, WHITE} from '../../libraries/colors';
import { getTeamListLeadsProcess, getTeamListLeadClose } from '../../actions';

type Props = {};
type State = {
    dataSource: Array,
    isNull: Boolean,
    refreshing: Boolean,
    member: Object
};

class DetailTimLeads extends Component<Props, State> {

    state = {
        dataSource: [],
        isNull: false,
        refreshing: false,
        member: null
    }

    componentDidMount() {        
        if(this.props.status === "process") this.props.getTeamListLeadsProcess(this.props.memberCode);
        if(this.props.status === "close") { this.props.getTeamListLeadClose(this.props.memberCode); }
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.leadProcess !== prevProps.leadProcess) {
            if(this.props.leadProcess !== null) {
                if(this.props.leadProcess.status) {
                    if(this.props.leadProcess.data.total !== 0) {
                        this.setState({ dataSource: this.props.leadProcess.data.data })
                    } else {
                        this.setState({ isNull: true })
                    }
                }
            }
        }
        if(this.props.leadClose !== prevProps.leadClose) {
            if(this.props.leadClose !== null) {
                if(this.props.leadClose.status) {
                    if(this.props.leadClose.data.total !== 0) {
                        this.setState({ dataSource: this.props.leadClose.data.data })
                    } else {
                        this.setState({ isNull: true })
                    }
                }
            }
        }
    }
    
    render() {
        const { loading } = this.props;
        const { isNull, refreshing, dataSource, member } = this.state;
        return (
            <View>
            <FlatList 
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={dataSource}
                // stickyHeaderIndices={[0]}
                // ListHeaderComponent={() => {}}
                ListEmptyComponent={() => {
                    return (
                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 13 }}>
                            {loading &&
                                <View style={{ width: '100%' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                            <Placeholder.Media
                                                size={70}
                                                color="#eee"
                                                animate="fade"
                                                />
                                        </View>
                                        <View style={{ width: '50%', paddingLeft: 16, justifyContent: 'center' }}>
                                            <Placeholder.Paragraph
                                                lineNumber={3}
                                                textSize={16}
                                                lineSpacing={5}
                                                color="#eee"
                                                width="100%"
                                                lastLineWidth="70%"
                                                firstLineWidth="50%"
                                                animate="fade"
                                                />
                                        </View>
                                        <View style={{ width: '30%', justifyContent: 'center', paddingLeft: 20 }}>
                                            <Placeholder.Line
                                                color="#eee"
                                                width="70%"
                                                animate="fade"
                                                textSize={22}
                                                />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 13 }}>
                                        <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                            <Placeholder.Media
                                                size={70}
                                                color="#eee"
                                                animate="fade"
                                                />
                                        </View>
                                        <View style={{ width: '50%', paddingLeft: 16, justifyContent: 'center' }}>
                                            <Placeholder.Paragraph
                                                lineNumber={3}
                                                textSize={16}
                                                lineSpacing={5}
                                                color="#eee"
                                                width="100%"
                                                lastLineWidth="70%"
                                                firstLineWidth="50%"
                                                animate="fade"
                                                />
                                        </View>
                                        <View style={{ width: '30%', justifyContent: 'center', paddingLeft: 20 }}>
                                            <Placeholder.Line
                                                color="#eee"
                                                width="70%"
                                                animate="fade"
                                                textSize={22}
                                                />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 13 }}>
                                        <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                            <Placeholder.Media
                                                size={70}
                                                color="#eee"
                                                animate="fade"
                                                />
                                        </View>
                                        <View style={{ width: '50%', paddingLeft: 16, justifyContent: 'center' }}>
                                            <Placeholder.Paragraph
                                                lineNumber={3}
                                                textSize={16}
                                                lineSpacing={5}
                                                color="#eee"
                                                width="100%"
                                                lastLineWidth="70%"
                                                firstLineWidth="50%"
                                                animate="fade"
                                                />
                                        </View>
                                        <View style={{ width: '30%', justifyContent: 'center', paddingLeft: 20 }}>
                                            <Placeholder.Line
                                                color="#eee"
                                                width="70%"
                                                animate="fade"
                                                textSize={22}
                                                />
                                        </View>
                                    </View>
                                </View> }
                            {isNull && 
                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                    <FontAwesome5 name="search" size={14} style={{ marginBottom: 10 }} />
                                    <Text small style={{ color: '#818182'}}>Data Not Found</Text>
                                </View>}
                        </View>
                    )
                }}
                renderItem={({item, index}) => {
                    return (
                        <TouchableOpacity
                            key={index}
                            onPress={() => {}}
                            style={[styles.list, { padding: 0 }]}>
                            <View style={{ width: '20%', height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#eee' }}>
                                <Ionicon name="md-car" size={32} color="#777" />
                            </View>
                            <View style={{ width: '50%', paddingLeft: 16 }}>
                                <Text small style={styles.listDateSectionText}>{moment(item.CreatedDate).format('lll')}</Text>
                                <Text h5 style={{ color: '#155724' }}>{item.LeadCarBrand} {item.LeadCarModel}</Text>
                                <Text bold style={{ lineHeight: 19, color: GREY_800 }}>{item.LeadReferenceCode}</Text>
                            </View>
                            <View style={{ width: '30%' }}>
                                <View style={styles.listStatusSection}>
                                    <Text small bold style={{ color: GREY_700 }}>{item.LeadStatusExt}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
                keyExtractor={(item, index) => item.LeadReferenceCode }
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={() => {
                            if(this.props.status === "process") this.props.getTeamListLeadsProcess(this.props.memberCode);
                            if(this.props.status === "close") { this.props.getTeamListLeadClose(this.props.memberCode); }
                        }}
                    />
                }
            />
            {/* <Filter title={"Filter"} /> */}
        </View>
        );
    }

}

const mapStateToProps = ({ teamReducer }) => {
    const { loading, leadClose, leadProcess } = teamReducer;
    return { loading, leadClose, leadProcess }
}

export default connect(mapStateToProps, {
    getTeamListLeadsProcess,
    getTeamListLeadClose
})(DetailTimLeads);