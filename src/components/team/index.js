/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-14 10:18:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-06 15:11:03
 */
import React, {Component} from 'react';
import {ScrollView, View, Keyboard, TouchableOpacity, AsyncStorage, FlatList, RefreshControl, ActivityIndicator} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as _ from 'lodash';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';
import 'moment/locale/id';
import {Text, BlockHeader } from '../component';
import styles from './styles';
import globalStyles from '../../assets/styles';
import { getInsentive, getTeamDownline, getCheckToken } from '../../actions'
// import currency from '../../libraries/currency'
import {GREY_700, GREY_100, WHITE} from '../../libraries/colors';

type Props = {};
type State = {};

class Team extends Component<Props, State> {

    state = {
        insentive: 0,
        dataSource: [],
        isNull: false,
        refreshing: false
    }

    componentDidMount() {
        this.requestInsentiveAndDownline();
    }

    async requestInsentiveAndDownline() {
        let getMember = await AsyncStorage.getItem('@global:member');
        let member = JSON.parse(getMember);
        this.props.getInsentive(member.MemberCode);
        this.props.getTeamDownline(member.MemberCode);
        this.props.getCheckToken();
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.insentive !== prevProps.insentive) {
            return { insentive: this.props.insentive }
        }
        if(this.props.team !== prevProps.team) {
            return { team: this.props.team }
        }
        if(this.props.check !== prevProps.check) {
            return { check: this.props.check }
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.insentive !== undefined) {
                if(snapshot.insentive.status) {
                    this.setState({ insentive: snapshot.insentive.data });
                }
            }
            if(snapshot.team !== undefined) {
                if(snapshot.team.status) {
                    this.setState({
                        dataSource: snapshot.team.data,
                        isNull: snapshot.team.data.length > 0 ? false : true
                    })
                }
            }
            if(snapshot.check !== undefined) {
                if(snapshot.check !== null) {
                    if(snapshot.check.status) {
                        if(snapshot.check.data.invalid) {
                            AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                            Actions.reset('login', { invalid: true });
                        }
                    } else {
                        AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                        Actions.reset('login', { session: true });
                    }
                } else {
                    AsyncStorage.multiRemove(['@global:token', '@global:member', '@global:isLogin', '@global:detailMember']);
                    Actions.reset('login', { session: true });
                }
            }
        }
    }

    render() {
        const { loading } = this.props;
        const { dataSource, refreshing, isNull, insentive } = this.state;
        return (
            <View>
                <FlatList 
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={dataSource}
                    stickyHeaderIndices={[0]}
                    ListHeaderComponent={() => {
                        return (
                            <View>
                                <View style={{ width: '100%', height: 150, borderBottomColor: GREY_100, borderBottomWidth: 1, backgroundColor: WHITE, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text>Total Insentif s/d {moment().format('LL')}</Text>
                                    <Text h2 style={{ color: '#155724' }}>{`Rp. ${insentive}`}</Text>
                                </View>
                                <View style={{ width: '100%', height: 60, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ width: '15%', height: '100%', backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text>Level</Text>
                                    </View>
                                    <View style={{ width: '55%', paddingLeft: 16, backgroundColor: '#eee', justifyContent: 'center' }}>
                                        <Text>Member Name</Text>
                                    </View>
                                    <View style={{ width: '15%', height: '100%', backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text>Proses</Text>
                                    </View>
                                    <View style={{ width: '15%', height: '100%', backgroundColor: '#eee', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text>Selesai</Text>
                                    </View>
                                </View>
                            </View>
                        )
                    }}
                    ListEmptyComponent={() => {
                        return (
                            <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 13 }}>
                                {loading &&
                                    <View style={{ width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                                        <ActivityIndicator color={'#6c757d'} />
                                        <Text small style={{ color: '#818182'}}>Loading Data</Text>
                                    </View> 
                                }
                                {isNull && 
                                    <View style={{ width: '100%', height: 60, justifyContent: 'center', alignItems: 'center', borderBottomColor: GREY_100, borderBottomWidth: 1,  }}>
                                        <FontAwesome5 name="search" size={14} />
                                        <Text small style={{ color: '#818182'}}>Data Not Found</Text>
                                    </View>
                                }
                            </View>
                        )
                    }}
                    renderItem={({item, index}) => (
                        <View key={index} style={[styles.list, { height: 60, padding: 0 }]}>
                            <View style={{ width: '15%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                <Text>{index+1}</Text>
                            </View>
                            <View style={{ width: '55%', paddingLeft: 16 }}>
                                <Text>{item.membername}</Text>
                            </View>
                            <TouchableOpacity onPress={() => Actions.jump('detailTeamLeads', {title: "Team Leads Process", status: "process", memberCode: item.membercode })} style={{ width: '15%', height: '100%', backgroundColor: '#fff3cd', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#856404' }}>{item.memberleadprocess}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.jump('detailTeamLeads', {title: "Team Leads Close", status: "close", memberCode: item.membercode})} style={{ width: '15%', height: '100%', backgroundColor: '#d4edda', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#155724' }}>{item.memberleadclosed}</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                    keyExtractor={(item, index) => item.membercode }
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={() => this.requestInsentiveAndDownline() }
                        />
                    }
                />
            </View>
        )
    }
}

const mapStateToProps = ({ teamReducer, checkTokenReducer }) => {
    const { check } = checkTokenReducer;
    const { loading, insentive, team } = teamReducer;
    return { loading, insentive, team, check };
}

export default connect(mapStateToProps, {
    getInsentive,
    getTeamDownline,
    getCheckToken,
})(Team);