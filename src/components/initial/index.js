/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-08 21:17:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-09 14:22:29
 */
import React from 'react';
import Onboarding from 'react-native-onboarding-swiper';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import {PRIMARY_COLOR, AMBER_500, ORANGE_500, YELLOW_900, YELLOW_500, WHITE, BROWN_50, DEEP_ORANGE_50, LIGHT_GREEN_50, GREY_50, PRIMARY_COLOR_DARK} from '../../libraries/colors';
import {Button, Text} from '../component'

const Initial = () => (
    <Onboarding 
        showDone={false}
        onSkip={() => Actions.jump('login')}
        pages={[
            {
                title: 'Welcome',
                subtitle: 'Welcome to Belimob',
                backgroundColor: '#eb7369',
                image: (
                    <FontAwesome5 name={'handshake'} size={100} color={WHITE} />
                )
            },
            {
                title: 'Join Us',
                subtitle: 'Become a Member',
                backgroundColor: '#47498f',
                image: (
                    <FontAwesome5 name={'user-check'} size={100} color={WHITE} />
                )
            },
            {
                title: 'Benefit',
                subtitle: 'Get big incentive',
                backgroundColor: '#363535',
                image: (
                    <FontAwesome5 name={'hand-holding-usd'} size={100} color={WHITE} />
                )
            },
            {
                title: 'Enjoy the App',
                subtitle: (
                    <Button onPress={() => Actions.jump('login')} style={{ height: 50, width: 200, backgroundColor: PRIMARY_COLOR, borderRadius: 30, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: WHITE }}>Get Started</Text>
                    </Button>
                ),
                backgroundColor: WHITE,
                image: (
                    <FontAwesome5 name={'smile'} size={100} color={PRIMARY_COLOR_DARK} />
                )
            }
        ]}
    />
);

export default Initial;