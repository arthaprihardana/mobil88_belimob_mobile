/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-13 09:50:06 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2019-01-10 15:26:12
 */
// GLOBAL STORAGE
// member - @global:member
// detail member - @global:detailMember
// token - @global:token
// isLogin - @global:isLogin

// TEMPORARY STORAGE
// user register - @temporary:userRegister
// register - @temporary:register

/**
 * @constant API_URL
 * @description Global konstanta untuk path api url
 */
const API_URL = 'https://new-m88inventoryappdev.azurewebsites.net/api'; // DEVELOPMENT
// const API_URL = 'https://m88-apim.azure-api.net/belimob';    // PRODUCTION

export const APIM_KEY = '75092bfee75744eeb5f8be1abf408ee3';

// API MASTER
export const CAR_BRAND = API_URL + '/master/carbrand';
export const MEMBER_POSITION = API_URL + '/master/memberposition';
export const API_GET_BANK = API_URL + '/master/bank';
export const API_CAR_MODEL = API_URL + '/master/carmodel';
export const API_CAR_VARIANT = API_URL + '/master/carvariant';
export const API_CAR_TRANSMISSION = API_URL + '/master/cartransmission';
export const API_CAR_COLOR = API_URL + '/master/carcolour';
export const API_CAR_PREFIX_PLATE = API_URL + '/master/carprefixplate';
export const API_MRP = API_URL + '/master/mrp';
export const API_MRP_DESCRIPTION = API_URL + '/master/mrpdescription';
export const API_LEADS_REASON = API_URL + '/master/leadreason';
export const API_PROVINCE = API_URL + '/master/province';
export const API_CITY = API_URL + '/master/city';
export const API_DISTRICT = API_URL + '/master/district';
export const API_REGION = API_URL + '/master/region';
export const API_LEAD_STATUS = API_URL + '/master/leadstatus';

// API AUTH
export const REGISTER_MEMBER = API_URL + '/member/register';
export const REGISTER_PROFILE = API_URL + '/member/registerprofile';
export const LOGIN_MEMBER = API_URL + '/member/login';
export const LOGOUT_MEMBER = API_URL + '/data/member/logout';
export const LUPA_PASSWORD = API_URL + '/changepassword/checkmail';
export const RESET_PASSWORD = API_URL + '/changepassword/proses';
export const CHECK_TOKEN = API_URL + '/member/checktoken';
export const REFRESH_TOKEN = API_URL + '/data/member/refreshtoken';

// API MEMBER
export const MEMBER = API_URL + '/data/member';

// API LEADS
export const API_LEADS = API_URL + '/data/lead';

// API TEAM
export const API_TEAM = API_URL + '/data/team';
