/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 12:04:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-19 10:43:33
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_CITY } from '../constant';
import { getCitySuccess, getCityFailure } from '../actions';
import { GET_CITY } from '../actions/type';

const getCityRequest = async (ProvinsiId) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_CITY}/${ProvinsiId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
    .then(response => response)
    .catch(error => error)
}

function* getCityFromServer({ payload }) {
    const ProvinsiId = payload;
    try {
        let response = yield call(getCityRequest, ProvinsiId);
        yield put(getCitySuccess(response))
    } catch (error) {
        yield put(getCityFailure(error))
    }
}

export function* getCitySaga() {
    yield takeEvery(GET_CITY, getCityFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCitySaga)
    ])
}