/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 16:56:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 17:01:41
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_CAR_VARIANT } from '../constant';
import { getCarvariantSuccess, getCarVariantFailure } from '../actions';
import { GET_CAR_VARIANT } from '../actions/type';

const getCarVariantRequest = async (carModelId) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_CAR_VARIANT}/${carModelId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarVariantFromServer({ payload }) {
    const carModelId = payload;
    try {
        let response = yield call(getCarVariantRequest, carModelId);
        if(response.status) {
            yield put(getCarvariantSuccess(response))
        } else {
            yield put(getCarVariantFailure(response.message || response.errorMsg))
        }
    } catch (error) {
        yield put(getCarVariantFailure(error));
    }
}

export function* getCarVariantSaga() {
    yield takeEvery(GET_CAR_VARIANT, getCarVariantFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getCarVariantSaga)
    ])
}