/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 11:47:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-04 11:52:00
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_CAR_PREFIX_PLATE } from '../constant';
import { getCarPrefixPlateSuccess, getCarPrefixPlateFailure } from '../actions';
import { GET_CAR_PREFIX_PLATE } from '../actions/type';

const getCarPrefixRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(API_CAR_PREFIX_PLATE, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarPrefixPlateFromServer() {
    try {
        let response = yield call(getCarPrefixRequest);
        yield put(getCarPrefixPlateSuccess(response));
    } catch (error) {
        yield put(getCarPrefixPlateFailure(error));
    }
}

export function* getCarPrefixPlateSaga() {
    yield takeEvery(GET_CAR_PREFIX_PLATE, getCarPrefixPlateFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCarPrefixPlateSaga)
    ])
};