/**
 * @author: Artha Prihardana 
 * @Date: 2018-11-06 10:52:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-06 15:11:21
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { CHECK_TOKEN, REFRESH_TOKEN } from '../constant';
import { getCheckTokenSuccess, getCheckTokenFailure, updateRefreshTokenSuccess, updateRefreshTokenFailure } from '../actions';
import { GET_CHECK_TOKEN, UPDATE_REFRESH_TOKEN } from '../actions/type';
import moment from 'moment';
import 'moment/locale/id';

const getCheckTokenRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${CHECK_TOKEN}/${token}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

const updateRefreshTokenRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    let member = await AsyncStorage.getItem('@global:member');
    let jsonMember = JSON.parse(member);
    return await fetchWithTimeout(`${REFRESH_TOKEN}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify({
            memberid: jsonMember.MemberId
        })
    })
    .then(response => response)
    .catch(error => error);
}

function* getCheckTokenFromServer() {
    try {
        let response = yield call(getCheckTokenRequest);
        if(response.status) {
            let expDate = moment(response.data.ExpiredDate).format('YYYY-MM-DD HH:mm:ss');
            let expDate10 = moment(expDate).subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss');
            if(moment().isBetween(expDate10, expDate)) {
                let requestRefreshToken = yield call(updateRefreshTokenRequest);
                AsyncStorage.setItem('@global:token', requestRefreshToken.data.DeviceToken);
                yield put(getCheckTokenSuccess(requestRefreshToken));
            } else {
                yield put(getCheckTokenSuccess(response));
            }
        } else {
            yield put(getCheckTokenSuccess(response));
        }
    } catch (error) {
        yield put(getCheckTokenFailure(error));
    }
}

export function* getCheckToken() {
    yield takeEvery(GET_CHECK_TOKEN, getCheckTokenFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getCheckToken)
    ])
}