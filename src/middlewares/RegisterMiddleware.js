/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 14:33:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-21 14:16:49
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import fetchWithTimeout from '../actions/fetch';
import {REGISTER_MEMBER, REGISTER_PROFILE} from '../constant';
import {GET_REGISTER, GET_DETAIL_REGISTER} from '../actions/type';
import { getRegisterSuccess, getRegisterFailure, getDetailRegisterSuccess, getDetailRegisterFailure} from '../actions';

const getRegisterRequest = async (user_register) => {
    return fetchWithTimeout(REGISTER_MEMBER, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user_register)
    })
    .then(response => response)
    .catch(error => error)
}

function* getRegisterToServer({ payload }) {
    try {
        const response = yield call(getRegisterRequest, payload);
        yield put(getRegisterSuccess(response));
    } catch (error) {
        yield put(getRegisterFailure(error))
    }
}

export function* getRegisterSaga() {
    yield takeEvery(GET_REGISTER, getRegisterToServer);
}

const getDetailRegisterRequest = async (user_register) => {
    return fetchWithTimeout(REGISTER_PROFILE, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user_register)
    })
    .then(response => response)
    .catch(error => error);
}

function* getDetailRegisterToServer({ payload }) {
    try {
        const response = yield call(getDetailRegisterRequest, payload);
        yield put(getDetailRegisterSuccess(response));
    } catch (error) {
        yield put(getDetailRegisterFailure(error));
    }
}

export function* getDetailRegisterSaga() {
    yield takeEvery(GET_DETAIL_REGISTER, getDetailRegisterToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getRegisterSaga),
        fork(getDetailRegisterSaga)
    ])
}