/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 11:11:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-02 15:59:02
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {getLoginSuccess, getLoginFailure, getLogoutSuccess, getLogoutFailure} from '../actions/loginActions';
import {GET_LOGIN, GET_LOGOUT} from '../actions/type';
import {LOGIN_MEMBER, LOGOUT_MEMBER} from '../constant';

const getLoginRequest = async (user_data) => {
    return fetchWithTimeout(LOGIN_MEMBER, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: user_data.email,
            password: user_data.password
        })
    })
    .then(response => response)
    .catch(error => error);
}

function* getLoginToServer({ payload }) {
    try {
        const response = yield call(getLoginRequest, payload);
        yield put(getLoginSuccess(response));
    } catch (error) {
        yield put(getLoginFailure(error));
    }
}

export function* getLoginSaga() {
    yield takeEvery(GET_LOGIN, getLoginToServer);
}

const getLogoutRequest = async (FormData) => {
    let token = await AsyncStorage.getItem('@global:token');
    return fetchWithTimeout(LOGOUT_MEMBER, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* getLogoutToServer({ payload }) {
    try {
        let FormData = payload;
        let response = yield call(getLogoutRequest, FormData);
        yield put(getLogoutSuccess(response));
    } catch (error) {
        yield put(getLogoutFailure(error));
    }
}

export function* getLogoutSaga() {
    yield takeEvery(GET_LOGOUT, getLogoutToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getLoginSaga),
        fork(getLogoutSaga)
    ])
};