/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-01 23:15:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-01 23:19:14
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_REGION } from '../constant';
import { getRegionSuccess, getRegionFailure } from '../actions';
import { GET_REGION } from '../actions/type';

const getRegionRequest = async () => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(API_REGION, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getRegionToServer({ payload }) {
    try {
        let response = yield call(getRegionRequest);
        yield put(getRegionSuccess(response));
    } catch (error) {
        yield put(getRegionFailure(error));
    }
}

export function* getRegionSaga() {
    yield takeEvery(GET_REGION, getRegionToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getRegionSaga)
    ])
}