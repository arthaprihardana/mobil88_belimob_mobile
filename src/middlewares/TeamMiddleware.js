/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-25 14:45:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 10:52:38
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_TEAM } from '../constant';
import { getInsentiveSuccess, getInsentiveFailure, getTeamDownlineSuccess, getTeamDownlineFailure, getTeamListLeadCloseSuccess, getTeamListLeadCloseFailure, getTeamListLeadsProcessSuccess, getTeamListLeadsProcessFailure } from '../actions';
import { GET_INSENTIVE, GET_TEAM_DOWNLINE, GET_TEAM_LIST_LEAD_CLOSE, GET_TEAM_LIST_LEAD_PROCESS } from '../actions/type';

const getInsentiveRequest = async (MemberCode) => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_TEAM}/incentive/${MemberCode}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getInsentiveFromServer({ payload }) {
    try {
        const MemberCode = payload;
        let response = yield call(getInsentiveRequest, MemberCode);
        yield put(getInsentiveSuccess(response));
    } catch (error) {
        yield put(getInsentiveFailure(error))
    }
}

export function* getInsentiveSaga() {
    yield takeEvery(GET_INSENTIVE, getInsentiveFromServer);
}

const getTeamDownlineRequest = async (MemberCode) => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_TEAM}/downline/${MemberCode}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getTeamDownlineFromServer({ payload }) {
    try {
        let MemberCode = payload;
        let response = yield call(getTeamDownlineRequest, MemberCode);
        yield put(getTeamDownlineSuccess(response));
    } catch (error) {
        yield put(getTeamDownlineFailure(error));
    }
}

export function* getTeamDownlineSaga() {
    yield takeEvery(GET_TEAM_DOWNLINE, getTeamDownlineFromServer);
}

const getTeamListLeadCloseRequest = async (MemberCode) => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_TEAM}/listlead/closed/${MemberCode}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getTeamListLeadCloseFromServer({ payload }) {
    try {
        let MemberCode = payload;
        let response = yield call(getTeamListLeadCloseRequest, MemberCode);
        yield put(getTeamListLeadCloseSuccess(response));
    } catch (error) {
        yield put(getTeamListLeadCloseFailure(error));
    }
}

export function* getTeamLeadCloseSaga() {
    yield takeEvery(GET_TEAM_LIST_LEAD_CLOSE, getTeamListLeadCloseFromServer)
}

const getTeamListLeadProcessRequest = async (MemberCode) => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_TEAM}/listlead/process/${MemberCode}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getTeamListLeadProcessFromServer({ payload }) {
    try {
        let MemberCode = payload;
        let response = yield call(getTeamListLeadProcessRequest, MemberCode);
        yield put(getTeamListLeadsProcessSuccess(response))
    } catch (error) {
        yield put(getTeamListLeadsProcessFailure(error));
    }
}

export function* getTeamListLeadSaga() {
    yield takeEvery(GET_TEAM_LIST_LEAD_PROCESS, getTeamListLeadProcessFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getInsentiveSaga),
        fork(getTeamDownlineSaga),
        fork(getTeamLeadCloseSaga),
        fork(getTeamListLeadSaga)
    ])
}