/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-24 11:45:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-24 13:41:28
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_LEADS } from '../constant';
import { getDashboardSuccess, getDashboardFailure } from '../actions';
import { GET_DASHBOARD } from '../actions/type';

const getDashboardRequest = async (Params) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_LEADS}/dashboard/member/${Params.MemberCode}?m=${Params.Month}&y=${Params.Year}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDashboardFromServer({ payload }) {
    try {
        let Params = payload;
        let response = yield call(getDashboardRequest, Params);
        yield put(getDashboardSuccess(response));
    } catch (error) {
        yield put(getDashboardFailure(error));
    }
}

export function* getDashboard() {
    yield takeEvery(GET_DASHBOARD, getDashboardFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDashboard)
    ])
}