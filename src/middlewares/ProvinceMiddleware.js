/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 11:59:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-04 13:27:02
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_PROVINCE } from '../constant';
import { getProvinceSuccess, getProvinceFailure } from '../actions';
import { GET_PROVINCE } from '../actions/type';

const getProvinceRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(API_PROVINCE, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getProvinceFromServer() {
    try {
        let response = yield call(getProvinceRequest);
        yield put(getProvinceSuccess(response));
    } catch (error) {
        yield put(getProvinceFailure(error));
    }
}

export function* getProvinceSaga() {
    yield takeEvery(GET_PROVINCE, getProvinceFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getProvinceSaga)
    ])
}