/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 12:09:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-19 11:01:10
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {API_DISTRICT} from '../constant';
import { getDistrictSuccess, getDistrictFailure } from '../actions';
import { GET_DISTRICT } from '../actions/type';

const getDistrictRequest = async (CityId) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_DISTRICT}/${CityId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDistrictFromServer({ payload }) {
    const CityId = payload;
    try {
        let response = yield call(getDistrictRequest, CityId);
        yield put(getDistrictSuccess(response))
    } catch (error) {
        yield put(getDistrictFailure(error))
    }
}

export function* getDistrictSaga() {
    yield takeEvery(GET_DISTRICT, getDistrictFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getDistrictSaga)
    ])
}