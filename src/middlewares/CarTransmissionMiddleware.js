/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 17:04:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-17 13:43:30
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import {AsyncStorage} from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {API_CAR_TRANSMISSION} from '../constant';
import { getCarTransmissionSuccess, getCarTransmissionFailure } from '../actions';
import { GET_CAR_TRANSMISSION } from '../actions/type';

const getCarTransmissionRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(API_CAR_TRANSMISSION, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarTransmissionFromServer() {
    try {
        let response = yield call(getCarTransmissionRequest);
        if(response.status) {
            yield put(getCarTransmissionSuccess(response))
        } else {
            yield put(getCarTransmissionFailure(response.message || response.errorMsg))
        }
    } catch (error) {
        yield put(getCarTransmissionFailure(error))
    }
}

export function* getCarTransmissionSaga() {
    yield takeEvery(GET_CAR_TRANSMISSION, getCarTransmissionFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCarTransmissionSaga)
    ])
}