/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-24 10:02:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 14:59:26
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import _ from 'lodash';
import fetchWithTimeout from '../actions/fetch';
import { API_LEADS, API_LEAD_STATUS } from '../constant';
import { getLeadsSuccess, getLeadsFailure, getLeadsDetailSuccess, getLeadsDetailFailure, addLeadsSuccess, addLeadsFailure, insertDmsSuccess, insertDmsFailure, leadsNotAppointmentSuccess, leadsYesAppointmentSuccess, cancelLeadsSuccess, cancelLeadsFailure, getLeadStatusSuccess, getLeadStatusFailure } from '../actions';
import { GET_LEADS, GET_DETAIL_LEADS, ADD_LEADS, INSERT_DMS, LEAD_NOT_APPOINTMENT, LEAD_YES_APPOINTMENT, CANCEL_LEADS, GET_LEAD_STATUS } from '../actions/type';
import objectToQueryString from '../libraries/objectToQueryString';

const getLeadsRequest = async (params) => {
    let token = await AsyncStorage.getItem('@global:token');
    let MemberCode = params.MemberCode;
    let qs = _.omit(params, ['MemberCode']);
    let prm = objectToQueryString(qs);
    return await fetchWithTimeout(`${API_LEADS}/all/member/${MemberCode}?${prm}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getLeadsFromServer({ payload }) {
    try {
        let params = payload;
        let response = yield call(getLeadsRequest, params);
        yield put(getLeadsSuccess(response));
    } catch (error) {
        yield put(getLeadsFailure(error));
    }
}

export function* getLeadsSaga() {
    yield takeEvery(GET_LEADS, getLeadsFromServer);
}

const getLeadsDetailRequest = async (RefCode) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_LEADS}/detail/leadreference/${RefCode}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getLeadsDetailFromServer({ payload }) {
    try {
        let RefCode = payload;
        let response = yield call(getLeadsDetailRequest, RefCode);
        yield put(getLeadsDetailSuccess(response));
    } catch (error) {
        yield put(getLeadsDetailFailure(error));
    }
}

export function* getLeadsDetailSaga() {
    yield takeEvery(GET_DETAIL_LEADS, getLeadsDetailFromServer);
}

const addLeadsRequest = async(FormData) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_LEADS}/add`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error)
}

function* addLeadsToServer({ payload }) {
    try {
        let FormData = payload;
        let response = yield call(addLeadsRequest, FormData);
        yield put(addLeadsSuccess(response));
    } catch (error) {
        yield put(addLeadsFailure(error));
    }
}

export function* addLeadsSaga() {
    yield takeEvery(ADD_LEADS, addLeadsToServer)
}

const leadNoAppointmentRequest = async (FormData) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_LEADS}/appointment/not`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* leadNoAppointmentToServer({ payload }) {
    try {
        let FormData = payload;
        let response = yield call(leadNoAppointmentRequest, FormData);
        yield put(leadsNotAppointmentSuccess(response));
    } catch (error) {
        yield put(leadsNotAppointmentFailure(error))
    }
}

export function* leadNotAppointmentSaga() {
    yield takeEvery(LEAD_NOT_APPOINTMENT, leadNoAppointmentToServer);
}

const leadYesAppointmentRequest = async (FormData) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_LEADS}/appointment/yes`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* leadYesAppointmentToServer({ payload }) {
    try {
        let FormData = payload;
        let response = yield call(leadYesAppointmentRequest, FormData);
        yield put(leadsYesAppointmentSuccess(response));
    } catch (error) {
        yield put(leadsYesAppointmentFailure(error));
    }
}

export function* leadYesAppointmentSaga() {
    yield takeEvery(LEAD_YES_APPOINTMENT, leadYesAppointmentToServer);
}

const cancelLeadsRequest = async (FormData) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_LEADS}/update/status`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* cancelLeadsToServer({ payload }) {
    try {
        let FormData = payload;
        let response = yield call(cancelLeadsRequest, FormData);
        yield put(cancelLeadsSuccess(response));
    } catch (error) {
        yield put(cancelLeadsFailure(error));
    }
}

export function* cancelLeadsSaga() {
    yield takeEvery(CANCEL_LEADS, cancelLeadsToServer);
}

const getLeadStatusRequest = async () => {
    return await fetchWithTimeout(`${API_LEAD_STATUS}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* getLeadStatusFromServer() {
    try {
        let response = yield call(getLeadStatusRequest)
        yield put(getLeadStatusSuccess(response))
    } catch (error) {
        yield put(getLeadStatusFailure(error))
    }
}

export function* getLeadStatusSaga() {
    yield takeEvery(GET_LEAD_STATUS, getLeadStatusFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getLeadsSaga),
        fork(getLeadsDetailSaga),
        fork(addLeadsSaga),
        fork(leadNotAppointmentSaga),
        fork(leadYesAppointmentSaga),
        fork(cancelLeadsSaga),
        fork(getLeadStatusSaga)
    ])
}