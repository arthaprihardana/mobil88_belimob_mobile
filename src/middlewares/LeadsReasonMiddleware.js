/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 11:53:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-05 15:34:12
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import {AsyncStorage} from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_LEADS_REASON } from '../constant';
import { getLeadsReasonSuccess, getLeadsReasonFailure } from '../actions';
import {GET_LEADS_REASON} from '../actions/type';

const getLeadsReasonRequest = async () => {
    return await fetchWithTimeout(API_LEADS_REASON, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getLeadsReasonFromServer() {
    try {
        let response = yield call(getLeadsReasonRequest);
        yield put(getLeadsReasonSuccess(response));
    } catch (error) {
        yield put(getLeadsReasonFailure(error));
    }
}

export function* getLeadsReasonSaga() {
    yield takeEvery(GET_LEADS_REASON, getLeadsReasonFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getLeadsReasonSaga)
    ])
}