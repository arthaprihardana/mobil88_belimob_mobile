/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 13:51:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 14:49:28
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {MEMBER} from '../constant';
import {getMemberSuccess, getMemberFailure, updateMemberSuccess, updateMemberFailure, updatePasswordSuccess, updatePasswordFailure, updateFotoProfilSuccess, updateFotoProfilFailure, updatePembayaranSuccess, updatePembayaranFailure} from '../actions/memberActions';
import {GET_MEMBER, UPDATE_MEMBER, UPDATE_PASSWORD, UPDATE_FOTO_PROFIL, UPDATE_PEMBAYARAN} from '../actions/type';

const getMemberRequest = async (member_code) => {
    let token = await AsyncStorage.getItem('@global:token');
    return fetchWithTimeout(`${MEMBER}/code/${member_code}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMemberFromServer({ payload }) {
    try {
        const response = yield call(getMemberRequest, payload);
        if(response.status) {
            yield put(getMemberSuccess(response));
        } else {
            yield put(getMemberFailure(response.message || response.errorMsg));
        }
    } catch (error) {
        yield put(getMemberFailure(error));
    }
}

export function* getMemberSaga() {
    yield takeEvery(GET_MEMBER, getMemberFromServer);
}

const updateMemberRequest = async (member) => {
    let token = await AsyncStorage.getItem('@global:token');
    return fetchWithTimeout(`${MEMBER}/profile/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(member)
    })
    .then(response => response)
    .catch(error => error);
}

function* updateMemberToServer({ payload }) {
    const member = payload;
    try {
        const response = yield call(updateMemberRequest, member);
        yield put(updateMemberSuccess(response));
    } catch (error) {
        yield put(updateMemberFailure(error));
    }
}

export function* updateMemberSaga() {
    yield takeEvery(UPDATE_MEMBER, updateMemberToServer);
}

const updatePasswordRequest = async (reset_password) => {
    let token = await AsyncStorage.getItem('@global:token');
    return fetchWithTimeout(`${MEMBER}/changepassword/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(reset_password)
    })
    .then(response => response)
    .catch(error => error);
}

function* updatePasswordToServer({ payload }) {
    const reset_password = payload;
    try {
        const response = yield call(updatePasswordRequest, reset_password);
        yield put(updatePasswordSuccess(response));
    } catch (error) {
        yield put(updatePasswordFailure(error));
    }
}

export function* updatePasswordSaga() {
    yield takeEvery(UPDATE_PASSWORD, updatePasswordToServer);
}

const updateFotoProfilRequest = async (foto) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${MEMBER}/profile/changeimgprofile`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(foto)
    })
    .then(response => response)
    .catch(error => error);
}

function* updateFotoProfilToServer({ payload }) {
    const foto = payload;
    try {
        const response = yield call(updateFotoProfilRequest, foto);
        yield put(updateFotoProfilSuccess(response));
    } catch (error) {
        yield put(updateFotoProfilFailure(error));
    }
}

export function* updateFotoProfilSaga() {
    yield takeEvery(UPDATE_FOTO_PROFIL, updateFotoProfilToServer);
}

const updatePembayaranRequest = async (pembayaran) => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${MEMBER}/changepayment/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(pembayaran)
    })
    .then(response => response)
    .catch(error => error)
}

function* updatePembayaranToServer({ payload }) {
    const pembayaran = payload;
    try {
        const response = yield call(updatePembayaranRequest, pembayaran);
        yield put(updatePembayaranSuccess(response))
        // if(response.success) {
        //     yield put(updatePembayaranSuccess(response))
        // } else {
        //     yield put(updatePembayaranFailure(response.message || response.errorMsg))
        // }
    } catch (error) {
        yield put(updatePembayaranFailure(error))
    }
}

export function* updatePembayranSaga() {
    yield takeEvery(UPDATE_PEMBAYARAN, updatePembayaranToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMemberSaga),
        fork(updateMemberSaga),
        fork(updatePasswordSaga),
        fork(updateFotoProfilSaga),
        fork(updatePembayranSaga)
    ])
}