/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 16:42:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 16:42:52
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {CAR_BRAND} from '../constant';
import { getCarBrandSuccess, getCarBrandFailure } from '../actions';
import {GET_CAR_BRAND} from '../actions/type';

const getCarBrandRequest = async() => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(CAR_BRAND, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarBrandFromServer() {
    try {
        let response = yield call(getCarBrandRequest);
        if(response.status) {
            yield put(getCarBrandSuccess(response))
        } else {
            yield put(getCarBrandFailure(response.message || response.errorMsg))
        }
    } catch (error) {
        yield put(getCarBrandFailure(error));
    }
}

export function* getCarBrandSaga() {
    yield takeEvery(GET_CAR_BRAND, getCarBrandFromServer);
}

export default function* rooSaga() {
    yield all([
        fork(getCarBrandSaga)
    ])
}