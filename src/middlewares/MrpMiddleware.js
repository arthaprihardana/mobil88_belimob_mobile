/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-02 10:12:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-09 14:27:11
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {API_MRP, API_MRP_DESCRIPTION} from '../constant';
import { getMrpSuccess, getMrpFailure, getMrpDescriptionSuccess, getMrpDescriptionFailure } from '../actions';
import { GET_MRP , GET_MRP_DESCRIPTION} from '../actions/type';
import objectToQueryString from '../libraries/objectToQueryString';

const getMrpRequest = async(params) => {
    const qs = objectToQueryString(params);
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_MRP}?${qs}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMrpFromServer({ payload }) {
    try {
        let params = payload;
        let response = yield call(getMrpRequest, params);
        yield put(getMrpSuccess(response));
    } catch (error) {
        yield put(getMrpFailure(error));
    }
}

export function* getMrpSaga() {
    yield takeEvery(GET_MRP, getMrpFromServer)
}

const getMrpDescriptionRequest = async () => {
    return await fetchWithTimeout(`${API_MRP_DESCRIPTION}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMrpDescriptionFromServer({ payload }) {
    try {
        let response = yield call(getMrpDescriptionRequest);
        yield put(getMrpDescriptionSuccess(response));
    } catch (error) {
        yield put(getMrpDescriptionFailure(error));
    }
}

export function* getMrpDescriptionSaga() {
    yield takeEvery(GET_MRP_DESCRIPTION, getMrpDescriptionFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMrpSaga),
        fork(getMrpDescriptionSaga)
    ])
}