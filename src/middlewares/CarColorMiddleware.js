/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 17:10:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-17 13:56:32
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { API_CAR_COLOR } from '../constant';
import { getCarColorSuccess, getCarColorFailure } from '../actions';
import { GET_CAR_COLOR } from '../actions/type';

const getCarColorRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(API_CAR_COLOR, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarColorFromServer() {
    try {
        let response = yield call(getCarColorRequest);
        if(response.status) {
            yield put(getCarColorSuccess(response))
        } else {
            yield put(getCarColorFailure(response.message || response.errorMsg))
        }
    } catch (error) {
        yield put(getCarColorFailure(error))
    }
}

export function* getCarColorSaga() {
    yield takeEvery(GET_CAR_COLOR, getCarColorFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCarColorSaga)
    ])
}