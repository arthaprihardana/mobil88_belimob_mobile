/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 10:00:48 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-30 10:00:48 
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import {getVerifikasiSuccess, getVerifikasiFailure} from '../actions/verifikasiActions';
import {GET_VERIFIKASI} from '../actions/type';

const getVerifikasiRequest = async (verifikasi) => {
    let insertVerifyCode = verifikasi.insertVerifyCode;
    let validateVerifiyCode = verifikasi.validateVerifiyCode;
    if(insertVerifyCode === validateVerifiyCode) {
        return true;
    } else {
        return false;
    }
}

function* getVerifikasiValidate({ payload }) {
    try {
        const response = yield call(getVerifikasiRequest, payload);
        if(response) {
            yield put(getVerifikasiSuccess(response));
        } else {
            yield put(getVerifikasiFailure(response));
        }
    } catch (error) {
        yield put(getVerifikasiFailure(error));
    }
}

export function* getVerifikasiSaga() {
    yield takeEvery(GET_VERIFIKASI, getVerifikasiValidate);
}

export default function* rootSaga() {
    yield all([
        fork(getVerifikasiSaga)
    ])
}