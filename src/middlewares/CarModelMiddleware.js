/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 16:49:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-17 11:33:55
 */
import { all, call, fork, put, takeEvery} from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {API_CAR_MODEL} from '../constant';
import { getCarModelSuccess, getCarModelFailure } from '../actions';
import { GET_CAR_MODEL } from '../actions/type';

const getCarModelRequest = async (carBrandId) => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(`${API_CAR_MODEL}/${carBrandId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getCarModelFromServer({ payload }) {
    const carBrandId = payload;
    try {
        let response = yield call(getCarModelRequest, carBrandId);
        if(response.status) {
            yield put(getCarModelSuccess(response))
        } else [
            yield put(getCarModelFailure(response.message || response.errorMsg))
        ]
    } catch (error) {
        yield put(getCarModelFailure(error))
    }
}

export function* getCarModelSaga() {
    yield takeEvery(GET_CAR_MODEL, getCarModelFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getCarModelSaga)
    ])
}