/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 10:36:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-01 15:11:09
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {GET_BANK} from '../actions/type';
import { getBankSuccess, getBankFailure } from '../actions';
import { API_GET_BANK } from '../constant';

const getBankRequest = async () => {
    const token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(API_GET_BANK, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getBankFromServer() {
    try {
        let response = yield call(getBankRequest);
        yield put(getBankSuccess(response));
    } catch (error) {
        yield put(getBankFailure(error));
    }
}

export function* getBankSaga() {
    yield takeEvery(GET_BANK, getBankFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getBankSaga)
    ])
}

