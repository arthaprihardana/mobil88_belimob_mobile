/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-10 13:26:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-10 14:24:41
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import { LUPA_PASSWORD, RESET_PASSWORD } from '../constant';
import { getLupaPasswordSuccess, getLupaPasswordFailure, getResetPasswordSuccess, getResetPasswordFailure } from '../actions';
import { GET_LUPA_PASSWORD, GET_RESET_PASSWORD } from '../actions/type';

const getLupaPasswordRequest = FormData => {
    return fetchWithTimeout(LUPA_PASSWORD, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* getLupaPasswordToServer({ payload }) {
    try {
        const response = yield call(getLupaPasswordRequest, payload);
        yield put(getLupaPasswordSuccess(response));
    } catch (error) {
        yield put(getLupaPasswordFailure(error));
    }
}

export function* getLupaPasswordSaga() {
    yield takeEvery(GET_LUPA_PASSWORD, getLupaPasswordToServer);
}

const getResetPasswordRequest = FormData => {
    return fetchWithTimeout(RESET_PASSWORD, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(FormData)
    })
    .then(response => response)
    .catch(error => error);
}

function* getResetPasswordToServer({ payload }) {
    try {
        const response = yield call(getResetPasswordRequest, payload);
        yield put(getResetPasswordSuccess(response));
    } catch (error) {
        yield put(getResetPasswordFailure(error));
    }
}

export function* getResetPasswordSaga() {
    yield takeEvery(GET_RESET_PASSWORD, getResetPasswordToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getLupaPasswordSaga),
        fork(getResetPasswordSaga)
    ])
};