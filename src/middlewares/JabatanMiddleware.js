/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-27 14:09:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 14:14:43
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import fetchWithTimeout from '../actions/fetch';
import {MEMBER_POSITION} from '../constant';
import { getJabatanSuccess, getJabatanFailure } from '../actions/jabatanActions';
import { GET_JABATAN } from '../actions/type';

const getJabatanRequest = async () => {
    let token = await AsyncStorage.getItem('@global:token');
    return await fetchWithTimeout(MEMBER_POSITION, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getJabatanFromServer({ payload }) {
    try {
        let response = yield call(getJabatanRequest);
        yield put(getJabatanSuccess(response));
    } catch (error) {
        yield put(getJabatanFailure(error))
    }
}

export function* getJabatanSaga() {
    yield takeEvery(GET_JABATAN, getJabatanFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getJabatanSaga)
    ])
}