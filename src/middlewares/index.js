/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 11:07:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 12:12:34
 */
import { all } from 'redux-saga/effects';
import Login from './LoginMiddleware';
import Register from './RegisterMiddleware';
import Verifikasi from './VerifikasiMiddleware';
import Member from './MemberMiddleware';
import LupaPassword from './LupaPasswordMiddleware';
import ChekToken from './CheckTokenMiddleware';

// MASTER
import Bank from './BankMiddleware';
import CarBrand from './CarBrandMiddleware';
import CarModel from './CarModelMiddleware';
import CarVariant from './CarVariantMiddleware';
import CarTransmision from './CarTransmissionMiddleware';
import CarColor from './CarColorMiddleware';
import CarPrefixPlate from './CarPrefixMiddleware';
import LeadsReason from './LeadsReasonMiddleware';
import Province from './ProvinceMiddleware';
import City from './CityMiddleware';
import District from './DistrictMiddleware';
import Jabatan from './JabatanMiddleware';
import Region from './RegionMiddleware'
import Mrp from './MrpMiddleware';

// DASHBOARD
import Dashboard from './DashboardMiddleware';

// LEADS
import Leads from './LeadsMiddleware';

// TEAMS
import Teams from './TeamMiddleware';

/**
 * Generate saga middleware
 * 
 * @generator
 * @function rootSaga
 * @export
 * @param {*} getState
 */
export default function* rootSaga(getState) {
    yield all([
        Login(),
        Register(),
        Verifikasi(),
        Member(),
        LupaPassword(),
        ChekToken(),
        // Master
        Bank(),
        CarBrand(),
        CarModel(),
        CarVariant(),
        CarTransmision(),
        CarColor(),
        CarPrefixPlate(),
        LeadsReason(),
        Province(),
        City(),
        District(),
        Leads(),
        Dashboard(),
        Teams(),
        Jabatan(),
        Region(),
        Mrp()
    ]);
}