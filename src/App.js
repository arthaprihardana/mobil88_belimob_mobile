/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-08 20:41:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-05 14:33:43
 */
import React, {Component} from 'react';
import {StatusBar, View, Platform, ToastAndroid, BackHandler, NetInfo, YellowBox} from 'react-native';
import CardStackStyleInterpolator from 'react-navigation/src/views/StackView/StackViewStyleInterpolator';
import {Scene, Router, Actions, Reducer, ActionConst, Overlay, Modal, Tabs, Drawer, Stack, Lightbox} from 'react-native-router-flux';
import {connect, Provider} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import configureStore from './store/configureStore';
const store = configureStore();
const mapStateToProps = state => ({
    state: state.nav,
});
const RouterWithRedux = connect(mapStateToProps)(Router);

import { WHITE, PRIMARY_COLOR_DARK, BLACK} from './libraries/colors';
import {FontFamily} from './libraries/fonts';
import {Button, Text} from './components/component';
import globalStyles from './assets/styles';
import NavBar from './components/navBar';

// pages
import SplasScreen from './components/splashscreen';
import Initial from './components/initial';
import Login from './components/login';
import Register from './components/register';
import DetailRegister from './components/register/detailRegister';
import Verifikasi from './components/verifikasi';
import LupaPassword from './components/lupapassword';
import ResetPassword from './components/lupapassword/resetPassword';
import Home from './components/home';
import Leads from './components/leads';
import FormLeads from './components/leads/formLeads';
import DetailLeads from './components/leads/detailLeads';
import CancelLeads from './components/leads/cancelLead';
import EstimasiHarga from './components/leads/estimasiHarga';
import JadwalPertemuan from './components/leads/jadwalPertemuan';
import Team from './components/team';
import TeamLeads from './components/team/timLeads';
import DetailTeamLeads from './components/team/detailTimLeads';
import Profil from './components/profil';
import AturProfil from './components/profil/aturProfil';
import AturKataSandi from './components/profil/aturKataSandi';
import AturPembayaran from './components/profil/aturPembayaran';
import GaleriCamera from './components/media/cameraRoll';
import Camera from './components/media/camera';

YellowBox.ignoreWarnings(['Warning: ...']);

/**
 * @description 
 * 
 * @constant reducerCreate
 * @param {any} params 
 * @return function
 */
const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        return defaultReducer(state, action);
    };
};

/**
 * @description Style default yang digunakan pada setial Scene
 * 
 * @constant getSceneStyle
 * @returns void
 */
const getSceneStyle = () => ({
    backgroundColor: '#FFFFFF',
    shadowOpacity: 1,
    shadowRadius: 3,
});

/**
 * @description fungsi yang digunakan untuk mengelola back handler (ANDROID)
 * 
 * @constant onBackPress
 * @returns void
 */
let exitCount = 0;
const onBackPress = () => {
    let route = Actions.state.routes;
    let topSection = route[route.length - 1];
    console.log('route[route.length - 1].routeName ==>', route[route.length - 1].routeName);
    switch (route[route.length - 1].routeName) {
        case "main":
            let tab = route[route.length - 1].routes[0].index;
            if(tab == 0) {
                exitCount++;
                ToastAndroid.showWithGravityAndOffset(
                    'Tekan lagi untuk keluar',
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    100
                );
                if(exitCount == 2) {
                    exitCount = 0
                    BackHandler.exitApp()
                }
            } else {
                exitCount = 0
                Actions.pop()
            }
            return true;
        case "login":
            BackHandler.exitApp();
            return true;
        case "estimasiHarga":
            return true;
        case "jadwalPertemuan":
            return true;
        default:
            Actions.pop();
            return true;
    }
};

type Props = {};
type State = {};
/**
 * @description Class App adalah base class Root dari aplikasi
 *
 * @export
 * @class App
 * @extends {Component<Props>}
 */
export default class App extends Component<Props, State> {

    componentDidMount() {
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
        });
        function handleFirstConnectivityChange(connectionInfo) {
            console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
            NetInfo.removeEventListener(
                'connectionChange',
                handleFirstConnectivityChange
            );
        }
        NetInfo.addEventListener(
            'connectionChange',
            handleFirstConnectivityChange
        );
    }
    
    
    /**
     * @method render
     *
     * @returns
     * @memberof App
     */
    render() {
        return (
        <View style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={PRIMARY_COLOR_DARK}
                barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
                networkActivityIndicatorVisible={true}
                />
            <Provider store={store}>
                <RouterWithRedux
                    createReducer={reducerCreate} 
                    getSceneStyle={getSceneStyle}
                    backAndroidHandler={onBackPress}>
                    <Stack key="root">
                        <Scene key="splashscreen" component={SplasScreen} hideNavBar hideTabBar initial />
                        <Scene key="initial" component={Initial} hideNavBar hideTabBar panHandlers={null} />
                        <Scene key="login" component={Login} hideNavBar hideTabBar panHandlers={null} />
                        <Scene key="register" component={Register} hideTabBar title={"Daftar"} titleStyle={[{ color: WHITE, fontWeight: "normal" }, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="detailregister" component={DetailRegister} hideTabBar title={"Lengkapi Pendaftaran"} titleStyle={[{ color: WHITE, fontWeight: "normal" }, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.popTo('login') } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="verifikasi" component={Verifikasi} hideTabBar title={"Verifikasi Email"} titleStyle={[{ color: WHITE, fontWeight: "normal" }, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="lupapassword" component={LupaPassword} hideTabBar title={"Lupa Kata Sandi"} titleStyle={[{ color: WHITE, fontWeight: "normal" }, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="resetpassword" component={ResetPassword} hideTabBar title={"Atur Ulang Kata Sandi"} titleStyle={[{ color: WHITE, fontWeight: "normal" }, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.popTo('login') } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="main" hideNavBar type={ActionConst.RESET} panHandlers={null}>
                            <Tabs
                                key="menu"
                                swipeEnabled={false}
                                tabBarComponent={ ({navigation}) => <NavBar selectedIndex={navigation.state.index} /> }
                                showLabel={false}
                                lazy={true}
                                tabBarPosition={'bottom'}>
                                    <Scene key="home" title="Home" component={Home} panHandlers={null} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} initial type={ActionConst.RESET} />
                                    <Scene key="leads" title="Leads" component={Leads} panHandlers={null} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} type={ActionConst.RESET} />
                                    <Scene key="team" title="Team" component={Team} panHandlers={null} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} type={ActionConst.RESET} />
                                    <Scene key="profil" title="Profil" component={Profil} panHandlers={null} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} type={ActionConst.RESET} />
                            </Tabs>
                        </Scene>
                        <Scene key="aturProfil" title="Atur Profil" hideNavBar={false} hideTabBar component={AturProfil} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="aturKataSandi" title="Atur Kata Sandi" hideNavBar={false} hideTabBar component={AturKataSandi} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="aturPembayaran" title="Atur Pembayaran" hideNavBar={false} hideTabBar component={AturPembayaran} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="galeriCamera" title="Galeri" hideNavBar={false} hideTabBar component={GaleriCamera} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="camera" title="Camera" hideNavBar={false} hideTabBar component={Camera} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={{ backgroundColor: BLACK, borderBottomWidth: 0 }} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="formLeads" title="Leads" hideNavBar={false} hideTabBar component={FormLeads} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="estimasiHarga" title="Harga Mobil" hideNavBar={false} hideTabBar component={EstimasiHarga} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => <View />} />
                        <Scene key="jadwalPertemuan" title="Jadwal Pertemuan" hideNavBar={false} hideTabBar component={JadwalPertemuan} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => <View />} />
                        <Scene key="detailLeads" title="Detail Leads" hideNavBar={false} hideTabBar component={DetailLeads} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="teamLeads" hideNavBar={false} hideTabBar component={TeamLeads} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="detailTeamLeads" hideNavBar={false} hideTabBar component={DetailTeamLeads} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                        <Scene key="cancelLeads" hideNavBar={false} hideTabBar component={CancelLeads} titleStyle={[globalStyles.navigationBarTitle, FontFamily]} navigationBarStyle={globalStyles.navigationBar} renderBackButton={() => (
                            <Button onPress={() => Actions.pop() } style={[globalStyles.buttonLink, { width: 56, height: 56, justifyContent: 'center', alignItems: 'center' }]}>
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} color={WHITE} size={24} />
                            </Button>
                        )} />
                    </Stack>
                </RouterWithRedux>
            </Provider>
        </View>
        );
    }
}