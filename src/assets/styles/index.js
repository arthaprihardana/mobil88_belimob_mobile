/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-10 09:24:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-27 13:43:45
 */
import { StyleSheet } from 'react-native';
import {PRIMARY_COLOR, WHITE, GREY_200, GREY_50, GREY_300, GREY_500, GREY_700, GREY_600, GREY_400, RED_500, RED_300} from '../../libraries/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    boxContainer: {
        paddingLeft: 16, 
        paddingRight: 16,
        paddingTop: 26,
        paddingBottom: 16
    },
    boxContent: {
        width: '100%', 
        backgroundColor: WHITE,
        marginBottom: 16,
        borderRadius: 5,
        elevation: 1,
        shadowOffset:{  width: 2,  height: 2  },
        shadowColor: '#eee',
        shadowOpacity: 1.0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonPrimary: {
        width: '100%', 
        height: 50, 
        // backgroundColor: PRIMARY_COLOR, 
        backgroundColor: '#dc3545',
        borderColor: '#bd2130',
        borderWidth: 1,
        justifyContent: 'center', 
        alignItems: 'center',
        borderRadius: 4
    },
    buttonPrimaryOutline: {
        width: '100%', 
        height: 50, 
        backgroundColor: WHITE, 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderWidth: 1, 
        borderColor: '#bd2130',
        borderRadius: 4,
        marginBottom: 10
    },
    buttonSecondary: {
        width: '100%', 
        height: 50, 
        backgroundColor: '#6c757d', 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderWidth: 1, 
        borderColor: '#6c757d',
        borderRadius: 4,
        marginBottom: 10
    },
    buttonLink: {
        justifyContent: 'center', 
        alignItems: 'center', 
        width: '100%'
    },
    buttonDisabled: {
        width: '100%', 
        height: 50, 
        backgroundColor: '#6c757d', 
        justifyContent: 'center', 
        alignItems: 'center',
        borderWidth: 1, 
        borderRadius: 4,
        borderColor: '#6c757d',
        opacity: 0.3
    },
    navigationBar: {
        // backgroundColor: PRIMARY_COLOR, 
        backgroundColor: '#dc3545',
        borderBottomWidth: 0, 
        elevation: 0.5
    },
    navigationBarTitle: {
        color: WHITE, 
        fontWeight: "normal"
    }
});

export default styles;