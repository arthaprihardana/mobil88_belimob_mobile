/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 13:56:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-17 13:52:43
 */
import {GET_CAR_COLOR, GET_CAR_COLOR_SUCCESS, GET_CAR_COLOR_FAILURE, OPEN_DIALOG_CAR_COLOR, CLOSE_DIALOG_CAR_COLOR} from '../actions/type';
const INIT_STATE = {
    loading: false,
    color: null,
    dialog: false
}
/**
 * @function CarColorReducer
 * @param {Object} state - State Data Master Car Color
 * @param {Object} action - Action Method Master Car Color
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAR_COLOR:
            return { ...state, loading: true }
        case GET_CAR_COLOR_SUCCESS:
            return { ...state, loading: false, color: action.payload }
        case GET_CAR_COLOR_FAILURE:
            return { ...state, loading: false, color: null }
        case OPEN_DIALOG_CAR_COLOR:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CAR_COLOR:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}