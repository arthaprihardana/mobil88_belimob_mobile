import { GET_DASHBOARD, GET_DASHBOARD_SUCCESS, GET_DASHBOARD_FAILURE, OPEN_DIALOG_MONTH, CLOSE_DIALOG_MONTH, OPEN_DIALOG_YEAR, CLOSE_DIALOG_YEAR, OPEN_DIALOG_MONTH_YEAR, CLOSE_DIALOG_MONTH_YEAR } from "../actions/type";

const INIT_STATE = {
    loading: false,
    dashboard: null,
    dialog: false,
    tipe: null,
    value: null
}
/**
 * @function DashboardReducer
 * @param {Object} state - State Data Dashboard
 * @param {Object} action - Action Method Dashboard
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DASHBOARD:
            return { ...state, loading: true }
        case GET_DASHBOARD_SUCCESS:
            return { ...state, loading: false, dashboard: action.payload }
        case GET_DASHBOARD_FAILURE:
            return { ...state, loading: false, dashboard: null }
        case OPEN_DIALOG_MONTH_YEAR:
            return { ...state, dialog: true, tipe: action.payload, value: null }
        case CLOSE_DIALOG_MONTH_YEAR:
            return { ...state, dialog: false, tipe: null, value: action.payload }
        default:
            return { ...state }
    }
}