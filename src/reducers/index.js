/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 07:38:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:53:59
 */
import {combineReducers} from 'redux';
import routes from './routes';

// Master
import bankReducer from './bankReducer';
import carBrandReducer from './carBrandReducer';
import carModelReducer from './carModelReducer';
import carVariantReducer from './carVariantReducer';
import carTransmissionReducer from './carTransmissionReducer';
import carColorReducer from './carColorReducer';
import carPrefixPlateReducer from './carPrefixPlateReducer';
import leadsReasonReducer from './leadsReasonReducer';
import provinceReducer from './provinceReducer';
import cityReducer from './cityReducer';
import districtReducer from './districtReducer';
import jabatanReducer from './jabatanReducer';
import regionReducer from './regionReducer';
import mrpReducer from './mrpReducer';

import takePictureReducer from './takePictureReducer';

import loginReducer from './loginReducer';
import registerReducer from './registerReducer';
import verifikasiReducer from './verifikasiReducer';
import cameraReducer from './cameraReducer';
import memberReducer from './memberReducer';
import lupaPasswordReducer from './lupaPasswordReducer';
import checkTokenReducer from './checkTokenReducer';

// DASSHBOARD
import dashboardReducer from './dashboardReducer';
// LEADS
import leadsReducer from './leadsReducer';
// TEAM
import teamReducer from './teamReducer';

// RESPONSE
import responseReducer from './responseReducer';

export default combineReducers({
    routes,
    // other reducers
    loginReducer,
    registerReducer,
    verifikasiReducer,
    cameraReducer,
    memberReducer,
    lupaPasswordReducer,
    checkTokenReducer,

    takePictureReducer,

    // Master
    bankReducer,
    carBrandReducer,
    carModelReducer,
    carVariantReducer,
    carTransmissionReducer,
    carColorReducer,
    carPrefixPlateReducer,
    leadsReasonReducer,
    provinceReducer,
    cityReducer,
    districtReducer,
    jabatanReducer,
    regionReducer,
    mrpReducer,
    
    // DASHBOARD
    dashboardReducer,
    // LEADS
    leadsReducer,
    // TEAM
    teamReducer,

    responseReducer
})