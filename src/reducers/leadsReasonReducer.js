/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 14:18:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:54:43
 */
import { GET_LEADS_REASON, GET_LEADS_REASON_SUCCESS, GET_LEADS_REASON_FAILURE, OPEN_DIALOG_LEADS_REASON, CLOSE_DIALOG_LEADS_REASON } from "../actions/type";

const INIT_STATE = {
    loading: false,
    reason: null,
    dialog: false
}
/**
 * @function LeadReasonReducer
 * @param {Object} state - State Data Master Lead Reason
 * @param {Object} action - Action Method Master Lead Reason
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_LEADS_REASON:
            return { ...state, loading: true }
        case GET_LEADS_REASON_SUCCESS:
            return { ...state, loading: false, reason: action.payload }
        case GET_LEADS_REASON_FAILURE:
            return { ...state, loading: false, reason: null }
        case OPEN_DIALOG_LEADS_REASON:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_LEADS_REASON:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}