/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 14:30:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:58:58
 */
import {GET_REGISTER, GET_REGISTER_FAILURE, GET_REGISTER_SUCCESS, GET_DETAIL_REGISTER, GET_DETAIL_REGISTER_SUCCESS, GET_DETAIL_REGISTER_FAILURE} from '../actions/type';
import { Alert } from 'react-native';

const INIT_STATE = {
    loading: false,
    register: null,
    registerDetail: null
};
/**
 * @function RegisterReducer
 * @param {Object} state - State Data Register
 * @param {Object} action - Action Method Register
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_REGISTER:
            return { ...state, loading: true }
        case GET_REGISTER_SUCCESS:
            return { ...state, loading: false, register: action.payload }
        case GET_REGISTER_FAILURE:
            return { ...state, loading: false, register: null }
        case GET_DETAIL_REGISTER:
            return { ...state, loading: true }
        case GET_DETAIL_REGISTER_SUCCESS:
            return { ...state, loading: false, registerDetail: action.payload }
        case GET_DETAIL_REGISTER_FAILURE:
            return { ...state, loading: false, registerDetail: action.payload }
        default:
            return { ...state }
    }
}