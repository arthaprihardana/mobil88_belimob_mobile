/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-10 11:39:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:57:28
 */
import { GET_LUPA_PASSWORD, GET_LUPA_PASSWORD_SUCCESS, GET_LUPA_PASSWORD_FAILURE, GET_RESET_PASSWORD, GET_RESET_PASSWORD_SUCCESS, GET_RESET_PASSWORD_FAILURE } from "../actions/type";

const INIT_STATE = {
    loading: false,
    lupaPassword: null,
    resetPassword: null
}
/**
 * @function LupaPasswordReducer
 * @param {Object} state - State Data Lupa Password
 * @param {Object} action - Action Method Lupa Password
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_LUPA_PASSWORD:
            return { ...state, loading: true }
        case GET_LUPA_PASSWORD_SUCCESS:
            return { ...state, loading: false, lupaPassword: action.payload }
        case GET_LUPA_PASSWORD_FAILURE:
            return { ...state, loading: false, lupasPassword: null }
        case GET_RESET_PASSWORD:
            return { ...state, loading: true }
        case GET_RESET_PASSWORD_SUCCESS:
            return { ...state, loading: false, resetPassword: action.payload }
        case GET_RESET_PASSWORD_FAILURE:
            return { ...state, loading: false, resetPassword: null }
        default:
            return { ...state }
    }
}