/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 13:33:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 10:26:48
 */
import { GET_CAR_TRANSMISSION, GET_CAR_TRANSMISSION_SUCCESS, GET_CAR_TRANSMISSION_FAILURE, OPEN_DIALOG_CAR_TRANSMISSION, CLOSE_DIALOG_CAR_TRANSMISSION } from "../actions/type";

const INIT_STATE = {
    loading: false,
    transmission: null,
    dialog: false
}
/**
 * @function CarTransmissionReducer
 * @param {Object} state - State Data Master Car Transmission
 * @param {Object} action - Action Method Master Car Transmission
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAR_TRANSMISSION:
            return { ...state, loading: true }
        case GET_CAR_TRANSMISSION_SUCCESS:
            return { ...state, loading: false, transmission: action.payload }
        case GET_CAR_TRANSMISSION_FAILURE:
            return { ...state, loading: false, transmission: null}
        case OPEN_DIALOG_CAR_TRANSMISSION:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CAR_TRANSMISSION:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}