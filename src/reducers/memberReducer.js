/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 10:35:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:57:52
 */
import { GET_MEMBER, GET_MEMBER_SUCCESS, GET_MEMBER_FAILURE, UPDATE_MEMBER, UPDATE_MEMBER_SUCCESS, UPDATE_MEMBER_FAILURE, UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAILURE, UPDATE_FOTO_PROFIL, UPDATE_FOTO_PROFIL_SUCCESS, UPDATE_FOTO_PROFILE_FAILURE, UPDATE_PEMBAYARAN, UPDATE_PEMBAYARAN_SUCCESS, UPDATE_PEMBAYARAN_FAILURE } from "../actions/type";

const INIT_STATE = {
    loading: false,
    member: null,
    updatemember: null,
    updatepassword: null,
    updatefotoprofil: null,
    updatepembayaran: null
}
/**
 * @function MemberReducer
 * @param {Object} state - State Data Member
 * @param {Object} action - Action Method Data Member
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MEMBER:
            return { ...state, loading: true }
        case GET_MEMBER_SUCCESS:
            return { ...state, loading: false, member: action.payload }
        case GET_MEMBER_FAILURE:
            return { ...state, loading: false, member: null }
        case UPDATE_MEMBER:
            return { ...state, loading: true }
        case UPDATE_MEMBER_SUCCESS:
            return { ...state, loading: false, updatemember: action.payload }
        case UPDATE_MEMBER_FAILURE:
            return { ...state, loading: false, updatemember: null }
        case UPDATE_PASSWORD:
            return { ...state, loading: true }
        case UPDATE_PASSWORD_SUCCESS:
            return { ...state, loading: false, updatepassword: action.payload }
        case UPDATE_PASSWORD_FAILURE:
            return { ...state, loading: false, updatepassword: null }
        case UPDATE_FOTO_PROFIL:
            return { ...state, loading: true }
        case UPDATE_FOTO_PROFIL_SUCCESS:
            return { ...state, loading: false, updatefotoprofil: action.payload }
        case UPDATE_FOTO_PROFILE_FAILURE:
            return { ...state, loading: false, updatefotoprofil: null }
        case UPDATE_PEMBAYARAN:
            return { ...state, loading: true }
        case UPDATE_PEMBAYARAN_SUCCESS:
            return { ...state, loading: false, updatepembayaran: action.payload }
        case UPDATE_PEMBAYARAN_FAILURE:
            return { ...state, loading: false, updatepembayaran: null }
        default:
            return { ...state }
    }
}