/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 14:44:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:52:54
 */
import { GET_CITY, GET_CITY_SUCCESS, GET_CITY_FAILURE, OPEN_DIALOG_CITY, CLOSE_DIALOG_CITY } from "../actions/type";

const INIT_STATE = {
    loading: false,
    city: null,
    dialog: false
}
/**
 * @function CityReducer
 * @param {Object} state - State Data Master City
 * @param {Object} action - Action Method Master City
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CITY:
            return { ...state, loading: true }
        case GET_CITY_SUCCESS:
            return { ...state, loading: false, city: action.payload }
        case GET_CITY_FAILURE:
            return { ...state, loading: false, city: null }
        case OPEN_DIALOG_CITY:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CITY:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}