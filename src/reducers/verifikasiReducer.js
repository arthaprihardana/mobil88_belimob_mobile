/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 09:54:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 12:00:38
 */
import {GET_VERIFIKASI, GET_VERIFIKASI_SUCCESS, GET_VERIFIKASI_FAILURE} from '../actions/type';
const INIT_STATE = {
    loading: false,
    response: null
};
/**
 * @function VerifikasiReducer
 * @param {Object} state - State Data Verifikasi
 * @param {Object} action - Action Method Verifikasi
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VERIFIKASI:
            return { ...state, loading: true }
        case GET_VERIFIKASI_SUCCESS:
            return { ...state, loading: false, response: action.payload }
        case GET_VERIFIKASI_FAILURE:
            return { ...state, loading: false, response: action.payload }
        default:
            return { ...state }
    }
}