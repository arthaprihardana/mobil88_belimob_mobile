/**
 * @author: Artha Prihardana 
 * @Date: 2018-11-06 10:36:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:52:28
 */
import { GET_CHECK_TOKEN, GET_CHECK_TOKEN_SUCCESS, GET_CHECK_TOKEN_FAILURE, UPDATE_REFRESH_TOKEN, UPDATE_REFRESH_TOKEN_SUCCESS, UPDATE_REFRESH_TOKEN_FAILURE } from "../actions/type";

const INIT_STATE = {
    loading: false,
    check: null,
    newToken: null
}
/**
 * @function CheckTokenReducer
 * @param {Object} state - State Data Check Token And Refresh Token
 * @param {Object} action - Action Method Check Token And Refresh Token
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CHECK_TOKEN:
            return { ...state, loading: true }
        case GET_CHECK_TOKEN_SUCCESS:
            return { ...state, loading: false, check: action.payload }
        case GET_CHECK_TOKEN_FAILURE:
            return { ...state, loading: false, check: null }
        case UPDATE_REFRESH_TOKEN:
            return { ...state, loading: true }
        case UPDATE_REFRESH_TOKEN_SUCCESS:
            return { ...state, loading: false, newToken: action.payload }
        case UPDATE_REFRESH_TOKEN_FAILURE:
            return { ...state, loading: false, newToken: null }
        default:
            return { ...state }
    }
}