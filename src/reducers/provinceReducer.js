/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 14:40:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:58:30
 */
import { GET_PROVINCE, GET_PROVINCE_SUCCESS, GET_PROVINCE_FAILURE, OPEN_DIALOG_PROVINCE, CLOSE_DIALOG_PROVINCE } from "../actions/type";

const INIT_STATE = {
    loading: false,
    province: null,
    dialog: false
}
/**
 * @function ProvinceReducer
 * @param {Object} state - State Data Master Province
 * @param {Object} action - Action Method Master Province
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PROVINCE:
            return { ...state, loading: true }
        case GET_PROVINCE_SUCCESS:
            return { ...state, loading: false, province: action.payload }
        case GET_PROVINCE_FAILURE:
            return { ...state, loading: false, province: null }
        case OPEN_DIALOG_PROVINCE:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_PROVINCE:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}