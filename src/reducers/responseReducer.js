/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-28 14:30:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:59:14
 */
import { OPEN_DIALOG_RESPONSE, CLOSE_DIALOG_RESPONSE } from "../actions/type";

const INIT_STATE = {
    dialog: false
}
/**
 * @function ResponseReducer
 * @param {Object} state - State Data Dialog Response
 * @param {Object} action - Action Method Dialog Response
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case OPEN_DIALOG_RESPONSE:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_RESPONSE:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}