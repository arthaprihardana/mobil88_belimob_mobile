/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 13:29:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 10:27:13
 */
import { GET_CAR_VARIANT, GET_CAR_VARIANT_SUCCESS, GET_CAR_VARIANT_FAILURE, OPEN_DIALOG_CAR_VARIANT, CLOSE_DIALOG_CAR_VARIANT } from "../actions/type";

const INIT_STATE = {
    loading: false,
    carVariant: null,
    dialog: false
}
/**
 * @function CarVehicleReducer
 * @param {Object} state - State Data Master Car Vehicle
 * @param {Object} action - Action Method Master Car Vehicle
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAR_VARIANT:
            return { ...state, loading: true }
        case GET_CAR_VARIANT_SUCCESS:
            return { ...state, loading: false, carVariant: action.payload }
        case GET_CAR_VARIANT_FAILURE:
            return { ...state, loading: false, carVariant: null }
        case OPEN_DIALOG_CAR_VARIANT:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CAR_VARIANT:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}