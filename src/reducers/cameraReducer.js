/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-30 11:05:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 11:10:40
 */
import {TAKE_PICTURE, TAKE_PICTURE_SUCCESS, TAKE_PICTURE_FAILURE} from '../actions/type';

const INIT_STATE = {
    loading: false,
    response: null
};
/**
 * @function CameraReducer
 * @param {Object} state - State Data Capture Camera
 * @param {Object} action - Action Method Capture Camera
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case TAKE_PICTURE:
            return { ...state, loading: true }
        case TAKE_PICTURE_SUCCESS:
            return { ...state, loading: false, response: action.payload }
        case TAKE_PICTURE_FAILURE:
            return { ...state, loading: false, response: action.payload }
        default:
            return { ...state }
    }
}