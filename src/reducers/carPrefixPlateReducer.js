/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 13:59:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 10:26:24
 */
import { GET_CAR_PREFIX_PLATE, GET_CAR_PREFIX_PLATE_SUCCESS, GET_CAR_PREFIX_PLATE_FAILURE, OPEN_DIALOG_CAR_PREFIX_PLATE, CLOSE_DIALOG_CAR_PREFIX_PLATE } from "../actions/type";

const INIT_STATE = {
    loading: false,
    prefixPlate: null,
    dialog: false
}
/**
 * @function CarPrefixPlateReducer
 * @param {Object} state - State Data Master Car Prefix Plate
 * @param {Object} action - Action Method Master Car Prefix Plate
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAR_PREFIX_PLATE:
            return { ...state, loading: true }
        case GET_CAR_PREFIX_PLATE_SUCCESS:
            return { ...state, loading: false, prefixPlate: action.payload }
        case GET_CAR_PREFIX_PLATE_FAILURE:
            return { ...state, loading: false, prefixPlate: null }
        case OPEN_DIALOG_CAR_PREFIX_PLATE:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CAR_PREFIX_PLATE:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}