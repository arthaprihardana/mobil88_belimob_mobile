/**
 * @author: Artha Prihardana 
 * @Date: 2018-11-08 10:25:42 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-11-08 10:25:42 
 */
import { GET_CAR_MODEL, GET_CAR_MODEL_SUCCESS, GET_CAR_MODEL_FAILURE, OPEN_DIALOG_CAR_MODEL, CLOSE_DIALOG_CAR_MODEL } from "../actions/type";

const INIT_STATE = {
    loading: false,
    carModel: null,
    dialog: false
};
/**
 * @function CarModelReducer
 * @param {Object} state - State Data Master Car Mode
 * @param {Object} action - Action Method Master Car Model
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAR_MODEL:
            return { ...state, loading: true }
        case GET_CAR_MODEL_SUCCESS:
            return { ...state, loading: false, carModel: action.payload }
        case GET_CAR_MODEL_FAILURE:
            return { ...state, loading: false, carModel: null }
        case OPEN_DIALOG_CAR_MODEL:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CAR_MODEL:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}