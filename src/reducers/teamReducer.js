/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-25 14:40:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 12:00:19
 */
import { GET_INSENTIVE, GET_INSENTIVE_SUCCESS, GET_INSENTIVE_FAILURE, GET_TEAM_DOWNLINE, GET_TEAM_DOWNLINE_SUCCESS, GET_TEAM_DOWNLINE_FAILURE, GET_TEAM_LIST_LEAD_CLOSE, GET_TEAM_LIST_LEAD_CLOSE_SUCCESS, GET_TEAM_LIST_LEAD_CLOSE_FAILURE, GET_TEAM_LIST_LEAD_PROCESS, GET_TEAM_LIST_LEAD_PROCESS_SUCCESS, GET_TEAM_LIST_LEAD_PROCESS_FAILURE } from "../actions/type";

const INIT_STATE = {
    loading: false,
    team: null,
    insentive: null,
    leadClose: null,
    leadProcess: null
};
/**
 * @function TeamReducer
 * @param {Object} state - State Data Team
 * @param {Object} action - Action Method Data Team
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_INSENTIVE:
            return { ...state, loading: true }
        case GET_INSENTIVE_SUCCESS:
            return { ...state, loading: false, insentive: action.payload }
        case GET_INSENTIVE_FAILURE:
            return { ...state, loading: false, insentive: null }
        case GET_TEAM_DOWNLINE:
            return { ...state, loading: true }
        case GET_TEAM_DOWNLINE_SUCCESS:
            return { ...state, loading: false, team: action.payload }
        case GET_TEAM_DOWNLINE_FAILURE:
            return { ...state, loading: false, team: null }
        case GET_TEAM_LIST_LEAD_CLOSE:
            return { ...state, loading: true }
        case GET_TEAM_LIST_LEAD_CLOSE_SUCCESS:
            return { ...state, loading: false, leadClose: action.payload, leadProcess: null }
        case GET_TEAM_LIST_LEAD_CLOSE_FAILURE:
            return { ...state, loading: false, leadClose: null, leadProcess: null }
        case GET_TEAM_LIST_LEAD_PROCESS:
            return { ...state, loading: true }
        case GET_TEAM_LIST_LEAD_PROCESS_SUCCESS:
            return { ...state, loading: false, leadProcess: action.payload, leadClose: null }
        case GET_TEAM_LIST_LEAD_PROCESS_FAILURE:
            return { ...state, loading: false, leadProcess: null, leadClose: null }
        default:
            return { ...state }
    }
}