/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-27 14:07:53 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:54:19
 */
import { GET_JABATAN, GET_JABATAN_SUCCESS, GET_JABATAN_FAILURE, OPEN_DIALOG_JABATAN, CLOSE_DIALOG_JABATAN } from "../actions/type";

const INIT_STATE = {
    loading: false,
    jabatan: null,
    dialog: false
}
/**
 * @function JabatanReducer
 * @param {Object} state - State Data Master Jabatan
 * @param {Object} action - Action Method Master Jabatan
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_JABATAN:
            return { ...state, loading: true }
        case GET_JABATAN_SUCCESS:
            return { ...state, loading: false, jabatan: action.payload }
        case GET_JABATAN_FAILURE:
            return { ...state, loading: false, jabatan: null }
        case OPEN_DIALOG_JABATAN:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_JABATAN:  
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}