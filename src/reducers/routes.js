/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 07:35:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-15 11:37:02
 */
import {ActionConst} from 'react-native-router-flux';

const initialState = {
    scene: {}
}
/**
 * @method reducer
 *
 * @export
 * @param {*} [state=initialState]
 * @param {*} [action={}]
 * @returns {Object} state
 * 
 * @description reduce state dan action
 */
export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case ActionConst.FOCUS:
            console.log('focus ==>', action);
            return {
                ...state,
                scene: action.scene,
            };
        case ActionConst.PUSH:
            console.log('push ==>', action);
            return {
                ...state,
                scene: action.scene
            }
        
        default:
            return state;
    }
}