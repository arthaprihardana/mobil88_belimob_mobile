/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 10:43:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:57:03
 */
import {GET_LOGIN, GET_LOGIN_FAILURE, GET_LOGIN_SUCCESS, GET_LOGOUT, GET_LOGOUT_SUCCESS, GET_LOGOUT_FAILURE} from '../actions/type';

const INIT_STATE = {
    loading: false,
    login: null,
    logout: null
}
/**
 * @function LoginReducer
 * @param {Object} state - State Data Login And Logout
 * @param {Object} action - Action Method Login And Logout
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_LOGIN:
            return { ...state, loading: true }
        case GET_LOGIN_SUCCESS:
            return { ...state, loading: false, login: action.payload }
        case GET_LOGIN_FAILURE:
            return { ...state, loading: false, login: action.payload }
        case GET_LOGOUT:
            return { ...state, loading: true }
        case GET_LOGOUT_SUCCESS:
            return { ...state, loading: false, logout: action.payload }
        case GET_LOGOUT_FAILURE:
            return { ...state, loading: false, logout: null }
        default:
            return { ...state }
    }
}