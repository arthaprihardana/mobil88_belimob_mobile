/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-27 14:52:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 12:00:01
 */
import { TAKE_PICTURE_PROFILE, TAKE_PICTURE_PROFILE_SUCCESS, TAKE_PICTURE_PROFILE_FAILURE, TAKE_PICTURE_KTP, TAKE_PICTURE_KTP_SUCCESS, TAKE_PICTURE_KTP_FAILURE, TAKE_PICTURE_CARD_ID, TAKE_PICTURE_CARD_ID_SUCCESS, TAKE_PICTURE_CARD_ID_FAILURE, OPEN_CAMERA, CLOSE_CAMERA, OPEN_CAMERA_ROLL, CLOSE_CAMERA_ROLL } from "../actions/type";
import { Actions } from "react-native-router-flux";

const INIT_STATE = {
    loading: false,
    cameraOpen: false,
    cameraRollOpen: false,
    type: null,
    pictureProfile: null,
    pictureKtp: null,
    pictureCardId: null,
}
/**
 * @function TakePictureReducer
 * @param {Object} state - State Data Take Picture
 * @param {Object} action - Action Method Take Picture
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case OPEN_CAMERA:
            return { ...state, cameraOpen: true, type: action.payload }
        case CLOSE_CAMERA:
            return { ...state, cameraOpen: false, type: null }
        case OPEN_CAMERA_ROLL:
            return { ...state, cameraRollOpen: true, type: action.payload }
        case CLOSE_CAMERA_ROLL: 
            return { ...state, cameraRollOpen: false, type: null }
        case TAKE_PICTURE_PROFILE:
            return { ...state, loading: true }
        case TAKE_PICTURE_PROFILE_SUCCESS:
            return { ...state, loading: false, pictureProfile: action.payload, cameraOpen: false, cameraRollOpen: false }
        case TAKE_PICTURE_PROFILE_FAILURE:
            return { ...state, loading: false, pictureProfile: null }
        case TAKE_PICTURE_KTP:
            return { ...state, loading: true }
        case TAKE_PICTURE_KTP_SUCCESS:
            return { ...state, loading: false, pictureKtp: action.payload, cameraOpen: false, cameraRollOpen: false }
        case TAKE_PICTURE_KTP_FAILURE:
            return { ...state, loading: false, pictureKtp: null }
        case TAKE_PICTURE_CARD_ID:
            return { ...state, loading: true }
        case TAKE_PICTURE_CARD_ID_SUCCESS:
            return { ...state, loading: false, pictureCardId: action.payload, cameraOpen: false, cameraRollOpen: false }
        case TAKE_PICTURE_CARD_ID_FAILURE:
            return { ...state, loading: false, pictureCardId: null}
        default:
            return { ...state }
    }
}