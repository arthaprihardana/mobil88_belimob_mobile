/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 10:43:43 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-01 14:39:42
 */
import { GET_BANK, GET_BANK_SUCCESS, GET_BANK_FAILURE, OPEN_DIALOG_BANK, CLOSE_DIALOG_BANK } from "../actions/type";

const INIT_STATE = {
    loading: false,
    bank: null,
    dialog: false
}
/**
 * @function BankReducer
 * @param {Object} state - State Data Master Bank
 * @param {Object} action - Action Method Master Bank
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BANK:
            return { ...state, loading: true }
        case GET_BANK_SUCCESS:
            return { ...state, loading: false, bank: action.payload }
        case GET_BANK_FAILURE:
            return { ...state, loading: false, bank: null }
        case OPEN_DIALOG_BANK:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_BANK:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}