/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 11:55:18 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-20 14:00:13
 */
import {GET_CAR_BRAND, GET_CAR_BRAND_SUCCESS, GET_CAR_BRAND_FAILURE, OPEN_DIALOG_CAR_BRAND, CLOSE_DIALOG_CAR_BRAND} from '../actions/type';

const INIT_STATE = {
    loading: false,
    carBrand: null,
    dialog: false
}
/**
 * @function CarBrandReducer
 * @param {Object} state - State Data Master Car Brand
 * @param {Object} action - Action Method Master Car Brand
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CAR_BRAND:
            return { ...state, loading: true }
        case GET_CAR_BRAND_SUCCESS:
            return { ...state, loading: false, carBrand: action.payload }
        case GET_CAR_BRAND_FAILURE:
            return { ...state, loading: false, carBrand: action.payload }
        case OPEN_DIALOG_CAR_BRAND:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_CAR_BRAND:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}