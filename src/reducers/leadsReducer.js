/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 10:27:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 14:43:07
 */
import { OPEN_DIALOG_FILTER, CLOSE_DIALOG_FILTER, OPEN_DIALOG_TAHUN_PABRIKAN, CLOSE_DIALOG_TAHUN_PABRIKAN, OPEN_DIALOG_FACELIFT_MODE, CLOSE_DIALOG_FACELIFT_MODE, OPEN_DIALOG_TIPE_STNK, CLOSE_DIALOG_TIPE_STNK, GET_LEADS, GET_LEADS_SUCCESS, GET_LEADS_FAILURE, GET_DASHBOARD, GET_DASHBOARD_SUCCESS, GET_DETAIL_LEADS, GET_DETAIL_LEADS_SUCCESS, GET_DETAIL_LEADS_FAILURE, ADD_LEADS, ADD_LEADS_SUCCESS, ADD_LEADS_FAILURE, INSERT_DMS, INSERT_DMS_SUCCESS, INSERT_DMS_FAILURE, LEAD_NOT_APPOINTMENT, LEAD_NOT_APPOINTMENT_FAILURE, LEAD_NOT_APPOINTMENT_SUCCESS, LEAD_YES_APPOINTMENT, LEAD_YES_APPOINTMENT_FAILURE , LEAD_YES_APPOINTMENT_SUCCESS, CANCEL_LEADS, CANCEL_LEADS_SUCCESS, CANCEL_LEADS_FAILURE, GET_LEAD_STATUS, GET_LEAD_STATUS_SUCCESS, GET_LEAD_STATUS_FAILURE, OPEN_DIALOG_LEAD_STATUS, CLOSE_DIALOG_LEAD_STATUS} from "../actions/type";

const INIT_STATE = {
    loading: false,
    dialog: false,
    dialogThn: false,
    dialogFc: false,
    dialogTs: false,
    leads: null,
    detailLeads: null,
    newLeads: null,
    noAppointment: null,
    yesAppointment: null,
    cancel: null,
    leadStatus: null,
    dialogLs: false
}
/**
 * @function Lead
 * @param {Object} state - State Data Lead
 * @param {Object} action - Action Method Lead
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case OPEN_DIALOG_FILTER:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_FILTER:
            return { ...state, dialog: false }
        case OPEN_DIALOG_TAHUN_PABRIKAN:
            return { ...state, dialogThn: true }
        case CLOSE_DIALOG_TAHUN_PABRIKAN:
            return { ...state, dialogThn: false }
        case OPEN_DIALOG_FACELIFT_MODE:
            return { ...state, dialogFc: true }
        case CLOSE_DIALOG_FACELIFT_MODE:
            return { ...state, dialogFc: false }
        case OPEN_DIALOG_TIPE_STNK:
            return { ...state, dialogTs: true }
        case CLOSE_DIALOG_TIPE_STNK:
            return { ...state, dialogTs: false }
        case GET_LEADS:
            return { ...state, loading: true }
        case GET_LEADS_SUCCESS:
            return { ...state, loading: false, leads: action.payload }
        case GET_LEADS_FAILURE:
            return { ...state, loading: false, leads: null }
        case GET_DETAIL_LEADS:
            return { ...state, loading: true }
        case GET_DETAIL_LEADS_SUCCESS:
            return { ...state, loading: false, detailLeads: action.payload }
        case GET_DETAIL_LEADS_FAILURE:
            return { ...state, loading: false, detailLeads: null }
        case ADD_LEADS:
            return { ...state, loading: true }
        case ADD_LEADS_SUCCESS:
            return { ...state, loading: false, newLeads: action.payload }
        case ADD_LEADS_FAILURE:
            return { ...state, loading: false, newLeads: null }
        case LEAD_NOT_APPOINTMENT:
            return { ...state, loading: true }
        case LEAD_NOT_APPOINTMENT_SUCCESS:
            return { ...state, loading: false, noAppointment: action.payload }
        case LEAD_NOT_APPOINTMENT_FAILURE:
            return { ...state, loading: false, noAppointment: null }
        case LEAD_YES_APPOINTMENT:
            return { ...state, loading: true }
        case LEAD_YES_APPOINTMENT_SUCCESS:
            return { ...state, loading: false, yesAppointment: action.payload }
        case LEAD_YES_APPOINTMENT_FAILURE:
            return { ...state, loading: false, yesAppointment: null}
        case CANCEL_LEADS:
            return { ...state, loading: true }
        case CANCEL_LEADS_SUCCESS:
            return { ...state, loading: false, cancel: action.payload }
        case CANCEL_LEADS_FAILURE:
            return { ...state, loading: false, cancel: null }
        case GET_LEAD_STATUS:
            return { ...state }
        case GET_LEAD_STATUS_SUCCESS:
            return { ...state, leadStatus: action.payload }
        case GET_LEAD_STATUS_FAILURE:
            return { ...state, leadStatus: null }
        case OPEN_DIALOG_LEAD_STATUS:
            return { ...state, dialogLs: true }
        case CLOSE_DIALOG_LEAD_STATUS:
            return { ...state, dialogLs: false }
        default:
            return { ...state }
    }
}