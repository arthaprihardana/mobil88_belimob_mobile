/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-01 23:12:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:58:43
 */
import { GET_REGION, GET_REGION_SUCCESS, GET_REGION_FAILURE, OPEN_DIALOG_REGION, CLOSE_DIALOG_REGION } from "../actions/type";

const INIT_STATE = {
    loading: false,
    region: null,
    dialog: false
}
/**
 * @function RegionReducer
 * @param {Object} state - State Data Master Region
 * @param {Object} action - Action Method Master Region
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_REGION:
            return { ...state, loading: true }
        case GET_REGION_SUCCESS:
            return { ...state, loading: false, region: action.payload }
        case GET_REGION_FAILURE:
            return { ...state, loading: false, region: null }
        case OPEN_DIALOG_REGION:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_REGION:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}