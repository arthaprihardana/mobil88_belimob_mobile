/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 14:47:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:53:40
 */
import { GET_DISTRICT, GET_DISTRICT_SUCCESS, GET_DISTRICT_FAILURE, OPEN_DIALOG_DISTRICT, CLOSE_DIALOG_DISTRICT } from "../actions/type";

const INIT_STATE = {
    loading: false,
    district: null,
    dialog: false
}
/**
 * @function DistrictReducer
 * @param {Object} state - State Data Master District
 * @param {Object} action - Action Method Master District
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DISTRICT:
            return { ...state, loading: true }
        case GET_DISTRICT_SUCCESS:
            return { ...state, loading: false, district: action.payload }
        case GET_DISTRICT_FAILURE:
            return { ...state, loading: false, district: null }
        case OPEN_DIALOG_DISTRICT:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_DISTRICT:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}