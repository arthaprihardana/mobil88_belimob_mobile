/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 14:13:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-08 11:58:14
 */
import { GET_MRP, GET_MRP_SUCCESS, GET_MRP_FAILURE, GET_MRP_DESCRIPTION, GET_MRP_DESCRIPTION_SUCCESS, GET_MRP_DESCRIPTION_FAILURE, OPEN_DIALOG_MRP_DESCRIPTION, CLOSE_DIALOG_MRP_DESCRIPTION } from "../actions/type";

const INIT_STATE = {
    loading: false,
    mrp: null,
    mrpdescription: null,
    dialog: false
}
/**
 * @function MrpReducer
 * @param {Object} state - State Data MRP
 * @param {Object} action - Action Method Data MRP
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MRP:
            return { ...state, loading: true }
        case GET_MRP_SUCCESS:
            return { ...state, loading: false, mrp: action.payload }
        case GET_MRP_FAILURE:
            return { ...state, loading: false, mrp: null }
        case GET_MRP_DESCRIPTION:
            return { ...state, loading: true }
        case GET_MRP_DESCRIPTION_SUCCESS:
            return { ...state, loading: false, mrpdescription: action.payload }
        case GET_MRP_DESCRIPTION_FAILURE:
            return { ...state, loading: false, mrpdescription: null }
        case OPEN_DIALOG_MRP_DESCRIPTION:
            return { ...state, dialog: true }
        case CLOSE_DIALOG_MRP_DESCRIPTION:
            return { ...state, dialog: false }
        default:
            return { ...state }
    }
}