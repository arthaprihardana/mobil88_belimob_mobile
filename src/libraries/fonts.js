/**
 * @author: Artha Prihardana 
 * @Date: 2018-05-13 09:35:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-09 11:21:07
 */
import { Platform } from 'react-native';

export const FontFamily = Platform.OS === 'ios' ? { fontFamily: 'Roboto-Regular' } : { fontFamily: "Roboto-Regular" }
export const FontFamilyBold = Platform.OS === 'ios' ? { fontFamily: 'Roboto-Bold' } : { fontFamily: "Roboto-Bold" }
export const FontFamilyLight = Platform.OS === 'ios' ? { fontFamily: 'Roboto-Light' } : { fontFamily: "Roboto-Light" }
export const FontFamilyBlack = Platform.OS === 'ios' ? { fontFamily: 'Roboto-Black' } : { fontFamily: "Roboto-Black" }
export const FontFamilyThin = Platform.OS === 'ios' ? { fontFamily: 'Roboto-Thin' } : { fontFamily: "Roboto-Thin" }