/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-26 13:48:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-26 13:51:57
 */
export default currency = (num,iscurr) => {
    let curr = '';
    if(typeof iscurr !== 'undefined' && iscurr === true){
        curr = 'Rp. ';
    }
    let str = '';
    let numrev = num.toString().split('').reverse().join('');
    for(let i = 0;i < numrev.length;i++){
        if(i%3 === 0){
            str += numrev.substr(i,3)+'.';
        }
    }
    return curr+str.split('',str.length-1).reverse().join('');
};