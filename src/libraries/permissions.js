/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-24 14:40:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-24 14:46:04
 */
import { PermissionsAndroid } from 'react-native';

export const requestCameraPermission = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
                'title': 'Belimob App Camera Permission',
                'message': 'Belimob App needs access to your Camera'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera")
        } else {
            console.log("Camera permission denied")
        }
    } catch (error) {
        console.log('error camera permission');
    }
}

export const requestReadStorage = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
                'title': 'Belimob App Read Storage Permission',
                'message': 'Belimob App needs access to Read Storage'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can read storage")
        } else {
            console.log("Read storage permission denied")
        }
    } catch (error) {
        console.log('error read storage permission');
    }
}