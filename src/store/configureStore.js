/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-09 07:39:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-29 11:11:28
 */
import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from "redux-saga";
import reducers from '../reducers/index';
import RootSaga from '../middlewares/index';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

/**
 * @method configureStore
 *
 * @export
 * @returns {Object} store
 * 
 * @description konfigurasi redux store
 */
export default function configureStore () {
    const store = createStore(
        reducers, 
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        compose(applyMiddleware(...middlewares))
    );

    sagaMiddleware.run(RootSaga);
    
    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('../reducers/index').default;
            store.replaceReducer(nextRootReducer);
        })
    }
    return store;
}