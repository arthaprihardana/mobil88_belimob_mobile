/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-08 20:41:20 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-08 20:41:20 
 */
/** @format */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
