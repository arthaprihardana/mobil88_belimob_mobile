# Belimob Mobile Apps


Mobile apps for Belimob Member's ([http://belimob.com](http://belimob.com)). 

This apps created using React Native.
See the official [React Native website](https://facebook.github.io/react-native/) for start using React Native.

## Requirements

Minimum target Operating System >= Android 4.1 (API 16) dan >= iOS 9.0.

## Dependencies

- [lodash (^4.17.10)](https://lodash.com) - A modern JavaScript utility library delivering modularity, performance & extras
- [moment (2.22.2)](http://momentjs.com/docs/) - A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.
- [numeral (^2.0.6)](http://numeraljs.com/) - A javascript library for formatting and manipulating numbers
- [prop-types (^15.6.2)](https://www.npmjs.com/package/prop-types) - Runtime type checking for React props and similar objects.
- [react (16.4.1)](https://reactjs.org/) - React is a JavaScript library for creating user interfaces.
- [react-native (0.56.0)](https://facebook.github.io/react-native/) - Build mobile apps with React
- [react-native-camera (^1.6.4)](https://github.com/react-native-community/react-native-camera) - The comprehensive camera module for React Native
- [react-native-charts-wrapper (^0.4.4)](https://github.com/wuxudong/react-native-charts-wrapper) - A react-native charts support both android and ios.
- [react-native-fetch-blob (^0.10.8)](https://github.com/wkh237/react-native-fetch-blob#readme) - A module provides upload, download, and files access API. Supports file stream read/write for process large files.
- [react-native-keyboard-aware-scroll-view (^0.7.0)](https://github.com/APSL/react-native-keyboard-aware-scroll-view#readme) - A React Native ScrollView component that resizes when the keyboard appears.
- [react-native-linear-gradient (^2.4.0)](https://github.com/brentvatne/react-native-linear-gradient#readme) - A <LinearGradient> component for react-native
- [react-native-material-ripple (^0.8.0)](https://github.com/n4kz/react-native-material-ripple#readme) - Base component for touchable elements
- [react-native-onboarding-swiper (^0.7.0)](https://github.com/jfilter/react-native-onboarding-swiper#readme) - Delightful Onboarding for your React-Native App
- [react-native-router-flux (^4.0.0-beta.40)](https://github.com/aksonov/react-native-router-flux#readme) - React Native Router using Flux architecture
- [react-native-vector-icons (^5.0.0)](https://github.com/oblador/react-native-vector-icons) - Customizable Icons for React Native with support for NavBar/TabBar/ToolbarAndroid, image source and full styling
- [react-redux (^5.0.7)](https://github.com/gaearon/react-redux) - React bindings for Redux
- [redux (^4.0.0)](http://redux.js.org/) - Predictable state container for JavaScript apps
- [redux-saga (^0.16.0)](https://redux-saga.js.org/) - Saga middleware for Redux to handle Side Effects
- [rn-placeholder (^1.2.0)](https://github.com/mfrachet/rn-placeholder) - Display some placeholder stuff before rendering your text or media content in React Native.
- [validate.js (^0.12.0)](http://validatejs.org/) - Declarative validations for JavaScript

## Dev Dependencies

- [@types/react-native (^0.56.7)](https://www.npmjs.com/package/@types/react-native) - This package contains type definitions for react-native (https://github.com/facebook/react-native)
- [babel-jest (23.4.2)](https://www.npmjs.com/package/babel-jest) - Jest plugin to use babel for transformation
- [babel-plugin-transform-remove-console (^6.9.4)](https://www.npmjs.com/package/babel-plugin-transform-remove-console) - Remove all console.* calls
- [babel-preset-react-native (5.0.2)](https://www.npmjs.com/package/babel-preset-react-native) - Babel preset for React Native applications
- [jest (23.4.2)](https://www.npmjs.com/package/jest) - Delightful JavaScript Testing
- [jsdoc (^3.5.5)](https://www.npmjs.com/package/jsdoc) - An API documentation generator for JavaScript
- [minami (^1.2.3)](https://www.npmjs.com/package/minami) - Clean and minimal JSDoc 3 Template / Theme
- [plist (^3.0.1)](https://www.npmjs.com/package/plist) - Mac OS X Plist parser/builder for Node.js and browsers
- [react-native-debugger-open (^0.3.17)](https://www.npmjs.com/package/react-native-debugger-open) - Replace `open debugger-ui with Chrome` to `open React Native Debugger` from react-native packager
- [react-test-renderer (16.4.1)](https://www.npmjs.com/package/react-test-renderer) - React package for snapshot testing
- [semver (^5.5.0)](https://www.npmjs.com/package/semver) - The semantic version parser used by npm
- [wd (^1.10.3)](https://www.npmjs.com/package/wd) - WebDriver/Selenium 2 node.js client
- [webdriverio (^4.13.2)](https://www.npmjs.com/package/webdriverio) - A Node.js bindings implementation for the W3C WebDriver protocol

## How To Install And Run Apps

- Clone or Downlod The Project
```
git clone https://github.com/developerserasiautoraya/Mobil88_Belimob_Mobile.git
```

- Install Dependencies and Dev Dependencies inside Project Folder **Mobil88_Belimob_Mobile**
```
npm install
```

- Run on Android device or Simulator
```
react-native run-android
```

- Run on iOS Simulator
```
react-native run-ios
```

## License

Belimob Apps is MIT licensed